DNA {
    "modules" StringArray [
        "bmcommon",
        "appcommon",
        "b2b",
        "webconnect",
        "belk"
    ],

    "templates" DNA {
    	"client.header" String
"package {clientPackage};

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Client representation of a {bizObjName} object.
 */
@XmlRootElement(name=\"{bizObjClass}\")
public class {bizObjClass}Representation extends com.bluemartini.rest.client.BaseRepresentation {

    public {bizObjClass}Representation() {
        // do nothing
    }

    private AttributeList attributes_ = new AttributeList();
    
    @XmlElementWrapper(name=\"attributes\")
    @XmlElement(name=\"attribute\")
    public AttributeList getAttributes() {
        return attributes_;
    }
    
    public void setAttributeList(AttributeList attributes) {
        attributes_ = attributes;
    }

",

    "client.property" String "
    private {dataType} {propertyName}_;

    @XmlElement
    public {dataType} get{PropertyName}() {
        return {propertyName}_;
    }
    
    public void set{PropertyName}({dataType} {propertyName}) {
        {propertyName}_ = {propertyName};
    }
    
",

    "client.property.Currency" String "
    private BigDecimal {propertyName}_;

    @XmlElement
    public BigDecimal get{PropertyName}() {
        return {propertyName}_;
    }
    
    public void set{PropertyName}(BigDecimal {propertyName}) {
        {propertyName}_ = {propertyName};
    }
    
",

        "client.footer" String "
}
"

    	"server.header" String
"package {serverPackage};

import com.bluemartini.dna.BMContext;
import com.bluemartini.dna.BusinessObject;
import com.bluemartini.dna.Currency;
import com.bluemartini.rest.ResourceRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Server representation of a {bizObjName} object.
 */
@XmlRootElement(name=\"{bizObjClass}\")
public class {bizObjClass}Representation extends ResourceRepresentation {

    public {bizObjClass}Representation(BusinessObject bizObj) {
        super(bizObj);
        Number id = bizObj.getNumberID();
        if( id!=null ) setResourceID(id.toString());
        attributes_ = new AttributeList(bizObj);
    }

    public {bizObjClass}Representation() {
        this(BMContext.createBusinessObject(\"{bizObjName}\"));
    }


    private AttributeList attributes_;
    
    @XmlElementWrapper(name=\"attributes\")
    @XmlElement(name=\"attribute\")
    public AttributeList getAttributes() {
        return attributes_;
    }
    
    public void setAttributeList(AttributeList attributes) {
        attributes_ = attributes;
        attributes.apply(boResource_);
    }

",

    "server.property" String "

    @XmlElement
    public {dataType} get{PropertyName}() {
        return boResource_.get{tag}(\"{originalName}\");
    }
    
    public void set{PropertyName}({dataType} {propertyName}) {
        validateAndSet(\"{originalName}\",{propertyName});
    }
    
",

    "server.property.DateTime" String "

    @XmlElement
    public Calendar get{PropertyName}() {
        return boResource_.get{tag}(\"{originalName}\");
    }
    
    public void set{PropertyName}(Calendar {propertyName}) {
        validateAndSet(\"{originalName}\",{propertyName});
    }
    
",

    "server.property.Currency" String "

    @XmlElement
    public BigDecimal get{PropertyName}() {
        Currency value = boResource_.getCurrency(\"{originalName}\");
        return value==null ? null : value.toBigDecimal();
    }
    
    public void set{PropertyName}({dataType} {propertyName}) {
        Currency value = {propertyName}==null ? null : new Currency({propertyName});
        validateAndSet(\"{originalName}\",value);
    }
    
",
        "server.footer" String "
}
"
    }
}
