DNA {
    // This file defines parameters and configurable resources for the REST
    // API.  Configuration of custom classes should not be made directly in
    // this file.  They should instead be provided in another restapi.dna file
    // located in a custom module.
    //
    // Security settings - Turn authentication on or off and configure
    // authentication providers for the REST API globally and at the resource
    // level.
    "security" DNA {
        "authentication" DNA {
            // default authentication method
            "default" String "OAuth",

            "providers" DNA {
                "BASIC" DNA {
                    "class" String "com.bluemartini.rest.security.HTTPBasicAuthProvider",
                    "secure_transport" Boolean "true",
                    "login_bizact" String "Login",
                    "realm" String "belk.com"
                },
                "OAuth" DNA {
                    "class" String "com.bluemartini.rest.security.OAuthConsumerRequestProvider",
                    "max_timestamp_offset" Integer "300",
                    "secure_transport" Boolean "false"
                },
                "HTTPS" DNA {
                    "class" String "com.bluemartini.rest.security.HTTPSRequiredProvider",
                    "secure_transport" Boolean "true",
                }
                "None" DNA {
                    "class" String "com.bluemartini.rest.security.NoAuthenticationProvider",
                }
            }

            // Specify the configuration for any resource-specific authentication providers here.
            // Each entry to the override list should be keyed by the resource name. 
            "resource_overrides" DNA {
                "OAuthConsumer#POST" String "BASIC",
                "User#POST" String "HTTPS"
            }
        },
        "enabled" Boolean "true",
        "reset_priv_on_auth" Boolean "false"
    },

    // Log settings - set to true to enable log entries for responses with a
    // specific HTTP status code.  For any log entries to be recorded, the
    // 'rest' component log level must be set to the ERROR level (0) or higher
    // in appconfig.
    "log_error_responses" DNA {
        "400" Boolean "true",
        "404" Boolean "false",
        "500" Boolean "true"
    },

    // Following entries define the application classes that implement the REST
    // API.  The key for each entry should be a name for the object that is
    // unique within all instances of this file.  The value of each entry
    // should be the fully-qualified name of the class.
    //
    // Add any JAX-RS provider classes here.  These will be loaded at start-up
    // of the JAX-RS runtime.
    "providers" DNA {
        "JAXBContext" String "com.bluemartini.rest.JAXBContextResolver",
        "JSONContext" String "com.bluemartini.rest.JSONJacksonContextResolver",
        "BMException" String "com.bluemartini.rest.BMExceptionMapper",
        "WebApplicationException" String "com.bluemartini.rest.WebApplicationExceptionMapper",
        "JAXBException" String "com.bluemartini.rest.JAXBExceptionMapper",
        "JsonParseException" String "com.bluemartini.rest.JsonParseExceptionMapper",
        "RuntimeException" String "com.bluemartini.rest.RuntimeExceptionMapper"
    },

    // Add any JAX-RS root resource classes here.  These will be loaded at
    // start-up of the JAX-RS runtime.  The key for these entries will be
    // considered the name of the resource as referenced by the implementing
    // code and configuration files.  This name does not necessarily match the
    // name used in the URI.
    "resources" DNA {
        "CreateCart" String "com.belk.rest.server.CreateCartResource",
        "Objects" String "com.bluemartini.rest.ObjectResource",
	"Ping" String "com.bluemartini.rest.PingResource",
//        "GiftRegistry" String "com.bluemartini.rest.giftregistry.GiftRegistryResource",
        "Product" String "com.belk.rest.ProductResource",
//        "Sku" String "com.belk.rest.SkuResource"
        "OAuthConsumer" String "com.bluemartini.rest.security.OAuthConsumerResource"
    },

    // JAXB is used for marshaling resource representation beans to XML.  When
    // using inheritance and polymorphic types in the bean model, JAXB needs to
    // be made aware of any and all sub-classes in the model.  Otherwise, it
    // may have problems recognizing the runtime type of a bean and will only
    // serialize the properties of the base type.  Add any resource
    // representation beans here that need to be bootstrapped into the
    // JAXBContext.
    "jaxb_types" DNA {
//        "GiftRegistryJAXB" String "com.bluemartini.rest.giftregistry.GiftRegistryRepresentation",
//        "GiftRegistryItemJAXB" String "com.bluemartini.rest.giftregistry.GiftRegistryItemRepresentation",
//        "GiftRegistryItemCollectionJAXB" String "com.bluemartini.rest.giftregistry.GiftRegistryItemCollectionRepresentation",
//        "AssignmentOptionsJAXB" String "com.bluemartini.rest.giftregistry.AssignmentOptionsRepresentation"
    }
}
