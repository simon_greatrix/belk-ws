package com.belk.rest.gifts;

import com.bluemartini.core.BMUtil;
import com.bluemartini.dna.*;
import com.bluemartini.html.WebServiceContext;
import com.bluemartini.rest.Authenticate;
import com.bluemartini.rest.BaseResource;
import com.bluemartini.rest.RESTConstants;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import java.net.URI;

/**
 * A REST API resource for Gift Registry Line items.  This class is not a target resource for requests but is a 
 * sub-resource of {@link GiftRegistryResource}.  XML and JSON are the default media types supported.
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * @author Stephanie Kroll
 */
@Authenticate
public class GiftRegistryItemResource extends BaseResource {
    
    /** The valid expand fields for this resource */
    public static DNAStringArray EXPAND_FIELDS = new DNAStringArray(new String[]{"assignmentOptions"});
    
    /** The Path template to this sub-resource */
    public static final String RESOURCE_PATH_TEMPLATE = "/giftRegistries/{giftRegistryId}/items";
    
    // class name for logging
    private static final String CLASS_NAME = "GiftRegistryItemResource";
    
    // The name of this resource
    private static final String RESOURCE_NAME = "GiftRegistryItem";
    
    // object ID of the parent gift registry for the lines
    private Long giftRegistryId_ = null;
    
    /**
     * Constructs a new {@link GiftRegistryItemResource} for the gift registry with the specified
     * <em>giftRegistryId</em>.
     * 
     * @param request           the {@link Request} object for the current request
     * @param uriInfo           the {@link UriInfo} object for the current request
     * @param giftRegistryId    the Long object ID of the parent Gift Registry
     * @throws WebApplicationException
     *                          if <em>giftRegistryId</em> does not evaluate to a Long
     */
    public GiftRegistryItemResource(Request request, UriInfo uriInfo, Long giftRegistryId) {
        request_ = request;
        uriInfo_ = uriInfo;
        giftRegistryId_ = giftRegistryId;
    }
    
    /**
     * Returns the name of this resource
     * 
     * @return      a String containing the resource name
     * @see com.bluemartini.rest.BaseResource#getResourceName()
     */
    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }
    
    /**
     * Returns the base path to this resource (exclusive of the specific line item ID).
     * 
     * @return      a String containing the base path to this resource
     * @see com.bluemartini.rest.BaseResource#getBaseResourcePath()
     */
    @Override
    public String getBaseResourcePath() {
        return BMUtil.replaceString(RESOURCE_PATH_TEMPLATE, "{giftRegistryId}", giftRegistryId_.toString());
    }
    
    /**
     * Endpoint method for a GET request when the parent GiftRegistryResource returns this resource and the request 
     * path is fully matched in the parent.  This is expected to be <b>/giftRegistries/{giftRegistryId}/items</b>.
     * Returns the collection of Gift Registry Line items for the parent Gift Registry.
     * 
     * @param offset        optional - starting index for the page of resources to be returned
     * @param limit         optional - size of the page of resources to be returned
     * @return              collection of line items for the registry
     * @throws BMException  if an error occurs calling into the commerce API
     * @throws WebApplicationException
     *                      the REST API request can't be completed and an HTTP error status is being returned
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    public Response getGiftRegistryItems(
            @QueryParam(RESTConstants.PARAM_PAGE_OFFSET) Integer offset, 
            @QueryParam(RESTConstants.PARAM_PAGE_LIMIT) Integer limit) throws BMException {
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "getGiftRegistryItems", 
                    "enter giftRegistryId=" + giftRegistryId_);
        }

        preProcessRequest();
        
        // Default the paging parameters if not supplied
        if (offset == null) offset = RESTConstants.DEFAULT_PAGE_OFFSET;
        if (limit == null) limit = RESTConstants.DEFAULT_PAGE_LIMIT;
        
        // Call the business logic to get the line items
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        GiftRegistryItemCollectionRepresentation giftRegistryItems = 
                util.getGiftRegistryItems(giftRegistryId_, offset, limit, false);
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "getGiftRegistryItems", "exit success");
        }
                
        return Response.ok(giftRegistryItems).build();
    }
    
    /**
     * Endpoint method for a GET request when the parent {@link GiftRegistryResource} returns this resource and 
     * the request path is matched in the parent except for the last fragment specifying the line item ID.  
     * This is expected to be <b>/giftRegistries/{giftRegistryId}/items/{giftRegistryItemId}</b>.  
     * Returns the GiftRegistry line item for the specified <em>giftRegistryId</em> and <em>giftRegistryItemId</em>. 
     * 
     * @param itemId        the Long ID of the line item
     * @param expand        optional - CSV string containing Gift Registry Item field names to be expanded.  
     *                          Valid values are <ul><li>assignmentOptions</li></ul>
     * @return              the gift registry line item
     * @throws BMException  if an error occurs into calling the commerce API
     * @throws WebApplicationException
     *                      the REST API request can't be completed and an HTTP error status is being returned
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{giftRegistryItemId}")
    public Response getGiftRegistryItem(
            @PathParam("giftRegistryItemId") Long itemId,
            @QueryParam("expand") String expand) throws BMException {
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "getGiftRegistryItem", 
                    "enter giftRegistryId=" + giftRegistryId_ + ", itemId=" + itemId);
        }
        
        preProcessRequest();
        
        // Call the business logic to get the line item
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        GiftRegistryItemRepresentation giftRegistryItem = util.getGiftRegistryItem(giftRegistryId_, itemId, 
                uriInfo_.getAbsolutePathBuilder(), expand, false);
        
        // If no line item with specified ID, return Not Found
        if (giftRegistryItem == null) {
            DNAList dnaParams = new DNAList();
            dnaParams.setString("objectId", Long.toString(giftRegistryId_));
            dnaParams.setString("itemId", Long.toString(itemId));
            throw webAppException(Status.NOT_FOUND, "RESTAPI_GETGIFTREGISTRYITEM_NOT_FOUND", dnaParams);
        }
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE, CLASS_NAME, "getGiftRegistryItem", 
                        "response data=" + giftRegistryItem.toString());
            }
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "getGiftRegistryItem", "exit success");
        }
        
        return Response.ok(giftRegistryItem).build();
    }
    
    /**
     * Endpoint method for a POST request when the parent GiftRegistryResource returns this resource and the request 
     * path is fully matched in the parent.  This is expected to be <b>/giftRegistries/{giftRegistryId}/items</b>.
     * Creates a Gift Registry line item for the Gift Registry specified by <code>giftRegistryId</code> using the 
     * values for <code>skuId</code> and <code>qtyDesired</code> specified in the query parameters.  
     * <code>qtyDesired</code> must be positive or it defaults to one.
     * 
     * @param skuId         a String containing the object ID of the sku to add to the line - must evaluate to a Long
     * @param qtyDesired    a String containing the quantity Desired of the Sku - must evaluate to an Integer
     * @return              a status 201 Created with the URI to the new resource in the HTTP 'Location' header
     * @throws BMException  if an error occurs calling into the Commerce API
     * @throws WebApplicationException
     *                      the REST API request can't be completed and an HTTP error status is being returned
     */
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response createGiftRegistryItem(
            @QueryParam("skuId") String skuId,
            @QueryParam("quantityDesired") String qtyDesired) throws BMException {
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "createGiftRegistryItem", 
                    "enter giftRegistryId=" + giftRegistryId_ + ", skuId=" + skuId + ", quantityDesired=" + qtyDesired);
        }

        preProcessRequest();
        
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        
        // Verify user is owner of the parent gift registry - security needs to be enabled for this method to succeed.
        Long userId = WebServiceContext.getContext().getLoginID();
        if (userId == null) {
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG, CLASS_NAME, "createGiftRegistryItem", 
                        "A user ID for the request was not found. Is authentication enabled?");
            }
            throw webAppException(Status.INTERNAL_SERVER_ERROR, "RESTAPI_UNEXPECTED_CONDITION_ERROR");
        }
        if (!util.isGiftRegistryOwnedBy(giftRegistryId_, userId)) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_UPDATE");
        }

        // Validate query parameters
        Long iSkuId = null;
        Integer iQtyDesired = null;
        try {
            iSkuId = Long.parseLong(skuId);
        } catch (NumberFormatException nfe) {
            DNAList dnaParams = new DNAList();
            dnaParams.setString("paramName", "skuId");
            dnaParams.setString("datatype", "Long integer");
            throw webAppException(Status.BAD_REQUEST, "RESTAPI_PARAM_INVALID_TYPE", dnaParams);
        }
        try {
            iQtyDesired = Integer.parseInt(qtyDesired);
        } catch (NumberFormatException nfe) {
            DNAList dnaParams = new DNAList();
            dnaParams.setString("paramName", "quantityDesired");
            dnaParams.setString("datatype", "Integer");
            throw webAppException(Status.BAD_REQUEST, "RESTAPI_PARAM_INVALID_TYPE", dnaParams);
        }
        // Verify a sku with the specified ID exists
        if (!util.skuExists(iSkuId)) {
            DNAList dnaParams = new DNAList();
            dnaParams.setString("objectName", "Sku");
            dnaParams.setString("objectId", skuId);
            throw webAppException(Status.NOT_FOUND, "RESTAPI_OBJECT_NOT_FOUND", dnaParams);
        }
        
        // Call the business logic
        Long itemId = util.createGiftRegistryItem(giftRegistryId_, iSkuId, iQtyDesired);
        
        // Send back a "201 Created" with the location of the new resource
        URI location = uriInfo_.getAbsolutePathBuilder().path(itemId.toString()).build();
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "createGiftRegistryItem", 
                    "exit itemId=" + itemId);
        }
        
        return Response.created(location).build();
    }
    
    /**
     * Endpoint method for a PUT request when the parent {@link GiftRegistryResource} returns this resource and 
     * the request path is matched in the parent except for the last fragment specifying the line item ID.
     * This is expected to be <b>/giftRegistries/{giftRegistryId}/items/{giftRegistryItemId}</b>.  
     * Updates the Gift Registry Item resource whose object ID matches the specified <code>itemId</code> using 
     * the data in the associated request body.
     * 
     * @param itemId                the Long object ID of Gift Registry line to update
     * @param giftRegistryItem      a {@link GiftRegistryItemRepresention} with the new values for the
     *                                  Gift Registry line 
     * @return                      the updated Gift Registry Item with a status 200 OK
     * @throws BMException          if an error occurs calling into the Commerce API
     * @throws WebApplicationException
     *                              if the REST API request can't be completed and an HTTP error status 
     *                                  is being returned     
     */
    @PUT
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{giftRegistryItemId}")
    public Response updateGiftRegistryItem(
            @PathParam("giftRegistryItemId") Long itemId,
            GiftRegistryItemRepresentation giftRegistryItem) throws BMException {
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "updateGiftRegistryItem", 
                    "enter giftRegistryId=" + giftRegistryId_ + ", itemId=" + itemId);
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE, CLASS_NAME, "updateGiftRegistryItem", 
                        "request data=" + giftRegistryItem != null ? giftRegistryItem.toString() : null);
            }
        }
        
        preProcessRequest();
        
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        
        // Verify user is owner of this gift registry - security needs to be enabled for this method to succeed.
        Long userId = WebServiceContext.getContext().getLoginID();
        if (userId == null) {
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG, CLASS_NAME, "updateGiftRegistryItem", 
                        "A user ID for the request was not found. Is authentication enabled?");
            }
            throw webAppException(Status.INTERNAL_SERVER_ERROR, "RESTAPI_UNEXPECTED_CONDITION_ERROR");
        }
        if (!util.isGiftRegistryOwnedBy(giftRegistryId_, userId)) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_UPDATE");
        }

        // Validate parameters...
        // Validate itemId matches the resource ID of the passed-in resource being updated
        if (giftRegistryItem.getResourceID() == null || 
                giftRegistryItem.getResourceID().endsWith(Long.toString(itemId))) {
            giftRegistryItem.setResourceID(Long.toString(itemId));
        } else {
            throw webAppException(Status.CONFLICT, "RESTAPI_ID_MISMATCH");
        }
        // Validate line exists
        if (util.getGiftRegistryLine(giftRegistryId_, itemId) == null) {
            DNAList dnaParams = new DNAList();
            dnaParams.setString("objectId", Long.toString(giftRegistryId_));
            dnaParams.setString("itemId", Long.toString(itemId));
            throw webAppException(Status.NOT_FOUND, "RESTAPI_GETGIFTREGISTRYITEM_NOT_FOUND", dnaParams);
        }
        
        // Call the business logic
        giftRegistryItem = util.updateGiftRegistryItem(giftRegistryItem, giftRegistryId_);
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "updateGiftRegistryItem", "exit success");
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE, CLASS_NAME, "updateGiftRegistryItem", 
                        "response data=" + giftRegistryItem.toString());
            }
        }
        
        return Response.ok(giftRegistryItem).build();
    }
    
    /**
     * Endpoint method for a DELETE request when the parent {@link GiftRegistryResource} returns this resource and 
     * the request path is matched in the parent except for the last fragment specifying the line item ID.
     * This is expected to be <b>/giftRegistries/{giftRegistryId}/items/{giftRegistryItemId}</b>.  
     * Deletes the Gift Registry Item resource whose object ID matches the specified <code>giftRegistryItemId</code>.
     * 
     * @param itemId                the Long object ID of Gift Registry line to delete
     * @return                      an empty response with a status 204 No Content if successfully deleted
     * @throws BMException          if an error occurs calling into the Commerce API
     * @throws WebApplicationException
     *                              if the REST API request can't be completed and an HTTP error status 
     *                                  is being returned     
     */
    @DELETE
    @Path("{giftRegistryItemId}")
    public Response deleteGiftRegistryItem(@PathParam("giftRegistryItemId") Long itemId) throws BMException {
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "deleteGiftRegistryItem", 
                    "enter lineItemId=" + itemId);
        }
        
        preProcessRequest();
        
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        
        // Verify user is owner of this gift registry - security needs to be enabled for this method to succeed.
        Long userId = WebServiceContext.getContext().getLoginID();
        if (userId == null) {
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG, CLASS_NAME, "deleteGiftRegistryItem", 
                        "A user ID for the request was not found. Is authentication enabled?");
            }
            throw webAppException(Status.INTERNAL_SERVER_ERROR, "RESTAPI_UNEXPECTED_CONDITION_ERROR");
        }
        if (!util.isGiftRegistryOwnedBy(giftRegistryId_, userId)) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_DELETE");
        }
        
        GiftRegistryItemRepresentation giftRegistryItem =  util.getGiftRegistryItem(
                giftRegistryId_, itemId, uriInfo_.getAbsolutePathBuilder(), null, false);
        
        // If no line item with specified ID, return Not Found
        if (giftRegistryItem == null) {
            DNAList dnaParams = new DNAList();
            dnaParams.setString("objectId", Long.toString(giftRegistryId_));
            dnaParams.setString("itemId", Long.toString(itemId));
            throw webAppException(Status.NOT_FOUND, "RESTAPI_GETGIFTREGISTRYITEM_NOT_FOUND", dnaParams);
        }
        
        // Call the business logic
        util.deleteGiftRegistryItem(giftRegistryItem.getBusinessObject());
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "deleteGiftRegistryItem", "exit success");
        }
        
        return Response.noContent().build();
    }

}
