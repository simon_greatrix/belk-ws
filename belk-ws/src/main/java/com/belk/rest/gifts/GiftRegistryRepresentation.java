package com.belk.rest.gifts;

import com.bluemartini.dna.BMContext;
import com.bluemartini.dna.BusinessObject;
import com.bluemartini.htmlapp.UserAccount;
import com.bluemartini.rest.Link;
import com.bluemartini.rest.RESTConstants;
import com.bluemartini.rest.ResourceRepresentation;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.Calendar;

/**
 * A REST API representation for a Gift Registry resource.  Wraps the GIFT_REGISTRY_HEADER business object.
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * @author Stephanie Kroll
 */
@XmlRootElement(name="giftRegistry")
public class GiftRegistryRepresentation extends ResourceRepresentation {
    
    // the list of fields that can be expanded
    private static final String EXPAND_FIELDS = "items";
    
    // the UserAccount associated with the owner of the gift registry
    private UserAccount userAccount_;
    
    // the items child resource - can be a link or the in-lined collections resource
    Object items_ = null;
    
    /**
     * Default constructor required by JAXB.
     */
    public GiftRegistryRepresentation() {
        super(BMContext.createBusinessObject("GIFT_REGISTRY_HEADER"));
    }

    /**
     * Constructs a new representation wrapping the supplied {@link BusinessObject}.  The business object is
     * expected to be of type GIFT_REGISTRY_HEADER.
     * 
     * @param boRegistry        the business object containing the data for this resource representation
     */
    public GiftRegistryRepresentation(BusinessObject boRegistry) {
        super(boRegistry);
    }
    
    /**
     * Sets the user account for the owner of this gift registry resource.
     * 
     * @param userAccount       the {@link UserAccount} 
     */
    public void setRegistryOwner(UserAccount userAccount) {
        userAccount_ = userAccount;
    }
    
    /**
     * Returns the <code>expand</code> fields for this resource.
     * 
     * @return          a String containing the expandable field names delineated by commas
     * @see com.bluemartini.rest.ResourceRepresentation#getExpand()
     */
    @Override
    @XmlElement(name=RESTConstants.FIELD_EXPAND)
    public String getExpand() {
        return EXPAND_FIELDS;
    }
    
    /**
     * Returns the <code>registryName</code> property from the GIFT_REGISTRY business object.
     * 
     * @return          a String containing the registry name
     */
    @XmlElement
    public String getRegistryName() {
        return boResource_.getString("registryName");
    }
    
    /**
     * Sets the <code>registryName</code> property on the GIFT_REGISTRY business object.
     * 
     * @param value     a String containing the registry name
     */
    public void setRegistryName(String value) {
        validateAndSet("registryName", value);
    }
    
    /**
     * Returns the <code>eventDt</code> property from the GIFT_REGISTRY business object.
     * 
     * @return          a Calendar with the event date - a date-only property
     */
    @XmlElement
    public Calendar getEventDate() {
        // this is a date-only property
        return adjustAndGetDateTime("eventDt");
    }
    
    /**
     * Sets the <code>eventDt</code> property on the GIFT_REGISTRY business object.
     * 
     * @param value     a Calendar with the event date - a date-only property
     */
    public void setEventDate(Calendar value) {
        // this is a date-only property
        adjustAndSetDateTime("eventDt", value);
    }
    
    /**
     * Returns the <code>eventCity</code> property from the GIFT_REGISTRY business object.
     * 
     * @return          a String containing the event city
     */
    @XmlElement
    public String getEventCity() {
        return boResource_.getString("eventCity");
    }
    
    /**
     * Sets the <code>eventCity</code> property on the GIFT_REGISTRY business object.
     * 
     * @param value     a String containing the event city
     */
    public void setEventCity(String value) {
        validateAndSet("eventCity", value);
    }
    
    /**
     * Returns the <code>eventState_cd</code> property from the GIFT_REGISTRY business object.
     * 
     * @return          a String containing the event state code
     */
    @XmlElement
    public String getEventStateCd() {
        return boResource_.getString("eventState_cd");
    }
    
    /**
     * Sets the <code>eventState_cd</code> property on the GIFT_REGISTRY business object.
     * 
     * @param value     a String containing the event state code
     */
    public void setEventStateCd(String value) {
        validateAndSet("eventState_cd", value);
    }
    
    /**
     * Returns the <code>country_cd</code> property from the GIFT_REGISTRY business object.
     * 
     * @return          a String containing the country code
     */
    @XmlElement
    public String getEventCountryCd() {
        return boResource_.getString("country_cd");
    }
    
    /**
     * Sets the <code>country_cd</code> property on the GIFT_REGISTRY business object.
     * 
     * @param value     a String containing the country code
     */
    public void setEventCountryCd(String value) {
        validateAndSet("country_cd", value);
    }
    
    /**
     * Returns the <code>status_cd</code> property from the GIFT_REGISTRY business object.
     * 
     * @return          a String containing the status code
     */
    @XmlElement
    public String getStatusCd() {
        return boResource_.getString("status_cd");
    }
    
    /**
     * Sets the <code>status_cd</code> property on the GIFT_REGISTRY business object.
     * 
     * @param value     a String containing the status code
     */
    public void setStatusCd(String value) {
        validateAndSet("status_cd", value);
    }
    
    /**
     * Returns the <code>eventType</code> property from the GIFT_REGISTRY business object.
     * 
     * @return          a String containing the event type code
     */
    @XmlElement
    public Integer getEventTypeCd() {
        return boResource_.getInteger("eventType");
    }
    
    /**
     * Sets the <code>eventType</code> property on the GIFT_REGISTRY business object.
     * 
     * @param value     a String containing the event type code
     */
    public void setEventTypeCd(Integer value) {
        validateAndSet("eventType", value);
    }
    
    /**
     * Returns the <code>modifyDt</code> property from the GIFT_REGISTRY business object.
     * 
     * @return          a Calendar with the modify date
     */
    @XmlElement
    public Calendar getModifyDate() {
        return boResource_.getDateTime("modifyDt");
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setModifyDate(Calendar value) {
        // property is read only for data-binding
    }
    
    /**
     * Returns the <code>lastName</code> property from the User Account associated with this gift registry.
     * 
     * @return          a String containing the registry owner's last name
     */
    @XmlElement
    public String getLastName() {
         return userAccount_ != null ? userAccount_.getLastName() : null;
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setLastName(String value) {
        // property is read only for data-binding   
    }
    
    /**
     * Returns the <code>firstName</code> property from the User Account associated with this gift registry.
     * 
     * @return          a String containing the registry owner's first name
     */
    @XmlElement
    public String getFirstName() {
        return userAccount_ != null ? userAccount_.getFirstName() : null;
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setFirstName(String value) {
        // property is read only for data-binding           
    }
    
    /**
     * Returns the formatted full name from the User Account associated with this gift registry.
     * 
     * @return          a String containing the registry owner's full name
     */
    @XmlElement
    public String getFullName() {
        return userAccount_ != null ? userAccount_.getFullName() : null;
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setFullName(String value) {
        // property is read only for data-binding      
    }
    
    /**
     * Returns the object ID for the User Account associated with this gift registry.
     * 
     * @return          a Long with the ID of the user account
     */
    @XmlElement
    public Long getUserAccountId() {
        return userAccount_ != null ? userAccount_.getLongID() : null;
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setUserAccountId(Long value) {
        // property is read only for data-binding      
    }
    
    /**
     * Gets the gift registry line items as a child resource.  The return value can be a {@link Link} to the resource
     * or a {@link GiftRegistryItemCollectionRepresentation} as the in-lined resource.
     * 
     * @return              the items child resource, either linked or in-lined
     */
    @XmlElement
    public Object getItems() {
        return items_;
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param items         ignored
     */
    public void setItems(Object items) {
        // property is read only for data-binding   
    }
    
    /**
     * Sets the gift registry line items as a {@link Link} to the child resource.  The link will be built 
     * from the supplied {@link UriInfo} object and specified URI string.
     * 
     * @param uriInfo       the UriInfo object for the current request
     * @param itemsLinkUri  the URI string for the link's "href" field
     */
    public void setItems(UriInfo uriInfo, String itemsLinkUri) {
        items_ = new Link(uriInfo, "giftRegistries/items", itemsLinkUri);
    }
}
