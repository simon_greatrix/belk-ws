package com.belk.rest.gifts;

import com.bluemartini.dna.BMContext;
import com.bluemartini.dna.BusinessObject;
import com.bluemartini.dna.Currency;
import com.bluemartini.htmlapp.Sku;
import com.bluemartini.rest.Link;
import com.bluemartini.rest.RESTConstants;
import com.bluemartini.rest.ResourceRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;

/**
 * A REST API representation for a Gift Registry Item (line) resource.
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * @author Stephanie Kroll
 */
@XmlRootElement(name="giftRegistryItem")
public class GiftRegistryItemRepresentation extends ResourceRepresentation {
    
    // the list of fields that can be expanded
    private static final String EXPAND_FIELDS = "assignmentOptions";
    
    // the Sku referenced by this gift registry line
    private Sku sku_;
    
    // the sale price for this line item
    private Currency salePrice_;
    
    // URL to small image for the sku on the line item
    String imageSmallUrl_;
    
    // the options to assign this item to a different gift registry
    // can be a link to the collection of assignment links or the in-lined collection of links
    Object assignmentOptions_;

    /**
     * Default constructor required by JAXB.
     */
    public GiftRegistryItemRepresentation() {
        super(BMContext.createBusinessObject("GIFT_REGISTRY_LINE"));
    }
    
    /**
     * Constructs a new representation wrapping the supplied {@link BusinessObject}.  The business object is
     * expected to be of type GIFT_REGISTRY_LINE.
     * 
     * @param boRegistryItem    the business object containing the data for this resource representation
     */
    public GiftRegistryItemRepresentation(BusinessObject boRegistryItem) {
        super(boRegistryItem);
    }
    
    /**
     * Sets the sku referenced by this gift registry line item.
     * 
     * @param sku       the {@link Sku}
     */
    public void setSku(Sku sku) {
        sku_ = sku;
    }
    
    /**
     * Returns the <code>expand</code> fields for this resource.
     * 
     * @return          a String containing the expandable field names delineated by commas
     * @see com.bluemartini.rest.ResourceRepresentation#getExpand()
     */
    @Override
    @XmlElement(name=RESTConstants.FIELD_EXPAND)
    public String getExpand() {
        return EXPAND_FIELDS;
    }
    
    /**
     * Returns the <code>qtyDesired</code> property from the GIFT_REGISTRY_LINE business object.
     * 
     * @return      an Integer with the qtyDesired
     */
    @XmlElement
    public Integer getQuantityDesired() {
        return boResource_.getInteger("qtyDesired");
    }
    
    /**
     * Sets the <code>qtyDesired</code> property on the GIFT_REGISTRY_LINE business object.
     * 
     * @param value an Integer with the qtyDesired
     */
    public void setQuantityDesired(Integer value) {
        validateAndSet("qtyDesired", value);
    }
    
    /**
     * Returns the <code>qtyReceived</code> property from the GIFT_REGISTRY_LINE business object.
     * 
     * @return      an Integer with the qtyReceived
     */
    @XmlElement
    public Integer getQuantityReceived() {
        return boResource_.getInteger("qtyReceived");
    }
    
    /**
     * Sets the <code>qtyReceived</code> property on the GIFT_REGISTRY_LINE business object.
     * 
     * @param value an Integer with the qtyReceived
     */
    public void setQuantityReceived(Integer value) {
        validateAndSet("qtyReceived", value);
    }
    
    /**
     * Returns the <code>status_cd</code> property from the GIFT_REGISTRY_LINE business object.
     * 
     * @return      a String containing the status_cd
     */
    @XmlElement
    public String getStatusCd() {
        return boResource_.getString("status_cd");
    }
    
    /**
     * Sets the <code>status_cd</code> property on the GIFT_REGISTRY_LINE business object.
     * 
     * @param value a String containing the status_cd
     */
    public void setStatusCd(String value) {
        validateAndSet("status_cd", value);
    }
    
    /**
     * Returns the <code>message</code> property from the GIFT_REGISTRY_LINE business object.
     * 
     * @return      a String containing the message
     */
    @XmlElement
    public String getMessage() {
        return boResource_.getString("message");
    }
    
    /**
     * Sets the <code>message</code> property on the GIFT_REGISTRY_LINE business object.
     * 
     * @param value a String containing the message
     */
    public void setMessage(String value) {
        validateAndSet("message", value);
    }
    
    /**
     * Returns the <code>grg_sku_id</code> property from the GIFT_REGISTRY_LINE business object.
     * 
     * @return      a Long with the grg_sku_id
     */
    @XmlElement
    public Long getSkuId() {
        return boResource_.getLong("grg_sku_id");
    }

    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setSkuId(Long value) {
        // property is read only for data-binding
    }
    
    /**
     * Returns the <code>shortDesc</code> property from the Sku associated with this line.
     * 
     * @return      a String containing the Sku's short description
     */
    @XmlElement
    public String getSkuDescription() {
        return sku_ != null ? sku_.getString("shortDesc") : null;
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setSkuDescription(String value) {
        // property is read only for data-binding
    }
    
    /**
     * Returns the <code>ATR_Prd_Brand</code> attribute from the Sku associated with this line.
     * 
     * @return      a String containing the Sku's product brand attribute
     */
    @XmlElement
    public String getProductBrand() {
        return sku_ != null ? sku_.getString("ATR_Prd_Brand") : null;
    }

    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setProductBrand(String value) {
        // property is read only for data-binding
    }
    
    /**
     * Returns the product code from the Sku associated with this line.
     * 
     * @return      a String containing the Sku's product code
     */
    @XmlElement
    public String getProductCode() {
        return sku_ != null ? sku_.getProductCode() : null;
    }

    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setProductCode(String value) {
        // property is read only for data-binding
    }
    
    /**
     * Returns the default sale price for the Sku associated with this line. 
     * 
     * @return      a BigDecimal containing the numerical sale price
     */
    @XmlElement
    public BigDecimal getSalePrice() {
        return salePrice_ != null ? salePrice_.toBigDecimal() : null;
    }

    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setSalePrice(BigDecimal value) {
        // property is read only for data-binding
    }
    
    /**
     * Sets the sale price for this gift registry line item.
     * 
     * @param price     the sale price as a {@link Currency}
     */
    public void setSalePriceAsCurrency(Currency price) {
        salePrice_ = price;
    }
    
    /**
     * Returns the currency code of the default sale price for the Sku associated with this line
     * 
     * @return      a String containing the currency code
     */
    @XmlElement
    public String getSalePriceCurrencyCode() {
        return salePrice_ != null ? salePrice_.getCurrencyCode() : null;
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored
     */
    public void setSalePriceCurrencyCode(String value) {
        // property is read only for data-binding
    }
    
    /**
     * Gets a URL to the small image for the line item's sku.
     * 
     * @return          URL to the image as a string
     */
    @XmlElement
    public String getImageSmallUrl() {
        return imageSmallUrl_;
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param url       ignored
     */
    public void setImageSmallUrl(String url) {
        // property is read only for data-binding
    }
    
    /**
     * Gets the assignment options as a child resource.  The return value can be a {@link Link} to the collection
     * of assignment links or an {@link AssignmentOptionsRepresentation} as the in-lined resource.
     * 
     * @return          the assignmentOptions child resource, either linked or in-lined
     */
    @XmlElement
    public Object getAssignmentOptions() {
        return assignmentOptions_;
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param value     ignored    
     */
    public void setAssignmentOptions(Object value) {
        // property is read only for data-binding
    }
}
