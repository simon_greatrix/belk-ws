package com.belk.rest.gifts;

import com.bluemartini.rest.PageableResourceRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;
import java.util.List;

/**
 * A REST API representation for a collection of Gift Registry Items (lines).
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * @author Stephanie Kroll
 */
@XmlRootElement(name="items")
public class GiftRegistryItemCollectionRepresentation extends PageableResourceRepresentation {

    // the list of gift registry item representations
    private List<GiftRegistryItemRepresentation> itemList_ = new ArrayList<GiftRegistryItemRepresentation>();
    
    /**
     * Gets the list of gift registry items.
     * 
     * @return          a list of {@link GiftRegistryItemRepresentation} representations
     */
    @XmlElement(name="data")
    public List<GiftRegistryItemRepresentation> getItems() {
        return itemList_;
    }
    
    /**
     * Adds a gift registry item to the collection.
     * 
     * @param item      the {@link GiftRegistryItemRepresentation} to add
     */
    public void addGiftRegistryItem(GiftRegistryItemRepresentation item) {
        itemList_.add(item);
    }
    
    /**
     * Setter method declaration to support JAXB introspection - not implemented.  Property is read-only.
     * 
     * @param itemList  ignored
     */
    public void setItems(List<GiftRegistryItemRepresentation> itemList) {
        // property is read only for data-binding
    }
}
