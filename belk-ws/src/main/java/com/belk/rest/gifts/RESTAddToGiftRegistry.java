package com.belk.rest.gifts;

import com.bluemartini.core.Constants;
import com.bluemartini.dna.*;
import com.bluemartini.html.HTMLResponse;
import com.bluemartini.server.BMClient;
import com.bluemartini.webconnect.WebConnect;
import com.bluemartini.webconnect.htmlapp.WebConnectAddToGiftRegistry;

/**
 * Extends the {@link WebConnectAddToGiftRegistry} business action to set the newly-created 
 * GIFT_REGISTRY_LINE on the response.
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * @author Stephanie Kroll
 */
public class RESTAddToGiftRegistry extends WebConnectAddToGiftRegistry {
    
    /**
     * Create the GIFT_REGISTRY_LINE from the CART_ITEM passed in, using the GIFT_REGISTRY_HEADER passed in as 
     * the parent.  Overrides the base class in order to set a reference to the GIFT_REGISTRY_LINE onto dnaFormData.
     * <p>
     * @param dnaFormData   DNAList representing the formData processed by this business action
     */
    @Override
    protected void createGiftRegLine(DNAList dnaFormData) throws BMException{

        BusinessObject boCartItem = dnaFormData.removeBusinessObject(WebConnect.SESSION_ADD_CART_ITEM);
        Long iSkuID = boCartItem.getLong("sku_id");

        Integer quantity = boCartItem.getInteger("quantity");
        if ((quantity != null) && (quantity.intValue() > 0) && (iSkuID != null)) {
            BusinessObject boGiftRegistryLine = BMContext.createBusinessObject("GIFT_REGISTRY_LINE");
            boGiftRegistryLine.setInteger("qtyDesired", quantity);
            boGiftRegistryLine.setInteger("qtyReceived", 0);
            boGiftRegistryLine.setLong("grg_sku_id", iSkuID);
            BusinessObject boRegHeader = dnaFormData.getBusinessObject(WebConnect.SESSION_GIFT_REGISTRY_HEADER);
            boGiftRegistryLine.setLong("grg_grh_id", boRegHeader.getLongID());
            boGiftRegistryLine.setString("status_cd", Constants.PRODUCT_ENABLED);
            boGiftRegistryLine.setString("message",boCartItem.getString("message"));

            DNAList dnaParams = new DNAList();
            dnaParams.setBusinessObject("GIFT_REGISTRY_LINE", boGiftRegistryLine);
            DNAList dnaOut = BMClient.executeBusinessAction("CreateObject", dnaParams);
            boGiftRegistryLine = dnaOut.getBusinessObject("GIFT_REGISTRY_LINE");
            dnaFormData.setBusinessObject(WebConnect.SESSION_GIFT_REGISTRY_LINE, boGiftRegistryLine);
        }
    }

    /**
     * Overrides the base class to set the newly created GIFT_REGISTRY_LINE onto the response.
     * 
     * @param htmlResponse  the {@link HTMLResponse} object being returned from this business action
     * @param dnaFormData   DNAList representing the formData processed by this business action
     * @return              the updated HTMLResponse object 
     * 
     * @see com.bluemartini.webconnect.htmlapp.WebConnectCreateGiftRegistry#updateHTMLResponse(com.bluemartini.html.HTMLResponse, com.bluemartini.dna.DNAList)
     */
    @Override
    protected HTMLResponse updateHTMLResponseSuccess(HTMLResponse htmlResponse, DNAList dnaFormData) {
        BusinessObject boLine = dnaFormData.removeBusinessObject(WebConnect.SESSION_GIFT_REGISTRY_LINE);
        htmlResponse.setBusinessObjectRef(WebConnect.SESSION_GIFT_REGISTRY_LINE, boLine);
        return super.updateHTMLResponseSuccess(htmlResponse, dnaFormData);
    }
}
