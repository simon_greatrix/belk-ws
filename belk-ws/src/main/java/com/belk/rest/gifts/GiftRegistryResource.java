package com.belk.rest.gifts;

import com.bluemartini.dna.*;
import com.bluemartini.html.WebServiceContext;
import com.bluemartini.rest.Authenticate;
import com.bluemartini.rest.BaseResource;
import com.bluemartini.rest.RESTConstants;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import java.net.URI;

/**
 * A REST API resource for Gift Registries.  This class handles request URI's whose resource path begins with 
 * <pre>/giftRegistries</pre>.  XML and JSON are the default media types supported.
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * @author Stephanie Kroll
 */
@Authenticate
@Path("/giftRegistries")
public class GiftRegistryResource extends BaseResource {
    
    // the valid expand fields for this resource
    public static DNAStringArray EXPAND_FIELDS = new DNAStringArray(new String[]{"items"});
    
    // class name for logging
    private static final String CLASS_NAME = "GiftRegistryResource";
    
    // The name of this resource
    private static final String RESOURCE_NAME = "GiftRegistry";
    
    // error code for a BMException that indicates a duplicate object path
    private static final String OBJECT_PATH_CONFLICT = "OBJECT_PATH_CONFLICT";
    
    /**
     * Returns the name of this resource
     * 
     * @return      a String containing the resource name
     * @see com.bluemartini.rest.BaseResource#getResourceName()
     */
    @Override
    public String getResourceName() {
        return RESOURCE_NAME;
    }

    /**
     * Endpoint method for a GET request to the <b>/giftRegistries/{giftRegistryId}</b> resource path.
     * Returns the Gift Registry resource whose object ID matches the specified <code>giftRegistryId</code>.
     * 
     * @param giftRegistryId    the Long object ID of Gift Registry to return
     * @param expand            optional - Gift Registry field names to be expanded as CSV string.  Valid values are 
     *                              <ul><li>items</li></ul>
     * @return                  the Gift Registry, or a status 404 if not found
     * @throws BMException      if an error occurs calling into the Commerce API
     * @throws WebApplicationException
     *                          the REST API request can't be completed and an HTTP error status is being returned
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{giftRegistryId}")
    public Response getGiftRegistry(
            @PathParam("giftRegistryId") Long giftRegistryId,
            @QueryParam("expand") String expand) throws BMException {
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "getGiftRegistry", 
                    "enter giftRegistryId=" + giftRegistryId);
        }
        
        preProcessRequest();
        
        // Call the business logic
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        GiftRegistryRepresentation giftRegistry = util.getGiftRegistry(giftRegistryId, expand);
        if (giftRegistry == null) {
            throw webAppException(Status.NOT_FOUND, "RESTAPI_GIFTREGISTRY_NOT_FOUND", 
                    "objectId", giftRegistryId.toString());
        }
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE, CLASS_NAME, "getGiftRegistry", 
                        "response data=" + giftRegistry.toString());
            }
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "getGiftRegistry", "exit success");
        }
        
        // Return response
        return Response.ok(giftRegistry).build();
    }

    /**
     * Endpoint method for a POST request to the <b>/giftRegistries</b> resource path.
     * Creates the Gift Registry resource using the data in the associated request body.
     * 
     * @param giftRegistry  a {@link GiftRegistryRepresention} with the values for the new Gift Registry 
     * @return              a status 201 Created with the URI to the new resource in the HTTP 'Location' header
     * @throws BMException  if an error occurs calling into the Commerce API
     * @throws WebApplicationException
     *                      the REST API request can't be completed and an HTTP error status is being returned
     */
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response createGiftRegistry(GiftRegistryRepresentation giftRegistry) throws BMException {
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "createGiftRegistry", "enter");
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE, CLASS_NAME, "createGiftRegistry", 
                        "request data=" + giftRegistry != null ? giftRegistry.toString() : null);
            }
        }
        
        preProcessRequest();
        
        // Validate
        // - should not have a resourceId before it is created
        if (giftRegistry.getResourceID() != null) {
            throw webAppException(Status.BAD_REQUEST, "RESTAPI_ID_NOT_ALLOWED");
        }
        
        // Assign gift registry being created to current authenticated user - security needs to be enabled
        // for this method to succeed.
        Long userId = WebServiceContext.getContext().getLoginID();
        if (userId == null) {
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG, CLASS_NAME, "createGiftRegistry", 
                        "A user ID for the request was not found. Is authentication enabled?");
            }
            throw webAppException(Status.INTERNAL_SERVER_ERROR, "RESTAPI_UNEXPECTED_CONDITION_ERROR");
        }
        
        // Call the business logic
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        Long grh_id = null;
        try {
            grh_id = util.createGiftRegistry(giftRegistry, userId);
        } catch (BMException bme) {
            if (OBJECT_PATH_CONFLICT.equals(bme.getError())) {
                // Path of gift registry being created conflicts with an existing gift registry - probably the name.
                // This is a client error so need to send back a 409 instead of a 500
                throw webAppException(Status.CONFLICT, "RESTAPI_GIFTREGISTRY_NAME_CONFLICT");
            } else {
                throw bme;
            }
        }
        
        // Send back a "201 Created" with the location of the new resource
        URI location = uriInfo_.getAbsolutePathBuilder().path(grh_id.toString()).build();
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "createGiftRegistry", 
                    "exit giftRegistryId=" + grh_id);
        }
        
        return Response.created(location).build();
    }

    /**
     * Endpoint method for a PUT request to the <b>/giftRegistries/{giftRegistryId}</b> resource path.
     * Updates the Gift Registry resource whose object ID matches the specified <code>giftRegistryId</code> using 
     * the data in the associated request body.
     * 
     * @param giftRegistryId    the Long object ID of Gift Registry to update
     * @param giftRegistry      a {@link GiftRegistryRepresention} with the new values for the Gift Registry 
     * @return                  the updated Gift Registry with a status 200 OK
     * @throws BMException      if an error occurs calling into the Commerce API
     * @throws WebApplicationException
     *                          if the REST API request can't be completed and an HTTP error status is being returned
     */
    @PUT
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{giftRegistryId}")
    public Response updateGiftRegistry(
            @PathParam("giftRegistryId") Long giftRegistryId,
            GiftRegistryRepresentation giftRegistry) throws BMException {
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "updateGiftRegistry", 
                    "enter giftRegistryId=" + giftRegistryId);
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE, CLASS_NAME, "updateGiftRegistry", 
                        "request data=" + giftRegistry != null ? giftRegistry.toString() : null);
            }
        }
        
        preProcessRequest();
        
        // Validate giftRegistryId...
        // - matches the resource ID of the passed-in resource being updated
        if (giftRegistry.getResourceID() == null) {
            giftRegistry.setResourceID(giftRegistryId.toString());
        } else if (!giftRegistry.getResourceID().equals(giftRegistryId.toString())) {
            throw webAppException(Status.CONFLICT, "RESTAPI_ID_MISMATCH");
        }
        // - gift registry with the specified ID exists
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        if (!util.giftRegistryExists(giftRegistryId)) {
            throw webAppException(Status.NOT_FOUND, "RESTAPI_GIFTREGISTRY_NOT_FOUND", "objectId", 
                    giftRegistryId.toString());
        }
        // Verify user is owner of this gift registry - security needs to be enabled for this method to succeed.
        Long userId = WebServiceContext.getContext().getLoginID();
        if (userId == null) {
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG, CLASS_NAME, "updateGiftRegistry", 
                        "A user ID for the request was not found. Is authentication enabled?");
            }
            throw webAppException(Status.INTERNAL_SERVER_ERROR, "RESTAPI_UNEXPECTED_CONDITION_ERROR");
        }
        if (!util.isGiftRegistryOwnedBy(giftRegistryId, userId)) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_UPDATE");
        }
        
        // Call the business logic
        try {
            giftRegistry = util.updateGiftRegistry(giftRegistry);
        } catch (BMException bme) {
            if (OBJECT_PATH_CONFLICT.equals(bme.getError())) {
                // Path of gift registry being updated conflicts with an existing gift registry - probably the name.
                // This is a client error so need to send back a 409 instead of a 500
                throw webAppException(Status.CONFLICT, "RESTAPI_GIFTREGISTRY_NAME_CONFLICT");
            } else {
                throw bme;
            }
        }
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "updateGiftRegistry", "exit success");
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_VERBOSE, CLASS_NAME, "updateGiftRegistry", 
                        "response data=" + giftRegistry.toString());
            }
        }
        
        // Return the response
        return Response.ok(giftRegistry).build();
    }
    
    /**
     * Endpoint method for a DELETE request to the <b>/giftRegistries/{giftRegistryId}</b> resource path.
     * Deletes the Gift Registry resource whose object ID matches the specified <code>giftRegistryId</code>.
     * 
     * @param giftRegistryId    the Long object ID of Gift Registry to delete
     * @return                  an empty response with a status 204 No Content if successfully deleted
     * @throws BMException      if an error occurs calling into the Commerce API
     * @throws WebApplicationException
     *                          if the REST API request can't be completed and an HTTP error status is being returned     
     */
    @DELETE
    @Path("{giftRegistryId}")
    public Response deleteGiftRegistry(@PathParam("giftRegistryId") Long giftRegistryId) throws BMException {
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "deleteGiftRegistry", 
                    "enter giftRegistryId=" + giftRegistryId);
        }
        
        preProcessRequest();
        
        // Validate that gift registry with the specified ID exists
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        if (!util.giftRegistryExists(giftRegistryId)) {
            throw webAppException(Status.NOT_FOUND, "RESTAPI_GIFTREGISTRY_NOT_FOUND", "objectId", 
                    giftRegistryId.toString());
        }
        // Verify user is owner of this gift registry - security needs to be enabled for this method to succeed.
        Long userId = WebServiceContext.getContext().getLoginID();
        if (userId == null) {
            if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG)) {
                BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG, CLASS_NAME, "deleteGiftRegistry", 
                        "A user ID for the request was not found. Is authentication enabled?");
            }
            throw webAppException(Status.INTERNAL_SERVER_ERROR, "RESTAPI_UNEXPECTED_CONDITION_ERROR");
        }
        if (!util.isGiftRegistryOwnedBy(giftRegistryId, userId)) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_DELETE");
        }
        
        // Call the business logic
        util.deleteGiftRegistry(giftRegistryId);
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, CLASS_NAME, "deleteGiftRegistry", "exit success");
        }
        
        return Response.noContent().build();
    }
    
    /**
     * Subresource locator method for a request to the <b>/giftRegistries/{giftRegistryId}/items</b> resource path.
     * Delegates the request to the {@link GiftRegistryItemResource} for handling.
     * 
     * @param giftRegistryId    the Long object ID of the Gift Registry
     * @return                  the appropriate REST {@link Response} as determined by GiftRegistryItemResource
     * @throws BMException      if an error occurs calling into the Commerce API
     * @throws WebApplicationException
     *                          the REST API request can't be completed and an HTTP error status is being returned
     */
    @Path("{giftRegistryId}/items")
    public GiftRegistryItemResource handleGiftRegistryItemRequest(@PathParam("giftRegistryId") Long giftRegistryId) 
            throws BMException {
        
        // Verify a gift registry with the specified ID exists
        RESTGiftRegistryUtil util = new RESTGiftRegistryUtil(this);
        if (!util.giftRegistryExists(giftRegistryId)) {
            throw webAppException(Status.NOT_FOUND, "RESTAPI_GIFTREGISTRY_NOT_FOUND", "objectId", 
                    Long.toString(giftRegistryId));
        }
        return new GiftRegistryItemResource(request_, uriInfo_, giftRegistryId);
    }
}
