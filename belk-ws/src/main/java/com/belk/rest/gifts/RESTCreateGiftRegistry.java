package com.belk.rest.gifts;

import com.bluemartini.dna.BusinessObject;
import com.bluemartini.dna.DNAList;
import com.bluemartini.html.HTMLResponse;
import com.bluemartini.webconnect.WebConnect;
import com.bluemartini.webconnect.htmlapp.WebConnectCreateGiftRegistry;

/**
 * Extends the {@link WebConnectCreateGiftRegistry} business action to set the newly-created 
 * GIFT_REGISTRY_HEADER on the response.
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * @author Stephanie Kroll
 */
public class RESTCreateGiftRegistry extends WebConnectCreateGiftRegistry {
    
    /**
     * Overrides the base class to set the GIFT_REGISTRY_HEADER for the created Gift Registry onto the response.
     * 
     * @param htmlResponse  the {@link HTMLResponse} object being returned from this business action
     * @param dnaFormData   DNAList containing the User Account bizobj (WebConnect.SESSION_CURRENT_USER) and the
     *                          Gift Registry Header bizobj (WebConnect.SESSION_GIFT_REGISTRY_HEADER)
     * @return              the updated HTMLResponse object 
     * 
     * @see com.bluemartini.webconnect.htmlapp.WebConnectCreateGiftRegistry#updateHTMLResponse(com.bluemartini.html.HTMLResponse, com.bluemartini.dna.DNAList)
     */
    @Override
    protected HTMLResponse updateHTMLResponse(HTMLResponse htmlResponse, DNAList dnaFormData) {

        BusinessObject boUser = dnaFormData.removeBusinessObject(WebConnect.SESSION_CURRENT_USER);
        BusinessObject boGiftRegistry = dnaFormData.removeBusinessObject(WebConnect.SESSION_GIFT_REGISTRY_HEADER);
        boGiftRegistry.removeBasePair("new");
        htmlResponse.removeData(WebConnect.SESSION_GIFT_REGISTRY_HEADER);
        htmlResponse.setBusinessObjectRef(WebConnect.SESSION_CURRENT_USER, boUser);
        htmlResponse.setBusinessObjectRef(WebConnect.SESSION_GIFT_REGISTRY_HEADER, boGiftRegistry);
        return htmlResponse;
    }
}
