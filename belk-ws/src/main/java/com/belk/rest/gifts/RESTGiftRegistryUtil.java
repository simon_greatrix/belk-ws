package com.belk.rest.gifts;

import com.bluemartini.dna.*;
import com.bluemartini.html.WebServiceContext;
import com.bluemartini.htmlapp.*;
import com.bluemartini.rest.*;
import com.bluemartini.server.BMClient;
import com.bluemartini.webconnect.WebConnect;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

/**
 * Provides utility methods for working with Gift Registries and calling the Commerce API in the context 
 * of REST-based web services.
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * @author Stephanie Kroll
 */
public class RESTGiftRegistryUtil extends RESTUtil {
    
    // class name for logging
    private static final String CLASS_NAME = "RESTGiftRegistryUtil";

    /**
     * Constructs a new {@link RESTGiftRegistryUtil} for the supplied {@link Resource}.
     * 
     * @param resource      the Resource creating this instance - may not be null
     * @throws IllegalArgumentException 
     *                      if <code>resource</code> is null
     */
    public RESTGiftRegistryUtil(Resource resource) {
        super(resource);
    }
    
    /**
     * Returns an indicator as to whether a Gift Registry with the specified ID exists.
     * 
     * @param giftRegistryId    the Long Id of the Gift Registry
     * @return                  true if the Gift Registry exists, false otherwise
     * @throws BMException      if an error occurs calling into the Commerce API
     */
    public boolean giftRegistryExists(Long giftRegistryId) throws BMException {
        return HTMLUserAccountUtil.getGiftRegistryHeader(giftRegistryId) != null;
    }
    
    /**
     * Returns an indicator as to whether the Gift Registry with the specified <em>giftRegistryId</em> 
     * belongs to the user with the specified <em>userId</em>.
     * 
     * @param giftRegistryId    the Long ID of the Gift Registry
     * @param userId            the Long ID of the user
     * @return                  true if the Gift Registry is owned by the user, false otherwise or if no Gift Registry
     *                              exists with the specified ID
     * @throws BMException      if an error occurs calling into the Commerce API
     */
    public boolean isGiftRegistryOwnedBy(Long giftRegistryId, Long userId) throws BMException {
        BusinessObject boRegistry = HTMLUserAccountUtil.getGiftRegistryHeader(giftRegistryId);
        if (boRegistry == null || userId == null) return false;
        Long userAccountId = boRegistry.getLong("grh_parent_id");
        return userId.equals(userAccountId);
    }
    
    /**
     * Returns an indicator as to whether a SKU with the specified ID exists.
     * 
     * @param skuId             the Long Id of the SKU
     * @return                  true if the SKU exists, false otherwise
     * @throws BMException      if an error occurs calling into the Commerce API
     */
    public boolean skuExists(Long skuId) throws BMException {
        Sku sku = HTMLProductUtil.getSkuDetail(skuId);        
        return sku!=null && sku.getSkuCode()!=null;
    }

    /**
     * Returns a {@link GiftRegistryRepresentation} for the Gift Registry whose object ID matches the specified
     * <em>giftRegistryId</em>.
     * 
     * @param giftRegistryId    the Long object ID of the Gift Registry
     * @param expand            a String containing the comma-separated field names to be expanded; may be null
     * @return                  the GiftRegistryRepresentation, or null if a Gift Registry with the specified ID is
     *                              not found
     * @throws BMException      if an error occurs calling into the Commerce API
     */
    public GiftRegistryRepresentation getGiftRegistry(Long giftRegistryId, String expand) throws BMException {
        BusinessObject boRegistry = HTMLUserAccountUtil.getGiftRegistryHeader(giftRegistryId);
        if (boRegistry == null) return null;
        return getGiftRegistry(boRegistry, expand, false);
    }
    
    /**
     * Returns a {@link GiftRegistryRepresentation} for the supplied Gift Registry business object.
     * 
     * @param boRegistry    a {@link BusinessObject} of type GIFT_REGISTRY_HEADER
     * @param expand        a String containing the comma-separated field names to be expanded; may be null
     * @param addSelfId     a boolean indicator for constructing the self link - pass true to add this object's ID 
     *                          to the self link, false if the current path already contains this object's ID
     * @return              the GiftRegistryRepresentation
     * @throws BMException  if an error occurs calling into the Commerce API
     */
    public GiftRegistryRepresentation getGiftRegistry(
            BusinessObject boRegistry, String expand, boolean addSelfId) throws BMException {
        
        GiftRegistryRepresentation giftRegistry = new GiftRegistryRepresentation(boRegistry);
        
        // Build the self link
        UriBuilder selfBuilder = uriInfo_.getAbsolutePathBuilder();
        if (addSelfId) {
            selfBuilder.path(giftRegistry.getResourceID());
        }
        
        // Optional fields expansion
        DNAStringArray expandParams = parseExpandParams(expand, GiftRegistryResource.EXPAND_FIELDS);
        if (expandParams.contains("items")) {
            // In-line the "items" sub-resource
            giftRegistry.items_ = getGiftRegistryItems(boRegistry.getLongID(), 
                    RESTConstants.DEFAULT_PAGE_OFFSET, RESTConstants.DEFAULT_PAGE_LIMIT, true);
        } else {
            // Include a link to the "items" sub-resource
            giftRegistry.setItems(uriInfo_, selfBuilder.clone().path("items").build().toString());
        }
        appendExpandParams(selfBuilder, expandParams);
        
        // Set the self link
        giftRegistry.setSelf(selfBuilder.build().toString());
        
        // add the "actions" HATEOAS links, but only if the caller is the owner
        Long userId = WebServiceContext.getContext().getLoginID();
        if (isGiftRegistryOwnedBy(boRegistry.getLongID(), userId)) {
            giftRegistry.addActionLink(getUpdateLink(giftRegistry.getResourceID()));
            giftRegistry.addActionLink(getDeleteLink(giftRegistry.getResourceID()));   
        }
        
        // Set the registry owner details
        Long userAccountId = boRegistry.getLong("grh_parent_id");
        if (userAccountId != null) {
            giftRegistry.setRegistryOwner(HTMLUserAccountUtil.getUserAccount(userAccountId));
        }
        
        return giftRegistry;
    }

    /**
     * Returns a collection of Gift Registry line items for the gift registry whose object ID matches the specified
     * <em>giftRegistryId</em>. 
     * 
     * @param giftRegistryId    the Long Id of the Gift Registry
     * @param offset            an int containing the starting index for the page of resources to be returned - must 
     *                              be positive
     * @param limit             an int containing the size of the page of resources to be returned - must be 
     *                              greater than zero
     * @param inLined           a boolean - pass true to indicate this collection of lines is being in-lined within 
     *                              the parent resource
     * @return                  a {@link GiftRegistryItemCollectionRepresentation}  containing the line items for 
     *                              the Gift Registry
     * @throws BMException      if <code>giftRegistryId</code> is null or an error occurs calling into the Commerce API
     */
    public GiftRegistryItemCollectionRepresentation getGiftRegistryItems(Long giftRegistryId,
            int offset, int limit, boolean inLined) throws BMException {
        verifyParamNotNull(giftRegistryId, "giftRegistryId");
        
        GiftRegistryItemCollectionRepresentation giftRegistryItems = new GiftRegistryItemCollectionRepresentation();
        giftRegistryItems.setOffset(offset);
        giftRegistryItems.setLimit(limit);
        
        // If this collection is being in-lined within a GiftRegistry resource, then the request URI did
        // not contain "/items" as part of the path, so we need to add it to build the links.
        UriBuilder baseBuilder = uriInfo_.getAbsolutePathBuilder();
        if (inLined) {
            baseBuilder = baseBuilder.path("items");
        }
        
        // Retrieve the Gift Registry lines
        BusinessObjectArray boaLines = HTMLUserAccountUtil.getGiftRegistryLines(giftRegistryId);
        if (boaLines != null) {
            for (int ii = offset; ii < boaLines.size() && ii < (offset + limit); ii++) {
                GiftRegistryItemRepresentation giftRegistryItem = getGiftRegistryItem(
                        giftRegistryId, boaLines.elementAt(ii), baseBuilder.clone(), null, true);
                giftRegistryItems.addGiftRegistryItem(giftRegistryItem);
            }
            giftRegistryItems.setTotalCount(boaLines.size());
        }
        
        // Set the self, previous, and next links for the collection (if appropriate)
        giftRegistryItems.setSelf(buildSelfLink(baseBuilder, offset, limit));
        giftRegistryItems.setPrevious(buildPreviousLink(baseBuilder, offset, limit));
        giftRegistryItems.setNext(buildNextLink(baseBuilder, offset, limit, giftRegistryItems.getTotalCount()));
        
        return giftRegistryItems;
    }
    
    /**
     * Returns a {@link GiftRegistryItemRepresentation} for the Gift Registry Line whose object ID matches the 
     * specified <code>itemId</code> and that has as its parent a Gift Registry with the specified 
     * <code>giftRegistryId</code>.
     * 
     * @param giftRegistryId    the Long Id of the parent Gift Registry
     * @param itemId            the Long Id of the Gift Registry Line
     * @param uriBuilder        the {@link UriBuilder) for building the self link     
     * @param addSelfId         a boolean - pass true to add this object's ID to the self link, false to use the 
     *                              builder's current path for self
     * @return                  the GiftRegistryItemRepresentation for the Gift Registry Line, or null if a
     *                              Gift Registry or Gift Registry Line with specified IDs could not be found.
     * @throws BMException      if an error occurs calling into the Commerce API
     */
    public GiftRegistryItemRepresentation getGiftRegistryItem(Long giftRegistryId, Long itemId,
            UriBuilder uriBuilder, String expand, boolean addSelfId) throws BMException {
        
        BusinessObject boRegistryItem = getGiftRegistryLine(giftRegistryId, itemId);
        return getGiftRegistryItem(giftRegistryId, boRegistryItem, uriBuilder, expand, addSelfId);
    }
    
    /**
     *  Returns a {@link GiftRegistryItemRepresentation} for the supplied Gift Registry Line business object.
     *  
     * @param giftRegistryId    the Long Id of the parent Gift Registry
     * @param boRegistryItem    a {@link BusinessObject} of type GIFT_REGISTRY_LINE
     * @param uriBuilder        the {@link UriBuilder) for building the self link
     * @param addSelfId         a boolean - pass true to add this object's ID to the self link, false to use the 
     *                              builder's current path for self
     * @return                  the GiftRegistryItemRepresentation for the Gift Registry Line, or null if 
     *                              <code>boRegistryLine</code> is null
     * @throws BMException      if an error occurs calling into the Commerce API
     */
    public GiftRegistryItemRepresentation getGiftRegistryItem(Long giftRegistryId, BusinessObject boRegistryLine, 
            UriBuilder uriBuilder, String expand, boolean addSelfId) throws BMException {
        
        if (boRegistryLine == null) return null;
        GiftRegistryItemRepresentation giftRegistryItem = new GiftRegistryItemRepresentation(boRegistryLine);
        
        // The object ID for a gift registry line is not a unique identifier so create a resource ID that is
        // a concatenation of the parent header ID and this line.
        String giftRegistryItemId = boRegistryLine.getLongID().toString();
        giftRegistryItem.setResourceID(giftRegistryId + RESTConstants.RESOURCE_PROPERTY_SEPARATOR + giftRegistryItemId);
        
        // Build the self link
        if (addSelfId) {
            uriBuilder.path(giftRegistryItemId);
        }
        
        // Optional fields expansion
        DNAStringArray expandParams = parseExpandParams(expand, GiftRegistryItemResource.EXPAND_FIELDS);
        
        // We only include the assignment options and the modification action links if the current authenticated user
        // is the owner of this gift registry.  If not, then they are not allowed to make modifications.
        Long userId = WebServiceContext.getContext().getLoginID();
        if (isGiftRegistryOwnedBy(giftRegistryId, userId)) {
            appendExpandParams(uriBuilder, expandParams);   
            
            // Add the "actions" HATEOAS links for update and delete
            giftRegistryItem.addActionLink(getUpdateLink(giftRegistryItemId));
            giftRegistryItem.addActionLink(getDeleteLink(giftRegistryItemId));
        }
        
        // Set the self link
        giftRegistryItem.setSelf(uriBuilder.build().toString());
        
        // Add details about the Sku referenced by the line
        Long skuId = boRegistryLine.getLong("grg_sku_id");
        if (skuId != null) {
            Sku sku = HTMLProductUtil.getSkuDetail(skuId);
            if (sku != null) {
                giftRegistryItem.setSku(sku);
                
                // get the price
                DNAList dnaAppConfig = BMContext.getAppConfig().getList("app");
                String sDefaultFolder = null;
                if (dnaAppConfig != null) {
                    sDefaultFolder = dnaAppConfig.getString("default_price_folder");
                }
                if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG)) {
                    BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_DEBUG, CLASS_NAME, "getGiftRegistryItem", 
                            "default_price_folder = " + sDefaultFolder);
                }
                PriceList priceList = HTMLPriceListUtil.getPriceListByName(sDefaultFolder);
                if (priceList != null) {
                    Currency price = HTMLProductUtil.getProductSalePrice(sku.getProduct().getLongID(), priceList);
                    giftRegistryItem.setSalePriceAsCurrency(price);
                }
                
                // get the small or thumbnail image
                Content image = HTMLContentUtil.getContentDetail(sku.getLong("ATR_Prd_Image_Small_en"));
                if (image != null) {
                    // url will be relative or absolute depending on "produce_absolute_media_urls" in appconfig
                    giftRegistryItem.imageSmallUrl_ = image.getPath();
                }
            }
        }
        
        return giftRegistryItem;
    }
    
    /**
     * Finds and returns the Gift Registry Line business object for the specified <code>giftRegistryId</code> and
     * <code>itemId</code>.
     * 
     * @param giftRegistryId    the Long Id of the parent Gift Registry
     * @param itemId            the Long Id of the Gift Registry Line
     * @return                  a {@link BusinessObject} of type GIFT_REGISTRY_LINE, or null if not found
     * @throws BMException      if an error occurs calling into the Commerce API
     */
    public BusinessObject getGiftRegistryLine(Long giftRegistryId, Long itemId) throws BMException {
        
        // Find the Gift Registry Line bizobj
        BusinessObject boRegistryItem = null;
        if (itemId != null) {
            BusinessObjectArray boaLines = HTMLUserAccountUtil.getGiftRegistryLines(giftRegistryId);
            if (boaLines != null) {
                for (int ii = 0; ii < boaLines.size(); ii++) {
                    BusinessObject boLine = boaLines.elementAt(ii);
                    if (itemId.equals(boLine.getLongID())) {
                        boRegistryItem = boLine;
                        break;
                    }
                }
            }            
        }
        return boRegistryItem;
    }
    
    /**
     * Creates a Gift Registry using the values as supplied in <code>giftRegistry</code> and owned by the user
     * designated by <code>userId</code>.  The name supplied for the Gift Registry must be unique for the user.
     * 
     * @param giftRegistry      a {@link GiftRegistryRepresention} with the values for the new Gift Registry
     * @param userId            the Long ID for the user who is to be the owner of the new Gift Registry
     * @return                  the Long ID of the new Gift Registry created
     * @throws BMException      if <code>giftRegistry</code> is null, or <code>userId</code> is null or is not the
     *                              the ID of a valid user, or an error occurs calling into the Commerce API
     */
    public Long createGiftRegistry(GiftRegistryRepresentation giftRegistry, Long userId) throws BMException {
        verifyParamNotNull(giftRegistry, "giftRegistry");
        verifyParamNotNull(userId, "userId");
        
        // Prepare call to the create bizact
        BusinessObject boRegistry = giftRegistry.getBusinessObject();
        DNAList dnaIn = new DNAList();
        BusinessObject boUserAccount = HTMLUserAccountUtil.getUserAccount(userId);
        verifyParamNotNull(boUserAccount, "User Account");
        dnaIn.setBusinessObjectRef(WebConnect.SESSION_CURRENT_USER, boUserAccount);
        boRegistry.setLong("grh_uad_id", boUserAccount.getAsLong("usa_bill_to_uad_id"));
        dnaIn.setBusinessObjectRef(WebConnect.SESSION_GIFT_REGISTRY_HEADER, boRegistry);
        
        // Do the create.
        DNAList dnaResponse = WebServiceContext.getContext().executeHTMLBusinessAction("RESTCreateGiftRegistry", dnaIn);
        boRegistry = dnaResponse.getBusinessObject(WebConnect.SESSION_GIFT_REGISTRY_HEADER);
        return boRegistry.getLongID();
    }
    
    /**
     * Creates a Gift Registry line item for the Gift Registry specified by <code>giftRegistryId</code> with the 
     * specified values for <code>skuId</code> and <code>qtyDesired</code>.  The value for
     * <code>qtyDesired</code> must be positive or it defaults to one.
     * 
     * @param giftRegistryId    a Long with the object ID of the parent Gift Registry
     * @param skuId             a Long with the object ID of the Sku to add to the line
     * @param qtyDesired        an Integer with the quantity desired of the Sku
     * @return                  the Long ID of the new Gift Registry line item that is created
     * @throws BMException      if <code>giftRegistryId</code> is null, <code>skuId</code> is null, or an error 
     *                              occurs calling into the Commerce API
     */
    public Long createGiftRegistryItem(Long giftRegistryId, Long skuId, Integer qtyDesired) throws BMException {
        verifyParamNotNull(giftRegistryId, "giftRegistryId");
        verifyParamNotNull(skuId, "skuId");
        if (qtyDesired == null || qtyDesired.intValue() <= 0) qtyDesired = 1;
        
        // Prepare the call to the create bizact
        BusinessObject boRegistry = HTMLUserAccountUtil.getGiftRegistryHeader(giftRegistryId);
        BusinessObject boAddCartItem = BMContext.createBusinessObject("CART_ITEM");
        boAddCartItem.setLong("sku_id", skuId);
        boAddCartItem.setInteger("quantity", qtyDesired);
        DNAList dnaIn = new DNAList();
        dnaIn.setBusinessObject(WebConnect.SESSION_GIFT_REGISTRY_HEADER, boRegistry);
        dnaIn.setBusinessObject(WebConnect.SESSION_ADD_CART_ITEM, boAddCartItem);
        
        // Do the create
        DNAList dnaResponse = WebServiceContext.getContext().executeHTMLBusinessAction(
                "RESTAddToGiftRegistry", dnaIn);
        BusinessObject boRegistryLine = dnaResponse.getBusinessObject(WebConnect.SESSION_GIFT_REGISTRY_LINE);
        if (boRegistryLine == null) {
            // Should get a line back, no line is an unexpected error
            throw new BMException("RESTAPI_UNKNOWN_ERROR");
        }
        
        return boRegistryLine.getLongID();
    }
    
    /**
     * Updates the supplied Gift Registry.
     * 
     * @param giftRegistry      a {@link GiftRegistryRepresention} with the new values for the Gift Registry 
     * @return                  the updated GiftRegistryRepresentation
     * @throws BMException      if <code>giftRegistry</code> is null or an error occurs calling into the Commerce API
     */
    public GiftRegistryRepresentation updateGiftRegistry(GiftRegistryRepresentation giftRegistry) throws BMException {
        verifyParamNotNull(giftRegistry, "giftRegistry");
        
        // Prepare call to the update bizact (a WebConnect HTML bizact)
        BusinessObject boRegistry = giftRegistry.getBusinessObject();
        Long grh_id = Long.valueOf(giftRegistry.getResourceID());
        boRegistry.setLongID(grh_id);
        DNAList dnaIn = new DNAList();
        dnaIn.setBusinessObjectRef(WebConnect.SESSION_GIFT_REGISTRY_HEADER, boRegistry);
        
        // Do the update. Ignore the update response as we will re-retrieve and return the updated GiftRegistry
        WebServiceContext.getContext().executeHTMLBusinessAction("WebConnectUpdateGiftRegistry", dnaIn);
        return getGiftRegistry(grh_id, null);
    }
    
    /**
     * Updates the supplied Gift Registry Item.  Only the <code>qtyDesired</code>, <code>qtyReceived</code>, and
     * <code>message</code> properties of a line item can be updated through this method.
     * 
     * @param giftRegistryItem  a {@link GiftRegistryItemRepresentation} with the new values for the line.
     * @param giftRegistryId    the Long Id of the parent Gift Registry
     * @return                  the updated GiftRegistryItemRepresenation
     * @throws BMException      if <code>giftRegistryItem</code> is null or an error occurs 
     *                              calling into the Commerce API
     */
    public GiftRegistryItemRepresentation updateGiftRegistryItem(GiftRegistryItemRepresentation giftRegistryItem,
            Long giftRegistryId) throws BMException {
        verifyParamNotNull(giftRegistryItem, "giftRegistryItem");
        
        // Prepare the call to the update bizact.
        BusinessObject boRegistryLine = giftRegistryItem.getBusinessObject();
        Long grg_id = Long.valueOf(giftRegistryItem.getResourceID());
        boRegistryLine.setLongID(grg_id);
        DNAList dnaIn = new DNAList();
        BusinessObjectArray boaLines = dnaIn.setBusinessObjectArray(WebConnect.SESSION_GIFT_REGISTRY_LINES);
        boaLines.add(boRegistryLine);
        
        // Do the update. Ignore the update response as we will re-retrieve and return the updated GiftRegistryItem
        WebServiceContext.getContext().executeHTMLBusinessAction("WebConnectUpdateGiftRegistry", dnaIn);
        return getGiftRegistryItem(giftRegistryId, grg_id, uriInfo_.getAbsolutePathBuilder(), null, false);
    }
    
    /**
     * Deletes the specified Gift Registry.
     * 
     * @param giftRegistryId    the Long ID of the Gift Registry to delete - must not be null
     * @throws BMException      if <em>giftRegistryId</em> is null or an error occurs calling into the Commerce API
     */
    public void deleteGiftRegistry(Long giftRegistryId) throws BMException {
        verifyParamNotNull(giftRegistryId, "giftRegistryId");
               
        // Prepare call to the delete bizact.
        // WebConnectDeleteGiftRegistry expects the ID of the registry to be in a Long array set
        // on an otherwise unused GIFT_REGISTRY_HEADER
        BusinessObject boRegistry = BMContext.createBusinessObject("GIFT_REGISTRY_HEADER");
        DNALongArray selectRegistryList = new DNALongArray();
        selectRegistryList.add(giftRegistryId);
        boRegistry.setLongArrayRef("selectRegistryList", selectRegistryList);
        DNAList dnaIn = new DNAList();
        dnaIn.setBusinessObjectRef(WebConnect.SESSION_GIFT_REGISTRY_HEADER, boRegistry);
        
        // Do the delete.
        WebServiceContext.getContext().executeHTMLBusinessAction("WebConnectDeleteGiftRegistry", dnaIn);
    }
    
    /**
     * Deletes the supplied Gift Registry Line.
     * 
     * @param boRegistryLine    a BusinessObject of type GIFT_REGISTRY_LINE to delete - must not be null
     * @throws BMException      if <em>boRegistryLine</em> is null or an error occurs calling into the Commerce API
     */
    public void deleteGiftRegistryItem(BusinessObject boRegistryLine) throws BMException {
        verifyParamNotNull(boRegistryLine, "boRegistryLine");
        
        // Prepare the call to the bizact.
        // Lines are deleted by doing an update with the lines to be deleted marked with "remove"
        boRegistryLine.setBoolean("remove", true);
        DNAList dnaIn = new DNAList();
        BusinessObjectArray boaLines = dnaIn.setBusinessObjectArray(WebConnect.SESSION_GIFT_REGISTRY_LINES);
        boaLines.add(boRegistryLine);
        
        // Do the delete
        WebServiceContext.getContext().executeHTMLBusinessAction("WebConnectUpdateGiftRegistry", dnaIn);
    }

    /**
     * Assigns a line item to a different Gift Registry by deleting the line from its current parent and creating a 
     * new line for the target Gift Registry.
     * 
     * @param boRegistryLine    the Gift Registry line item to re-assign
     * @param targetRegistryId  the Gift Registry to assign the line item to
     * @return                  the Long ID of the line item in it's new Gift Registry
     * @throws BMException      if <em>boRegistryLine</em> or <em>targetRegistryId</em> is null, or an error 
     *                              occurs calling into the Commerce API
     */
    public Long assignGiftRegistryItem(BusinessObject boRegistryLine, Long targetRegistryId) throws BMException {
        verifyParamNotNull(boRegistryLine, "boRegistryLine");
        verifyParamNotNull(targetRegistryId, "targetRegistryId");
        
        // Add the line to the target gift registry
        boRegistryLine.setLong("grg_grh_id", targetRegistryId);
        DNAList dnaParams = new DNAList();
        dnaParams.setBusinessObject(WebConnect.SESSION_GIFT_REGISTRY_LINE, boRegistryLine);
        DNAList dnaResponse = BMClient.executeBusinessAction("CreateObject", dnaParams);
        BusinessObject boNewLine = dnaResponse.getBusinessObject(WebConnect.SESSION_GIFT_REGISTRY_LINE);
        
        // Remove the line from its current parent
        deleteGiftRegistryItem(boRegistryLine);
        
        return boNewLine.getLongID();
    }
    
    /**
     * Builds and returns the HATEOAS link for creating a new resource of the associated type.
     * 
     * @return      the {@link Link} to create a new resource
     */
    public Link getCreateLink() {
        return new Link(uriInfo_, "create", 
                uriInfo_.getBaseUriBuilder().path(resource_.getBaseResourcePath()).build().toString(), 
                Link.HTTPMethod.POST, 
                MediaType.APPLICATION_JSON + "," + MediaType.APPLICATION_XML);
    }
    
    /**
     * Builds and returns the HATEOAS link for updating an associated resource with the specified resource ID.
     * 
     * @param resourceId    a String containing the ID of the resource
     * @return              the {@link Link} to update the resource
     */
    public Link getUpdateLink(String resourceId) {
        return new Link(uriInfo_, "update", 
                uriInfo_.getBaseUriBuilder().path(resource_.getBaseResourcePath()).path(resourceId).build().toString(), 
                Link.HTTPMethod.PUT, 
                MediaType.APPLICATION_JSON + "," + MediaType.APPLICATION_XML);
    }
    
    /**
     * Builds and returns the HATEOAS link for deleting an associated resource with the specified resource ID.
     * 
     * @param resourceId    a String containing the ID of the resource
     * @return              the {@link Link} to delete the resource
     */
    public Link getDeleteLink(String resourceId) {
        return new Link(uriInfo_, "delete", 
                uriInfo_.getBaseUriBuilder().path(resource_.getBaseResourcePath()).path(resourceId).build().toString(), 
                Link.HTTPMethod.DELETE);
    }
}
