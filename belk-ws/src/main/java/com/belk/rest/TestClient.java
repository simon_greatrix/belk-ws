package com.belk.rest;

import com.belk.rest.actions.Login;
import com.belk.rest.actions.Logout;

import com.belk.rest.client.*;

public class TestClient {
    public static void main(String[] args) throws Exception {
        System.out.println(ClientUtil.BASE_HTTPS_URL);
        Login login = new Login("brian","martini".toCharArray());
        BMOAuthConsumer cons = login.call();
        
        Ping ping = new Ping(cons);
        ExceptionEntity ee = ping.call();
        System.out.println(ee);
        
        Logout logout = new Logout(cons);        
        logout.call();
    }
}
