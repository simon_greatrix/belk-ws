package com.belk.rest.server;

import com.bluemartini.dna.BMException;
import com.bluemartini.rest.Authenticate;
import com.bluemartini.rest.BaseResource;
import com.bluemartini.rest.NamedResource;
import com.bluemartini.rest.RESTConstants;
import com.bluemartini.rest.server.CreditCardRepresentation;
import com.bluemartini.security.BMAccessControl;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response.Status;

@Authenticate
@Path("/cart/payment")
@NamedResource("Payment")
public class PaymentResource extends BaseResource {

    /**
     * Get the access context that should be used for checking permissions. The
     * default implementation returns null to indicate the default context.
     * 
     * @return the name of the access context.
     */
    protected String getAccessContext() {
        return null;
    }


    @POST
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("brd")
    public Object validateBRD(CreditCardRepresentation card) throws BMException {
        preProcessRequest();
        Long loggedInAs = getLoginIdSafe();

        // must be card owner or have view permission on CreditCard
        Long cardOwner = card.getCcdUsaId();
        if( cardOwner == null ) {
            card.setCcdUsaId(loggedInAs);
        } else if( !(cardOwner.equals(loggedInAs) || BMAccessControl.canView(
                "CreditCard", getAccessContext())) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_VIEW");
        }

        // Execute the following business actions
        // + BelkValidateGiftCardFields
        return null; // TODO
    }


    /**
     * The BelkValidateGiftCardFields requires a captcha response, which we do
     * not have. Hence we need to emulate it.
     * 
     * @param card the card
     * @throws BMException
     */
    private void emulateBelkValidateGiftCardFields(CreditCardRepresentation card) throws BMException {
        String cardNum = card.getCardNum();
    }
}
