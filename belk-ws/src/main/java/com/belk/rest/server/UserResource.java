package com.belk.rest.server;

import com.bluemartini.core.BMUtil;
import com.bluemartini.core.Constants;
import com.bluemartini.database.BMDatabaseManager;
import com.bluemartini.database.BMPreparedStatement;
import com.bluemartini.database.BindArray;
import com.bluemartini.database.DBUtil;
import com.bluemartini.dna.*;
import com.bluemartini.html.WebServiceContext;
import com.bluemartini.htmlapp.*;
import com.bluemartini.rest.*;
import com.bluemartini.rest.Link.HTTPMethod;
import com.bluemartini.rest.server.AddressRepresentation;
import com.bluemartini.rest.server.CreditCardRepresentation;
import com.bluemartini.rest.server.UserAccountRepresentation;
import com.bluemartini.security.BMAccessControl;
import com.bluemartini.util.UserAccountUtil;
import com.bluemartini.webconnect.WebConnect;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.net.URI;
import java.util.List;

@Authenticate
@Path("user")
@NamedResource("User")
public class UserResource extends BaseResource {

    /** Query to test if a user exists */
    private static final String SQL_USER_EXISTS = "SELECT COUNT(*) FROM OBJECT, USER_ACCOUNT\n"
            + "WHERE OBJ_ID=USA_ID AND OBJ_TYPE=8 AND USA_TYPE_CD='B2C' AND OBJ_STATUS_CD<>'D'\n"
            + "AND OBJ_NM_LOWER=? AND OBJ_PARENT_ID=?";

    /** Set the billing and shipping addresses for a user */
    private static final String SQL_UPDATE_USER_ADDR = "UPDATE USER_ACCOUNT SET\n"
            + "USA_BILL_TO_UAD_ID=?,\n"
            + "USA_SHIP_TO_UAD_ID=?\n"
            + "WHERE USA_ID=?";


    private void addHATEOAS(AddressRepresentation rep) {
        Long uadId = rep.getUadId();
        Long usaId = rep.getUadUsaId();
        URI uri = uriInfo_.getAbsolutePathBuilder().path(String.valueOf(usaId)).path(
                "address").path(String.valueOf(uadId)).build();
        rep.setSelf(uri.toString());
        rep.addActionLink(new Link(null, "get", uri.toString(), HTTPMethod.GET));
        rep.addActionLink(new Link(null, "delete", uri.toString(),
                HTTPMethod.DELETE));
    }


    private void addHATEOAS(CreditCardRepresentation rep) {
        Long ccdId = rep.getCcdId();
        Long usaId = rep.getCcdUsaId();
        URI uri = uriInfo_.getAbsolutePathBuilder().path(String.valueOf(usaId)).path(
                "card").path(String.valueOf(ccdId)).build();
        rep.setSelf(uri.toString());
        rep.addActionLink(new Link(null, "get", uri.toString(), HTTPMethod.GET));
        rep.addActionLink(new Link(null, "delete", uri.toString(),
                HTTPMethod.DELETE));
    }


    /**
     * Get the access context that should be used for checking permissions. The
     * default implementation returns null to indicate the default context.
     * 
     * @return the name of the access context.
     */
    protected String getAccessContext() {
        return null;
    }


    /**
     * Register a new user by running the following business actions:
     * <ol>
     * <li>BelkCreateUser
     * <li>BelkSendEmail
     * </ol>
     * 
     * @param register
     *            User to register. Must specify email and password.
     * @param reset
     *            if set, reset the user's password
     * @return location of new user account
     * @throws BMException
     */
    @POST
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    public Response registerUser(UserAccountRepresentation register,
            @QueryParam("reset") String reset) throws BMException {
        if( reset != null ) {
            return resetPassword(register, reset);
        }
        ResourceRepresentation.checkRequired(register, "USER_ACCOUNT", "email",
                "password");
        preProcessRequest();

        // check if user already exists
        BindArray bind = new BindArray();
        bind.addString(register.getEmail().toLowerCase());
        bind.addLong(UserAccountUtil.getDefaultConsumerBSEID());
        int count = DBUtil.selectInt(SQL_USER_EXISTS, bind, "store");
        if( count > 0 ) {
            ExceptionEntity ee = new ExceptionEntity("USERID_MUST_BE_UNIQUE",
                    "User ID \"" + register.getEmail()
                            + "\" is already registered.",
                    BMMessages.getFormatMessage("USERID_MUST_BE_UNIQUE"));
            return Response.status(Status.CONFLICT).type(
                    MediaType.TEXT_PLAIN_TYPE).entity(ee).build();
        }

        // invoke business actions, first create sample session data
        WebServiceContext context = WebServiceContext.getContext();
        DNAList dna = new DNAList();

        // create user account object
        BusinessObject bo = BMContext.createBusinessObject("USER_ACCOUNT");
        bo.setString("type_cd", "B2C");
        bo.setString("userid", register.getEmail());
        bo.setString("email", register.getEmail());
        bo.setString("password", register.getPassword());
        bo.setBoolean("optOut", register.getOptOut());
        dna.setBusinessObjectRef(WebConnect.SESSION_TEMP_USER, bo);

        Boolean isOptOut = register.getOptOut();
        if( isOptOut != null ) {
            BusinessObject generic = BMContext.createGenericBusinessObject();
            generic.setBoolean("chk_send_email_optins",
                    !isOptOut.booleanValue());
            dna.setBusinessObjectRef(generic);
        }

        // Invoke the create user bizact
        dna = context.executeHTMLBusinessAction("BelkCreateUser", dna);

        if( !context.isFormValid() ) {
            ExceptionEntity ee = new ExceptionEntity(
                    "USER_REGISTRATION_FAILED", "User ID \""
                            + register.getEmail()
                            + "\" could not be registered.",
                    BMMessages.getFormatMessage("USER_REGISTRATION_FAILED"));
            return Response.status(Status.CONFLICT).type(
                    MediaType.TEXT_PLAIN_TYPE).entity(ee).build();
        }

        // get the updated user
        bo = dna.getBusinessObjectSafe(WebConnect.SESSION_CURRENT_USER);
        UserAccountRepresentation rep = new UserAccountRepresentation(bo);

        // send the user an e-mail
        dna = context.executeHTMLBusinessAction("BelkSendEmail", dna);
        URI location = uriInfo_.getAbsolutePathBuilder().path(
                String.valueOf(bo.getLongID())).path("profile").build();

        return Response.created(location).entity(rep).build();
    }


    /**
     * Rest a user's password
     * 
     * @param user
     *            the user account to reset
     * @param reset
     *            a flag
     * @return OK
     * @throws BMException
     */
    private Response resetPassword(UserAccountRepresentation user, String reset) throws BMException {
        ResourceRepresentation.checkRequired(user, "USER_ACCOUNT", "email");
        preProcessRequest();
        WebServiceContext context = WebServiceContext.getContext();
        String email = user.getEmail();

        // process is:
        // BelkValidateEmailForgotPWD
        // BelkResetUserPassword
        // BelkSendEmail

        BusinessObject generic = BMContext.createGenericBusinessObject();
        generic.setString("email", email);
        generic.setString("confirmEmail", email);
        generic.setBoolean("isUserName", true);
        DNAList dna = new DNAList();
        dna.setBusinessObjectRef(generic);

        dna = context.executeHTMLBusinessAction("BelkValidateEmailForgotPWD",
                dna);
        if( !context.isFormValid() ) {
            // Even though it is invalid, we say OK as we must not reveal
            // anything existence of the user account.
            return Response.ok().build();
        }

        dna = context.executeHTMLBusinessAction("BelkResetUserPassword", dna);
        context.executeHTMLBusinessAction("BelkSendEmail", dna);
        return Response.ok().build();
    }


    /**
     * Get a user account details
     * 
     * @param usaId
     *            the USA ID
     * @return user account
     * @throws BMException
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{id}/profile")
    public Response getUser(@PathParam("id") Long usaId) throws BMException {
        preProcessRequest();
        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canView("UserAccount", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_VIEW");
        }
        BusinessObject user = HTMLUserAccountUtil.getUserAccountDetail(usaId);
        UserAccountRepresentation rep = new UserAccountRepresentation(user);

        // add HATEOAS
        URI uri = uriInfo_.getAbsolutePathBuilder().path(String.valueOf(usaId)).path(
                "addresses").build();
        rep.addActionLink(new Link(null, "getAddresses", uri.toString(),
                HTTPMethod.GET));
        rep.addActionLink(new Link(null, "addAddress", uri.toString(),
                HTTPMethod.POST));

        uri = uriInfo_.getAbsolutePathBuilder().path(String.valueOf(usaId)).path(
                "payments").build();
        rep.addActionLink(new Link(null, "getPayments", uri.toString(),
                HTTPMethod.GET));
        rep.addActionLink(new Link(null, "addPayment", uri.toString(),
                HTTPMethod.POST));

        // provide HATEOAS for authentication
        if( RESTApplication.isSecurityEnabled()
                && getClass().isAnnotationPresent(Authenticate.class) ) {
            AuthenticationProvider provider = RESTApplication.getAuthenticationProvider(
                    getResourceName(), request_.getMethod());
            if( provider != null ) {
                List<Link> actions = provider.getActions();
                for(Link l:actions) {
                    rep.addActionLink(l);
                }
                rep.addActionLink(new Link(null, "authenticator",
                        "data:text/plain,"
                                + BMUtil.encodeURL(provider.getName())));
            }
        }

        return Response.ok(rep).build();
    }


    /**
     * Get addresses associated with a user account. A user can always view
     * their own details. To view another user's addresses they must have the
     * view privilege of "UserAccount".
     * 
     * @param usaId
     *            the USA ID
     * @return user account
     * @throws BMException
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{id}/addresses")
    public Response getAddresses(@PathParam("id") Long usaId) throws BMException {
        preProcessRequest();
        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canView("UserAccount", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_VIEW");
        }
        UserAccount user = HTMLUserAccountUtil.getUserAccountDetail(usaId);
        AddressArray addresses = HTMLUserAccountUtil.getAddresses(user);
        AddressRepresentation.Items list = new AddressRepresentation.Items(
                addresses);
        for(AddressRepresentation rep:list) {
            addHATEOAS(rep);
        }
        return Response.ok(list).build();
    }


    /**
     * Get an address associated with a user account. A user can always view
     * their own details. To view another user's addresses they must have the
     * view privilege of "UserAccount".
     * 
     * @param usaId
     *            the USA ID
     * @param uadId
     *            the address's ID
     * @return address
     * @throws BMException
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{usaId}/address/{uadId}")
    public Response getAddress(@PathParam("usaId") Long usaId,
            @PathParam("uadId") Long uadId) throws BMException {
        preProcessRequest();
        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canView("UserAccount", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_VIEW");
        }

        Address address = HTMLUserAccountUtil.getAddressDetail(uadId);
        if( address == null || address.isEmpty()
                || !usaId.equals(address.getLong("uad_usa_id")) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_VIEW");
        }

        AddressRepresentation rep = new AddressRepresentation(address);
        addHATEOAS(rep);
        return Response.ok(rep).build();
    }


    /**
     * Get a credit card associated with a user account. A user can always view
     * their own details. To view another user's addresses they must have the
     * view privilege of "UserAccount".
     * 
     * @param usaId
     *            the USA ID
     * @param ccdId
     *            the card's ID
     * @return address
     * @throws BMException
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{usaId}/card/{ccdId}")
    public Response getCard(@PathParam("usaId") Long usaId,
            @PathParam("ccdId") Long ccdId) throws BMException {
        preProcessRequest();
        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canView("UserAccount", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_VIEW");
        }

        CreditCard card = HTMLUserAccountUtil.getCreditCard(ccdId);
        if( card == null || card.isEmpty()
                || !usaId.equals(card.getLong("ccd_usa_id")) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_VIEW");
        }

        CreditCardRepresentation rep = new CreditCardRepresentation(card);
        addHATEOAS(rep);
        return Response.ok(rep).build();
    }


    /**
     * Delete a credit card associated with a user account. A user can always
     * edit their own details. To edit another user's addresses they must have
     * the edit privilege of "UserAccount".
     * 
     * @param usaId
     *            the USA ID
     * @param ccdId
     *            the card's ID
     * @return address
     * @throws BMException
     */
    @DELETE
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{usaId}/card/{ccdId}")
    public Response deleteCard(@PathParam("usaId") Long usaId,
            @PathParam("ccdId") Long ccdId) throws BMException {
        preProcessRequest();
        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canView("UserAccount", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_EDIT");
        }

        BMDatabaseManager.pushCurrentDatabase("store");
        try {
            CreditCard card = HTMLUserAccountUtil.getCreditCard(ccdId);
            if( card == null || card.isEmpty()
                    || !usaId.equals(card.getLong("ccd_usa_id")) ) {
                throw webAppException(Status.FORBIDDEN,
                        "RESTAPI_FORBIDDEN_EDIT");
            }

            DBUtil.deleteBusinessObject(card);
        } finally {
            BMDatabaseManager.popCurrentDatabase("store");
        }

        return Response.noContent().build();
    }


    /**
     * Get addresses associated with a user account. A user can always view
     * their own details. To view another user's addresses they must have the
     * view privilege of "UserAccount".
     * 
     * @param usaId
     *            the USA ID
     * @return user account
     * @throws BMException
     */
    @DELETE
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{usaId}/address/{uadId}")
    public Response deleteAddress(@PathParam("usaId") Long usaId,
            @PathParam("uadId") Long uadId) throws BMException {
        preProcessRequest();
        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canEdit("UserAccount", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_EDIT");
        }

        BMDatabaseManager.pushCurrentDatabase("store");
        try {
            Address address = HTMLUserAccountUtil.getAddressDetail(uadId);
            if( address == null || address.isEmpty()
                    || !usaId.equals(address.getLong("uad_usa_id")) ) {
                throw webAppException(Status.FORBIDDEN,
                        "RESTAPI_FORBIDDEN_EDIT");
            }

            DBUtil.deleteBusinessObject(address);
        } finally {
            BMDatabaseManager.popCurrentDatabase("store");
        }

        return Response.noContent().build();
    }


    /**
     * Add a new addresses for a user account. A user can always edit their own
     * details. To edit another user's addresses they must have the edit
     * privilege of "UserAccount".
     * 
     * @param usaId
     *            the USA ID
     * @param newAddress
     *            the new address
     * @return user account
     * @throws BMException
     */
    @POST
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{id}/addresses")
    public Response addAddress(@PathParam("id") Long usaId,
            AddressRepresentation newAddress) throws BMException {
        preProcessRequest();
        ResourceRepresentation.checkRequired(newAddress, "ADDRESS", "address1");

        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canEdit("UserAccount", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_EDIT");
        }

        // user account must already exist
        UserAccount user = HTMLUserAccountUtil.getUserAccountDetail(usaId);
        if( user == null ) {
            DNAList dna = DNAList.newDNAList("name", "usa_id");
            throw webAppException(Status.BAD_REQUEST,
                    "RESTAPI_PROPERTY_INVALID", dna);
        }

        // Validate the address is OK.
        // The UAD_ID field should be blank
        if( newAddress.getUadId() != null ) {
            throw webAppException(Status.BAD_REQUEST, "RESTAPI_ID_NOT_ALLOWED");
        }

        // cannot create an address for another user
        if( newAddress.getUadUsaId() == null ) {
            newAddress.setUadUsaId(usaId);
        } else if( !newAddress.getUadUsaId().equals(usaId) ) {
            DNAList dna = DNAList.newDNAList("name", "uad_usa_id");
            throw webAppException(Status.BAD_REQUEST,
                    "RESTAPI_PROPERTY_INVALID", dna);
        }

        // set the address type
        String type = null;
        if( newAddress.getIndDefaultBillTo() != null
                && newAddress.getIndDefaultBillTo().booleanValue() ) {
            type = Constants.ADDRESS_BILL;
        }
        if( newAddress.getIndDefaultShipTo() != null
                && newAddress.getIndDefaultShipTo().booleanValue() ) {
            type = (type == null) ? Constants.ADDRESS_SHIP
                    : Constants.ADDRESS_BILL_AND_SHIP;
        }
        newAddress.setTypeCd(type);

        // new addresses are always enabled
        newAddress.setStatusCd(Constants.ADDRESS_ENABLED);

        // ready to insert into database
        long uadId;
        BMDatabaseManager.pushCurrentDatabase("store");
        try {
            BusinessObject bizObj = newAddress.getBusinessObject();
            DBUtil.insertBusinessObject(bizObj, true);
            uadId = bizObj.getLongSafe("uad_id");

            // create an ObjectAddress entity to associate
            BusinessObject objectAddress = BMContext.createBusinessObject("OBJECT_ADDRESS");
            objectAddress.setLong("oad_uad_id", uadId);
            objectAddress.setLong("obj_id", usaId);
            objectAddress.setString("objectType", "USER_ACCOUNT");
            objectAddress.setString("type_cd", type);
            DBUtil.insertBusinessObject(objectAddress);

            // do we need to update the user account?
            if( type != null ) {
                BindArray bind = new BindArray();
                bind.addLong(user.getLong("usa_bill_to_uad_id", uadId));
                bind.addLong(user.getLong("usa_ship_to_uad_id", uadId));
                bind.addLong(usaId);

                // set the appropriate fields
                if( type.equals(Constants.ADDRESS_BILL) ) {
                    bind.setLong(0, uadId);
                } else if( type.equals(Constants.ADDRESS_SHIP) ) {
                    bind.setLong(1, uadId);
                } else if( type.equals(Constants.ADDRESS_BILL_AND_SHIP) ) {
                    bind.setLong(0, uadId);
                    bind.setLong(1, uadId);
                }

                // do the update
                BMPreparedStatement stmt = null;
                try {
                    stmt = BMDatabaseManager.getPreparedStatement(SQL_UPDATE_USER_ADDR);
                    stmt.setBindValues(bind);
                    stmt.executeUpdate();
                } finally {
                    if( stmt != null ) {
                        stmt.close();
                    }
                }
            }
        } finally {
            BMDatabaseManager.popCurrentDatabase("store");
        }

        // get location of new address
        URI location = uriInfo_.getAbsolutePathBuilder().path(
                String.valueOf(usaId)).path("addresses").path(
                String.valueOf(uadId)).build();

        // return updated address
        return Response.created(location).entity(newAddress).build();
    }


    /**
     * Add a new credit card for a user account. A user can always edit their
     * own details. To edit another user's addresses they must have the edit
     * privilege of "CreditCard".
     * 
     * @param usaId
     *            the USA ID
     * @param newAddress
     *            the new address
     * @return user account
     * @throws BMException
     */
    @POST
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{id}/card")
    public Response addCreditCard(@PathParam("id") Long usaId,
            CreditCardRepresentation newCard) throws BMException {
        preProcessRequest();
        ResourceRepresentation.checkRequired(newCard, "CREDIT_CARD", "cardNum",
                "cardBrand_cd", "cardholderName");

        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canEdit("CreditCard", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_EDIT");
        }

        // user account must already exist
        UserAccount user = HTMLUserAccountUtil.getUserAccountDetail(usaId);
        if( user == null ) {
            DNAList dna = DNAList.newDNAList("name", "usa_id");
            throw webAppException(Status.BAD_REQUEST,
                    "RESTAPI_PROPERTY_INVALID", dna);
        }

        // Validate the card is OK.
        // The CCD_ID field should be blank
        if( newCard.getCcdId() != null ) {
            throw webAppException(Status.BAD_REQUEST, "RESTAPI_ID_NOT_ALLOWED");
        }

        // cannot create a card for another user
        if( newCard.getCcdUsaId() == null ) {
            newCard.setCcdUsaId(usaId);
        } else if( !newCard.getCcdUsaId().equals(usaId) ) {
            DNAList dna = DNAList.newDNAList("name", "ccd_usa_id");
            throw webAppException(Status.BAD_REQUEST,
                    "RESTAPI_PROPERTY_INVALID", dna);
        }

        // ready to insert into database
        BMDatabaseManager.pushCurrentDatabase("store");
        try {
            BusinessObject bizObj = newCard.getBusinessObject();
            DBUtil.insertBusinessObject(bizObj, true);
        } finally {
            BMDatabaseManager.popCurrentDatabase("store");
        }

        // get location of new card
        URI location = uriInfo_.getAbsolutePathBuilder().path(
                String.valueOf(usaId)).path("card").path(
                String.valueOf(newCard.getCcdId())).build();

        // return updated address
        return Response.created(location).entity(newCard).build();
    }


    /**
     * Updates a credit card for a user account. A user can always edit their
     * own details. To edit another user's addresses they must have the edit
     * privilege of "CreditCard".
     * 
     * @param usaId
     *            the USA ID
     * @param newAddress
     *            the new address
     * @return user account
     * @throws BMException
     */
    @PUT
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{id}/card")
    public Response updateCreditCard(@PathParam("id") Long usaId,
            CreditCardRepresentation newCard) throws BMException {
        preProcessRequest();
        ResourceRepresentation.checkRequired(newCard, "CREDIT_CARD", "ccd_id",
                "ccd_usa_id");

        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canEdit("CreditCard", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_EDIT");
        }

        // user account must already exist
        UserAccount user = HTMLUserAccountUtil.getUserAccountDetail(usaId);
        if( user == null ) {
            DNAList dna = DNAList.newDNAList("name", "usa_id");
            throw webAppException(Status.BAD_REQUEST,
                    "RESTAPI_PROPERTY_INVALID", dna);
        }

        // ready to insert into database
        BMDatabaseManager.pushCurrentDatabase("store");
        try {
            // get the existing credit card
            CreditCard oldCard = HTMLUserAccountUtil.getCreditCard(newCard.getCcdId());

            // card must exist and be owned by this user
            if( oldCard == null
                    || oldCard.isEmpty()
                    || oldCard.getString("status_cd", "").equals(
                            Constants.CREDIT_CARD_DISABLED)
                    || !user.getLongID().equals(oldCard.getLong("ccd_usa_id")) ) {
                throw webAppException(Status.FORBIDDEN,
                        "RESTAPI_FORBIDDEN_EDIT");
            }
            
            // anything unset inherits from the old card
            BusinessObject bizObj = newCard.getBusinessObject();
            bizObj.inherit(oldCard);            
            DBUtil.insertBusinessObject(bizObj, true);
        } finally {
            BMDatabaseManager.popCurrentDatabase("store");
        }

        // get location of new card
        URI location = uriInfo_.getAbsolutePathBuilder().path(
                String.valueOf(usaId)).path("card").path(
                String.valueOf(newCard.getCcdId())).build();

        addHATEOAS(newCard);
        // return updated card
        return Response.created(location).entity(newCard).build();
    }


    /**
     * Get credit cards associated with a user account. A user can always view
     * their own details. To view another user's addresses they must have the
     * view privilege of "UserAccount".
     * 
     * @param usaId
     *            the USA ID
     * @return user account
     * @throws BMException
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("{id}/cards")
    public Response getCards(@PathParam("id") Long usaId) throws BMException {
        preProcessRequest();
        Long loggedInAs = getLoginIdSafe();
        if( !(loggedInAs.equals(usaId))
                || BMAccessControl.canView("UserAccount", getAccessContext()) ) {
            throw webAppException(Status.FORBIDDEN, "RESTAPI_FORBIDDEN_VIEW");
        }
        UserAccount user = HTMLUserAccountUtil.getUserAccountDetail(usaId);
        CreditCardArray addresses = HTMLUserAccountUtil.getUsersCreditCards(user);
        CreditCardRepresentation.Items list = new CreditCardRepresentation.Items(
                addresses);
        for(CreditCardRepresentation rep:list) {
            addHATEOAS(rep);
        }
        return Response.ok(list).build();
    }
}
