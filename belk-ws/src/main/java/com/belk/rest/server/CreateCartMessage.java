package com.belk.rest.server;

import com.bluemartini.rest.server.CartItemRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreateCart")
public class CreateCartMessage {
    @XmlElement
    public CartItemRepresentation[] cartItems;    
}
