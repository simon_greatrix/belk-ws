package com.belk.rest.server;

import com.bluemartini.dna.BMContext;
import com.bluemartini.dna.BMException;
import com.bluemartini.dna.BMLog;
import com.bluemartini.dna.DNAList;
import com.bluemartini.html.WebServiceContext;
import com.bluemartini.rest.*;
import com.bluemartini.webconnect.WebConnect;

import com.belk.rest.gifts.GiftRegistryResource;
import com.belk.website.util.BelkUserAccountUtil;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.net.URI;

@Authenticate
@Path("bridal")
@NamedResource("Bridal")
public class BridalResource extends BaseResource {
    @POST
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    public Response create(BridalCreateMessage message) throws BMException {
        ResourceRepresentation.checkRequired(message.userAccount,
                WebConnect.SESSION_CURRENT_USER, "usa_id");
        ResourceRepresentation.checkRequired(message.registrantAddress,
                "REGISTRANT_ADDRESS", "firstName", "lastName", "address1",
                "city", "state_cd", "postal");
        ResourceRepresentation.checkRequired(message.registrant, "REGISTRANT",
                "registrant_fname", "registrant_lname", "ph_area_code",
                "ph_phone1", "ph_phone2", "registrant_email", "coreg_fname",
                "coreg_lname", "registrant_store_no");
        ResourceRepresentation.checkRequired(message.event, "EVENT",
                "event_dt_str", "event_city", "event_state");
        if( message.alternateAddress != null ) {
            ResourceRepresentation.checkRequired(message.alternateAddress,
                    "ALTERNATE_ADDRESS", "firstName", "lastName", "address1",
                    "city", "state_cd", "postal");
            ResourceRepresentation.checkRequired(message.registrant,
                    "REGISTRANT", "registrant_alt_addr_date_str");

        }
        
        preProcessRequest();

        DNAList dna = new DNAList();
        dna.setBusinessObject(WebConnect.SESSION_CURRENT_USER,message.userAccount.getBusinessObject());
        dna.setBusinessObject("REGISTRANT_ADDRESS",message.registrantAddress.getBusinessObject());
        dna.setBusinessObject("REGISTRANT",message.registrant.getBusinessObject());
        dna.setBusinessObject("EVENT",message.event.getBusinessObject());
        if( message.alternateAddress != null ) {
            dna.setBusinessObject("ALTERNATE_ADDRESS",message.alternateAddress.getBusinessObject());
        } else {
            dna.setBusinessObject("ALTERNATE_ADDRESS",BMContext.createBusinessObject("ADDRESS"));
        }
        
        WebServiceContext context = WebServiceContext.getContext();
        dna = context.executeHTMLBusinessAction("BelkCreateBridalRegistry", dna);
        dna = context.executeHTMLBusinessAction("BelkSendEmail",dna);
        
        Long usaId = message.userAccount.getUsaId();
        Long registryId = BelkUserAccountUtil.getBridalRegistry(usaId).getLongID();

        URI uri = getUriInfo().getBaseUriBuilder().path(GiftRegistryResource.class).path(String.valueOf(registryId)).build();
        
        if (BMLog.logEnabled(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE)) {
            BMLog.log(BMLog.COMPONENT_REST, BMLog.LEVEL_TRACE, "BridalResource", "createGiftRegistry", 
                    "exit giftRegistryId=" + registryId);
        }
        
        return Response.created(uri).build();        
    }
}
