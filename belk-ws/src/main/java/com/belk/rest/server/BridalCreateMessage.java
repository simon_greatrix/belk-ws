package com.belk.rest.server;

import com.bluemartini.rest.server.AddressRepresentation;
import com.bluemartini.rest.server.UserAccountRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BridalCreate")
public class BridalCreateMessage {
    @XmlElement
    public UserAccountRepresentation userAccount;

    @XmlElement
    public AddressRepresentation registrantAddress;

    @XmlElement
    public AddressRepresentation alternateAddress;

    @XmlElement
    public RegistrantRepresentation registrant;

    @XmlElement
    public EventRepresentation event;
}
