package com.belk.rest.server;

import com.bluemartini.dna.BMException;
import com.bluemartini.htmlapp.HTMLProductUtil;
import com.bluemartini.htmlapp.Sku;
import com.bluemartini.rest.BaseResource;
import com.bluemartini.rest.Link;
import com.bluemartini.rest.RESTConstants;

import com.belk.website.BelkSku;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Accessor services for Belk SKUs
 * 
 * @author Simon Greatrix
 *
 */
@Path("/sku")
public class BelkSkuResource extends BaseResource {

    /**
     * Get a BelkSku's details by the SKU's UPC
     * 
     * @param upc
     *            SKU's UPC
     * @param giftRegistryID
     *            gift registry to which the SKU may be added
     * @return service response
     * @throws BMException
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("upc/{upc}")
    public Response getSkuByUPC(@PathParam("upc") String upc,
            @QueryParam("giftRegistryID") Long giftRegistryID) throws BMException {
        Sku sku = HTMLProductUtil.getSkuByUPC(upc);
        return makeSku(sku, giftRegistryID);
    }


    /**
     * Get a BelkSku's details by the SKU's Blue Martini ID
     * 
     * @param id
     *            SKU's ID
     * @param giftRegistryID
     *            gift registry to which the SKU may be added
     * @return service response
     * @throws BMException
     */
    @GET
    @Produces(RESTConstants.MEDIATYPE_JSON_OR_XML)
    @Path("id/{id}")
    public Response getSkuByID(@PathParam("id") Long id,
            @QueryParam("giftRegistryID") Long giftRegistryID) throws BMException {
        Sku sku = HTMLProductUtil.getSkuDetail(id);
        return makeSku(sku, giftRegistryID);
    }


    /**
     * Create a SKU representation in a response
     * 
     * @param sku
     *            the SKU to represent
     * @param giftRegistryID
     *            the gift registry ID it may be added to
     * @return the response
     */
    private Response makeSku(Sku sku, Long giftRegistryID) {
        BelkSku belkSku = new BelkSku(sku);
        BelkSkuRepresentation skuRep = new BelkSkuRepresentation(uriInfo_,
                belkSku);

        if( giftRegistryID != null ) {
            skuRep.addActionLink(new Link(null, "addToGiftRegistry", ""));
        }
        return Response.ok(skuRep).build();
    }


    @Override
    public String getResourceName() {
        return "BelkSku";
    }
}
