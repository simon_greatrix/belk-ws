package com.belk.rest.server;

import com.bluemartini.rest.server.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Message to create an order.
 * 
 * @author Simon Greatrix
 *
 */
@XmlRootElement(name = "OrderCreate")
public class OrderCreateMessage {

    /**
     * The user account which will own the order. Must specify either a USA_ID
     * or an e-mail address.
     */
    @XmlElement
    public UserAccountRepresentation userAccount;

    /** Address to ship the order to */
    @XmlElement
    public AddressRepresentation shipTo;

    /** Selected shipment method. Must specify a SHM_ID */
    @XmlElement
    public ShipmentMethodRepresentation shipmentMethod;

    /** Address for billing */
    @XmlElement
    public AddressRepresentation billTo;

    /**
     * The items in the shopping cart
     */
    @XmlElement
    public CartItemRepresentation.Items cartItems;

    @XmlElement
    public CreditCardRepresentation.Items creditCards;
    
    /**
     * If true, will stop at the validation step
     */
    @XmlElement
    public boolean testOnly = false;
}
