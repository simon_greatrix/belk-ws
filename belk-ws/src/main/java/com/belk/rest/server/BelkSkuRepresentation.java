package com.belk.rest.server;

import com.bluemartini.dna.BMException;
import com.bluemartini.dna.Currency;
import com.bluemartini.dna.DNAString;
import com.bluemartini.html.HTMLContext;
import com.bluemartini.rest.server.SkuRepresentation;

import com.belk.website.BelkConstants;
import com.belk.website.BelkSku;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.lang.reflect.Method;
import java.math.BigDecimal;

@XmlRootElement(name="sku")
public class BelkSkuRepresentation extends SkuRepresentation {

    private static final Method METHOD_BY_ID;
    
    static {
        Class<?> cl = BelkSkuResource.class;
        try {
            METHOD_BY_ID = cl.getDeclaredMethod("getSkuByID", new Class<?>[] { Long.class, Long.class});
        } catch (NoSuchMethodException e) {
            throw new Error("BelkSkuResource.getSkuByID not found");
        }
    }
    
    private String brand_;

    private String colorDesc_;

    private String department_;

    private String imageURL_;

    private boolean isForBridal_;

    private BigDecimal listPrice_;

    private String name_;

    private String shadeDesc_;

    private String shortDescription_;

    private String sizeDesc_;

    private String upc_;


    public BelkSkuRepresentation() {
        // do nothing
    }


    public BelkSkuRepresentation(UriInfo uriInfo, BelkSku sku) {
        if( sku == null ) return;

        brand_ = sku.getBrand();
        colorDesc_ = sku.getColorDescOnline();
        department_ = sku.getDepartment();
        imageURL_ = sku.getImageURL(BelkConstants.IF_PROD_WISH);
        isForBridal_ = sku.isBridalEligible();
        shadeDesc_ = sku.getShadeDesc();
        shortDescription_ = sku.getShortDescription();
        sizeDesc_ = sku.getSizeDesc();
        setResourceID(String.valueOf(sku.getLongID()));
        upc_ = sku.getUPC();

        // Logic copied for BelkProduct, but as it does not need BelkProduct
        // for anything we are not getting a product instance just to call it.
        String brandName = sku.getDisplayableString(
                BelkConstants.BELK_PRODUCT_BRAND, DNAString.EMPTY_STRING);
        if( DNAString.EMPTY_STRING.equals(brandName) ) {
            brandName = sku.getDisplayableString(BelkConstants.ATR_VENDOR_NAME,
                    DNAString.EMPTY_STRING);
        }

        String patternName = sku.getDisplayableString(
                BelkConstants.BELK_PRODUCT_PATTERN_NAME, DNAString.EMPTY_STRING);

        name_ = brandName + HTMLContext.HTML_SPACE + patternName
                + HTMLContext.HTML_SPACE + sku.getShortDescription();

        setSelf(uriInfo.getBaseUriBuilder().path(METHOD_BY_ID).path(getResourceID()).build().toString());
        
        try {
            Currency price = sku.getListPrice();
            listPrice_ = (price != null) ? price.toBigDecimal() : null;
        } catch (BMException bme) {
            listPrice_ = null;
        }
    }


    @XmlElement
    public String getBrand() {
        return brand_;
    }


    @XmlElement
    public String getColorDesc() {
        return colorDesc_;
    }


    @XmlElement
    public String getDepartment() {
        return department_;
    }


    @XmlElement
    public String getImageURL() {
        return imageURL_;
    }


    @XmlElement
    public BigDecimal getListPrice() {
        return listPrice_;
    }


    @XmlElement
    public String getName() {
        return name_;
    }


    @XmlElement
    public String getShadeDesc() {
        return shadeDesc_;
    }


    @XmlElement
    public String getShortDescription() {
        return shortDescription_;
    }


    @XmlElement
    public String getSizeDesc() {
        return sizeDesc_;
    }


    @XmlElement
    public String getUPC() {
        return upc_;
    }


    public boolean isForBridal() {
        return isForBridal_;
    }


    public void setBrand(String brand) {
        // property is read only for data-binding
    }


    public void setColorDesc(String colorDesc) {
        // property is read only for data-binding
    }


    public void setDepartment(String department) {
        // property is read only for data-binding
    }


    public void setForBridal(boolean isForBridal) {
        // property is read only for data-binding
    }


    public void setImageURL(String imageURL) {
        // property is read only for data-binding
    }


    public void setListPrice(BigDecimal listPrice) {
        // property is read only for data-binding
    }


    public void setName(String name) {
        // property is read only for data-binding
    }


    public void setShadeDesc(String shadeDesc) {
        // property is read only for data-binding
    }


    public void setShortDescription(String shortDescription) {
        // property is read only for data-binding
    }


    public void setSizeDesc(String sizeDesc) {
        // property is read only for data-binding
    }


    public void setUPC(String upc) {
        // property is read only for data-binding
    }
}
