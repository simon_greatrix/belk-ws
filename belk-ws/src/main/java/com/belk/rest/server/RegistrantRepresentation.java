package com.belk.rest.server;

import com.bluemartini.dna.BMContext;
import com.bluemartini.dna.BusinessObject;
import com.bluemartini.dna.Currency;
import com.bluemartini.rest.ResourceRepresentation;
import com.bluemartini.rest.server.AttributeList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;

/**
 * Server representation of a REGISTRANT object.
 */
@XmlRootElement(name="Registrant")
public class RegistrantRepresentation extends ResourceRepresentation {

    public RegistrantRepresentation(BusinessObject bizObj) {
        super(bizObj);
        Number id = bizObj.getNumberID();
        if( id!=null ) setResourceID(id.toString());
        attributes_ = new AttributeList(bizObj);
    }

    public RegistrantRepresentation() {
        this(BMContext.createBusinessObject("REGISTRANT"));
    }


    private AttributeList attributes_;
    
    @XmlElementWrapper(name="attributes")
    @XmlElement(name="attribute")
    public AttributeList getAttributes() {
        return attributes_;
    }
    
    public void setAttributeList(AttributeList attributes) {
        attributes_ = attributes;
        attributes.apply(boResource_,Collections.<String>emptySet());
    }



    @XmlElement
    public String getCoregEmail() {
        return boResource_.getString("coreg_email");
    }
    
    public void setCoregEmail(String coregEmail) {
        validateAndSet("coreg_email",coregEmail);
    }
    


    @XmlElement
    public String getCoregFname() {
        return boResource_.getString("coreg_fname");
    }
    
    public void setCoregFname(String coregFname) {
        validateAndSet("coreg_fname",coregFname);
    }
    


    @XmlElement
    public String getCoregLname() {
        return boResource_.getString("coreg_lname");
    }
    
    public void setCoregLname(String coregLname) {
        validateAndSet("coreg_lname",coregLname);
    }
    


    @XmlElement
    public String getCoregPhonenum() {
        return boResource_.getString("coreg_phonenum");
    }
    
    public void setCoregPhonenum(String coregPhonenum) {
        validateAndSet("coreg_phonenum",coregPhonenum);
    }
    


    @XmlElement
    public String getCoregRole() {
        return boResource_.getString("coreg_role");
    }
    
    public void setCoregRole(String coregRole) {
        validateAndSet("coreg_role",coregRole);
    }
    


    @XmlElement
    public Long getCoregUadId() {
        return boResource_.getLong("coreg_uad_id");
    }
    
    public void setCoregUadId(Long coregUadId) {
        validateAndSet("coreg_uad_id",coregUadId);
    }
    


    @XmlElement
    public Boolean getGiftCardAvailable() {
        return boResource_.getBoolean("gift_card_available");
    }
    
    public void setGiftCardAvailable(Boolean giftCardAvailable) {
        validateAndSet("gift_card_available",giftCardAvailable);
    }
    


    @XmlElement
    public String getOtherEmail() {
        return boResource_.getString("other_email");
    }
    
    public void setOtherEmail(String otherEmail) {
        validateAndSet("other_email",otherEmail);
    }
    


    @XmlElement
    public String getOtherFname() {
        return boResource_.getString("other_fname");
    }
    
    public void setOtherFname(String otherFname) {
        validateAndSet("other_fname",otherFname);
    }
    


    @XmlElement
    public String getOtherLname() {
        return boResource_.getString("other_lname");
    }
    
    public void setOtherLname(String otherLname) {
        validateAndSet("other_lname",otherLname);
    }
    


    @XmlElement
    public Long getOtherUadId() {
        return boResource_.getLong("other_uad_id");
    }
    
    public void setOtherUadId(Long otherUadId) {
        validateAndSet("other_uad_id",otherUadId);
    }
    


    @XmlElement
    public String getRegPhonenum() {
        return boResource_.getString("reg_phonenum");
    }
    
    public void setRegPhonenum(String regPhonenum) {
        validateAndSet("reg_phonenum",regPhonenum);
    }
    


    @XmlElement
    public Calendar getRegistrantAltAddrDate() {
        return boResource_.getDateTime("registrant_alt_addr_date");
    }
    
    public void setRegistrantAltAddrDate(Calendar registrantAltAddrDate) {
        validateAndSet("registrant_alt_addr_date",registrantAltAddrDate);
    }
    


    @XmlElement
    public Long getRegistrantAltUadId() {
        return boResource_.getLong("registrant_alt_uad_id");
    }
    
    public void setRegistrantAltUadId(Long registrantAltUadId) {
        validateAndSet("registrant_alt_uad_id",registrantAltUadId);
    }
    


    @XmlElement
    public String getRegistrantCommMethod() {
        return boResource_.getString("registrant_comm_method");
    }
    
    public void setRegistrantCommMethod(String registrantCommMethod) {
        validateAndSet("registrant_comm_method",registrantCommMethod);
    }
    


    @XmlElement
    public String getRegistrantConsulId() {
        return boResource_.getString("registrant_consul_id");
    }
    
    public void setRegistrantConsulId(String registrantConsulId) {
        validateAndSet("registrant_consul_id",registrantConsulId);
    }
    


    @XmlElement
    public String getRegistrantConsulStrNo() {
        return boResource_.getString("registrant_consul_str_no");
    }
    
    public void setRegistrantConsulStrNo(String registrantConsulStrNo) {
        validateAndSet("registrant_consul_str_no",registrantConsulStrNo);
    }
    


    @XmlElement
    public String getRegistrantEmail() {
        return boResource_.getString("registrant_email");
    }
    
    public void setRegistrantEmail(String registrantEmail) {
        validateAndSet("registrant_email",registrantEmail);
    }
    


    @XmlElement
    public String getRegistrantFamStrNo() {
        return boResource_.getString("registrant_fam_str_no");
    }
    
    public void setRegistrantFamStrNo(String registrantFamStrNo) {
        validateAndSet("registrant_fam_str_no",registrantFamStrNo);
    }
    


    @XmlElement
    public String getRegistrantFname() {
        return boResource_.getString("registrant_fname");
    }
    
    public void setRegistrantFname(String registrantFname) {
        validateAndSet("registrant_fname",registrantFname);
    }
    


    @XmlElement
    public String getRegistrantGrhCode() {
        return boResource_.getString("registrant_grh_code");
    }
    
    public void setRegistrantGrhCode(String registrantGrhCode) {
        validateAndSet("registrant_grh_code",registrantGrhCode);
    }
    


    @XmlElement
    public Long getRegistrantGrhId() {
        return boResource_.getLong("registrant_grh_id");
    }
    
    public void setRegistrantGrhId(Long registrantGrhId) {
        validateAndSet("registrant_grh_id",registrantGrhId);
    }
    


    @XmlElement
    public String getRegistrantLname() {
        return boResource_.getString("registrant_lname");
    }
    
    public void setRegistrantLname(String registrantLname) {
        validateAndSet("registrant_lname",registrantLname);
    }
    


    @XmlElement
    public Long getRegistrantPerImg() {
        return boResource_.getLong("registrant_per_img");
    }
    
    public void setRegistrantPerImg(Long registrantPerImg) {
        validateAndSet("registrant_per_img",registrantPerImg);
    }
    


    @XmlElement
    public Long getRegistrantPrimaryUadId() {
        return boResource_.getLong("registrant_primary_uad_id");
    }
    
    public void setRegistrantPrimaryUadId(Long registrantPrimaryUadId) {
        validateAndSet("registrant_primary_uad_id",registrantPrimaryUadId);
    }
    


    @XmlElement
    public String getRegistrantRole() {
        return boResource_.getString("registrant_role");
    }
    
    public void setRegistrantRole(String registrantRole) {
        validateAndSet("registrant_role",registrantRole);
    }
    


    @XmlElement
    public String getRegistrantShipPref() {
        return boResource_.getString("registrant_ship_pref");
    }
    
    public void setRegistrantShipPref(String registrantShipPref) {
        validateAndSet("registrant_ship_pref",registrantShipPref);
    }
    


    @XmlElement
    public String getRegistrantStoreNo() {
        return boResource_.getString("registrant_store_no");
    }
    
    public void setRegistrantStoreNo(String registrantStoreNo) {
        validateAndSet("registrant_store_no",registrantStoreNo);
    }
    


    @XmlElement
    public Long getRegistrantUadId() {
        return boResource_.getLong("registrant_uad_id");
    }
    
    public void setRegistrantUadId(Long registrantUadId) {
        validateAndSet("registrant_uad_id",registrantUadId);
    }
    

}
