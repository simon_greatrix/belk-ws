package com.belk.rest.server;

import com.bluemartini.dna.BMContext;
import com.bluemartini.dna.BusinessObject;
import com.bluemartini.dna.Currency;
import com.bluemartini.rest.ResourceRepresentation;
import com.bluemartini.rest.server.AttributeList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;

/**
 * Server representation of a EVENT object.
 */
@XmlRootElement(name="Event")
public class EventRepresentation extends ResourceRepresentation {

    public EventRepresentation(BusinessObject bizObj) {
        super(bizObj);
        Number id = bizObj.getNumberID();
        if( id!=null ) setResourceID(id.toString());
        attributes_ = new AttributeList(bizObj);
    }

    public EventRepresentation() {
        this(BMContext.createBusinessObject("EVENT"));
    }


    private AttributeList attributes_;
    
    @XmlElementWrapper(name="attributes")
    @XmlElement(name="attribute")
    public AttributeList getAttributes() {
        return attributes_;
    }
    
    public void setAttributeList(AttributeList attributes) {
        attributes_ = attributes;
        attributes.apply(boResource_, Collections.<String>emptySet());
    }



    @XmlElement
    public String getEventCity() {
        return boResource_.getString("event_city");
    }
    
    public void setEventCity(String eventCity) {
        validateAndSet("event_city",eventCity);
    }
    


    @XmlElement
    public Calendar getEventDt() {
        return boResource_.getDateTime("event_dt");
    }
    
    public void setEventDt(Calendar eventDt) {
        validateAndSet("event_dt",eventDt);
    }
    


    @XmlElement
    public Long getEventGrhId() {
        return boResource_.getLong("event_grh_id");
    }
    
    public void setEventGrhId(Long eventGrhId) {
        validateAndSet("event_grh_id",eventGrhId);
    }
    


    @XmlElement
    public String getEventOtherState() {
        return boResource_.getString("event_other_state");
    }
    
    public void setEventOtherState(String eventOtherState) {
        validateAndSet("event_other_state",eventOtherState);
    }
    


    @XmlElement
    public String getEventState() {
        return boResource_.getString("event_state");
    }
    
    public void setEventState(String eventState) {
        validateAndSet("event_state",eventState);
    }
    


    @XmlElement
    public Integer getNumGuests() {
        return boResource_.getInteger("num_guests");
    }
    
    public void setNumGuests(Integer numGuests) {
        validateAndSet("num_guests",numGuests);
    }
    

}
