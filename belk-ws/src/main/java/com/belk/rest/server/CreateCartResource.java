package com.belk.rest.server;

import com.bluemartini.dna.BMException;
import com.bluemartini.dna.BusinessObject;
import com.bluemartini.dna.BusinessObjectArray;
import com.bluemartini.dna.DNAList;
import com.bluemartini.html.HTMLContext;
import com.bluemartini.html.HTMLSession;
import com.bluemartini.html.WebServiceContext;
import com.bluemartini.htmlapp.HTMLUserAccountUtil;
import com.bluemartini.http.SessionUtil;
import com.bluemartini.rest.Authenticate;
import com.bluemartini.rest.BaseResource;
import com.bluemartini.rest.NamedResource;
import com.bluemartini.rest.server.CartItemRepresentation;
import com.bluemartini.webconnect.WebConnect;

import com.belk.website.BelkConstants;

import javax.servlet.http.HttpSession;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import java.net.URI;
import java.net.URISyntaxException;

@Authenticate
@Path("createCart")
@NamedResource("CreateCart")
public class CreateCartResource extends BaseResource {
    private static final String[] ADD_TO_CART_BIZACTS = new String[] {
        "BelkAjaxValidateSelectedSku",
        "BelkAjaxValidateCartContents",
        "BelkIdentifyAddToCartSku",
        "BelkSetCoremetricsCartItem",
        "BelkAutoAddPromoProduct",  
        "BelkAddToShoppingCart",
        "WebConnectAddToShoppingCart",
        "BelkSetRegistryOnCart",
        "BelkSaveCookieShoppingCart",
        "WebConnectUpdateCartQuantity",
        "BelkCalculateEstimatedShipping"        
    };
    
    @POST
    public Response createCart(CreateCartMessage message) throws BMException {
        preProcessRequest();
        WebServiceContext context = WebServiceContext.getContext();
        
        // Belk Business Actions check the previous template, so we must set it
        DNAList dnaRequest = context.getRequest();
        dnaRequest.setString(HTMLContext.BM_PREV_TEMPLATE,"web_service");
        
        // Create form data
        DNAList formData = new DNAList();
        BusinessObjectArray addCartItems = new BusinessObjectArray();
        formData.setBusinessObjectArrayRef(WebConnect.SESSION_ADD_CART_ITEM_ARRAY, addCartItems);
        formData.setBusinessObjectArrayRef(WebConnect.SESSION_CART_ITEM_ARRAY, new BusinessObjectArray());
        formData.setBusinessObject(WebConnect.SESSION_PRICE_LIST,BelkConstants.getSalePriceList());
        BusinessObject currentUser = HTMLUserAccountUtil.getUserAccountDetail(getLoginIdSafe());
        formData.setBusinessObjectRef(WebConnect.SESSION_CURRENT_USER,currentUser);
        
        // add cart items to form data
        for(CartItemRepresentation item : message.cartItems) {
            BusinessObject cartItem = item.getBusinessObject();
            addCartItems.add(cartItem);
        }
        
        // run the business actions
        for(String bizact : ADD_TO_CART_BIZACTS ) {
            formData = context.executeHTMLBusinessAction(bizact, formData);
        }
        
        // create the session
        HTMLSession htmlSession = new HTMLSession();
        htmlSession.move(formData);
        htmlSession.setIgnoreUnboundEvent(true);
        HttpSession httpSession = context.getHttpRequest().getSession();
        SessionUtil.setIsTemporarySession(httpSession);
        SessionUtil.setFormData(httpSession, htmlSession);
        SessionUtil.setLoginId(httpSession, getLoginIdSafe());
        
        String url = "http://www.belk.com/AST/Misc/Belk_Stores/Global_Navigation/Shopping_Bag.jsp";
        String redirectURL = context.getHttpResponse().encodeRedirectURL(url);
        URI uri;
        try {
            uri = new URI(redirectURL);
        } catch (URISyntaxException e) {
            throw new BMException("INVALID_URL",e,null);
        }
        
        BusinessObjectArray cartItems = htmlSession.getBusinessObjectArray(WebConnect.SESSION_CART_ITEM_ARRAY);
        CreateCartMessage output = new CreateCartMessage();
        output.cartItems = new CartItemRepresentation[cartItems.size()];
        for(int i=0;i<cartItems.size();i++) {
            output.cartItems[i] = new CartItemRepresentation(cartItems.get(i));
        }
        return Response.created(uri).entity(output).build();
    }
}
