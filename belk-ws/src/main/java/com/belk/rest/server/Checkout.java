package com.belk.rest.server;

import com.bluemartini.dna.*;
import com.bluemartini.html.WebServiceContext;
import com.bluemartini.htmlapp.HTMLOrderUtil;
import com.bluemartini.htmlapp.HTMLUserAccountUtil;
import com.bluemartini.rest.BaseResource;
import com.bluemartini.rest.NamedResource;
import com.bluemartini.rest.WebServiceRequestRunner;
import com.bluemartini.rest.WebServiceRequestRunner.Simulator;
import com.bluemartini.rest.server.*;
import com.bluemartini.webconnect.WebConnect;

import com.belk.website.BelkConstants;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("checkout")
@NamedResource("Checkout")
public class Checkout extends BaseResource {

    /**
     * Simulator for Registered User start checkout
     * @author Simon Greatrix
     *
     */
    public static class RU_Checkout implements Simulator {
        @Override
        public void invoke(String form, String submit, Type type, Object msg) throws BMException {
            if( type!=Type.USER ) return;
            
            OrderCreateMessage message = (OrderCreateMessage) msg;
            WebServiceContext context = WebServiceContext.getContext();
            DNAList formData = context.getFormData();

            // price list
            formData.setBusinessObject("PRICE_LIST",
                    BelkConstants.getListPriceList());

            // Registered user
            UserAccountRepresentation user = message.userAccount;
            Long userID = user.getUsaId();
            BusinessObject boUser = HTMLUserAccountUtil.getUserAccount(userID);
            formData.setBusinessObjectRef(WebConnect.SESSION_CURRENT_USER, boUser);

            // Create cart item array
            BusinessObjectArray cartArray = new BusinessObjectArray();
            for(CartItemRepresentation item:message.cartItems) {
                cartArray.add(item.getBusinessObject());
            }
            formData.setBusinessObjectArrayRef("SHOPPING_CART", cartArray);

            // Create cart header
            DNAList updated = HTMLOrderUtil.updateCartHeader(formData);
            formData.move(updated);
            cartArray = formData.removeBusinessObjectArray("SHOPPING_CART");
            formData.setBusinessObjectArrayRef("CART_ITEM_ARRAY", cartArray);
        }


        @Override
        public boolean isReusable() {
            return true;
        }
    }

    
    /**
     * Simulator for Registered User shipping
     * @author Simon Greatrix
     *
     */
    public static class RU_Shipping implements Simulator {
        @Override
        public void invoke(String form, String submit, Type type, Object msg) throws BMException {
            if( type!=Type.USER ) return;
            
            OrderCreateMessage message = (OrderCreateMessage) msg;
            ShipmentMethodRepresentation shipping = message.shipmentMethod;
            AddressRepresentation shipTo = message.shipTo;
            
            BusinessObject generic = BusinessObjectDef.createGenericBusinessObject();
            generic.setLong("shippingMethodList", shipping.getShmId());
            generic.setLong("selectAddressList", shipTo.getUadId());
            DNAList patch = new DNAList();
            patch.setBusinessObject(generic);
            WebServiceRequestRunner.patchFormData(patch);
        }


        @Override
        public boolean isReusable() {
            return true;
        }
    }

    /**
     * Simlator for Registered User Billing
     * @author Simon Greatrix
     *
     */
    public static class RU_Billing implements Simulator {
        @Override
        public void invoke(String form, String submit, Type type, Object msg) throws BMException {
            if( type!=Type.USER ) return;
            
            OrderCreateMessage message = (OrderCreateMessage) msg;
            CreditCardRepresentation card = message.creditCards.get(0);
            
            DNAList patch = new DNAList();
            
            // the selected credit card
            BusinessObjectArray cardList = patch.setBusinessObjectArray("CREDIT_CARD_ARRAY");
            BusinessObject boCard = BMContext.createBusinessObject("CREDIT_CARD");
            boCard.setLong("creditCardList",card.getCcdId());
            cardList.add(boCard);
            
            // A "ccd_id" array
            BusinessObject generic = BMContext.createGenericBusinessObject();
            DNALongArray ccdId = new DNALongArray();
            ccdId.add(card.getCcdId());
            ccdId.add(Long.valueOf(0));
            generic.setLongArrayRef("ccd_id", ccdId);
            patch.setBusinessObjectRef(generic);

            // the CVV
            patch.setString("cvvIndex_"+card.getCcdId(), card.getCardVerifyNum());
            
            WebServiceRequestRunner.patchFormData(patch);            
        }


        @Override
        public boolean isReusable() {
            return true;
        }
    }

    /**
     * Simulator for Registered User verify order
     * @author Simon Greatrix
     *
     */
    public static class RU_Order implements Simulator {
        @Override
        public void invoke(String form, String submit, Type type, Object msg) throws BMException {
            if( type!=Type.USER ) return;
            OrderCreateMessage message = (OrderCreateMessage) msg;          
            if( message.testOnly ) {
                WebServiceRequestRunner.setExitRequest();
            }
        }


        @Override
        public boolean isReusable() {
            return true;
        }
    }


    @POST
    public Response goForIt(OrderCreateMessage message) throws BMException {
        UserAccountRepresentation user = message.userAccount;
        Long userID = user.getUsaId();
        BusinessObject boUser;
        if( userID == null ) {
            // Guest user
            return doGuestUser(message);
        }

        // Get form data
        WebServiceContext context = WebServiceContext.getContext();
        DNAList formData = context.getFormData();
        DNAList formValues = context.getFormValues();

        // price list
        formData.setBusinessObject("PRICE_LIST",
                BelkConstants.getListPriceList());

        // Registered user
        boUser = HTMLUserAccountUtil.getUserAccount(userID);
        formData.setBusinessObjectRef(WebConnect.SESSION_CURRENT_USER, boUser);

        // Create cart item array
        BusinessObjectArray cartArray = new BusinessObjectArray();
        for(CartItemRepresentation item:message.cartItems) {
            cartArray.add(item.getBusinessObject());
        }
        formData.setBusinessObjectArrayRef("SHOPPING_CART", cartArray);

        // Create cart header
        DNAList updated = HTMLOrderUtil.updateCartHeader(formData);
        formData.move(updated);
        cartArray = formData.removeBusinessObjectArray("SHOPPING_CART");
        formData.setBusinessObjectArrayRef("CART_ITEM_ARRAY", cartArray);

        // Shipping address
        Long shipToUadID;
        AddressRepresentation shipAddr = message.shipTo;
        if( shipAddr == null ) {
            shipToUadID = boUser.getLongSafe("usa_ship_to_uad_id");
        } else {
            shipToUadID = message.shipTo.getUadId();
            if( shipToUadID == null ) {
                // TODO : create address and object_address entries for new
                // address
                throw new UnsupportedOperationException(
                        "Not implemented yet : Create shipping address within checkout.");
            }
        }
        if( shipToUadID == null ) {
            throw webAppException(Status.BAD_REQUEST, "SHIP_TO_ADDRESS_MISSING");
        }

        // Generic business object
        BusinessObject generic = BMContext.createGenericBusinessObject();
        formData.setBusinessObjectRef("GENERIC", generic);
        generic.setLong("selectAddressList", shipToUadID);
        generic.setLong("shippingMethodList", message.shipmentMethod.getShmId());

        // the second request requires some form values
        formValues.setString("shippingMethodList",
                message.shipmentMethod.getShmId().toString());
        formValues.setString("selectAddressList", shipToUadID.toString());

        WebServiceRequestRunner runner = new WebServiceRequestRunner();
        runner.execute("checkout_registered", null);

        return null; // TODO
    }


    protected Response doGuestUser(OrderCreateMessage message) throws BMException {
        // TODO
        throw new UnsupportedOperationException(
                "Not implemented yet : Guest user checkout.");
    }
}
