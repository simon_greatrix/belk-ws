package com.belk.rest.actions;

import com.bluemartini.rest.client.UserAccountRepresentation;

import com.belk.rest.client.BMOAuthConsumer;
import com.belk.rest.client.ClientUtil;
import com.belk.rest.client.ClientUtil.Response;
import com.belk.rest.client.ServerException;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * Reset a user's password
 * 
 * @author Simon Greatrix
 *
 */
public class ResetPassword implements Callable<Void> {

    /** User's email address for the account to reset */
    private final String email_;


    /**
     * Create new password reset action
     * 
     * @param email
     *            the email for the account to be reset
     */
    public ResetPassword(String email) {
        email_ = email;
    }


    @Override
    public Void call() throws IOException, ServerException {
        URL url = new URL(ClientUtil.BASE_HTTPS_URL, "user?reset=true");
        UserAccountRepresentation rep = new UserAccountRepresentation();
        rep.setEmail(email_);
        byte[] data = ClientUtil.writeValue(rep);
        Response<BMOAuthConsumer> resp = ClientUtil.doSimple("POST", url, data,
                null);
        resp.throwError();
        return null;
    }
}
