package com.belk.rest.actions;

import com.belk.rest.client.BMOAuthConsumer;
import com.belk.rest.client.ClientUtil;
import com.belk.rest.client.ClientUtil.Response;
import com.belk.rest.client.ServerException;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * Login to the server and get an OAuthConsumer for future connections.
 */
public class Login implements Callable<BMOAuthConsumer> {

    /** The user's ID */
    private final String userID_;

    /** The user's password, which will be blanked after use */
    private char[] password_;


    /**
     * New login attempt
     * 
     * @param userID
     *            the user's ID
     * @param password
     *            the user's password
     */
    public Login(String userID, char[] password) {
        userID_ = userID;
        password_ = password.clone();
    }


    @Override
    public BMOAuthConsumer call() throws IOException, ServerException {
        URL url = new URL(ClientUtil.BASE_HTTPS_URL, "oAuthConsumer");
        BMOAuthConsumer oauth = new BMOAuthConsumer();
        oauth.setUserId(userID_);
        oauth.initKey();

        byte[] data = ClientUtil.writeValue(oauth);
        Response<BMOAuthConsumer> resp = ClientUtil.doBasicAuth(userID_,
                password_, "POST", url, data, BMOAuthConsumer.class);
        resp.throwError();
        return resp.getValue();
    }

}
