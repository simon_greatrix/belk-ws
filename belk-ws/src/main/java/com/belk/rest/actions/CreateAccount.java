package com.belk.rest.actions;

import com.bluemartini.rest.client.UserAccountRepresentation;

import com.belk.rest.client.ClientUtil;
import com.belk.rest.client.ClientUtil.Response;
import com.belk.rest.client.ServerException;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * Action to create a new user account.
 * 
 * @author Simon Greatrix
 *
 */
public class CreateAccount implements Callable<UserAccountRepresentation> {
    /** The password for the new user account */
    private final String password_;

    /** The user-id for the new user account */
    private final String userID_;


    /**
     * New account action
     * 
     * @param userID
     *            the new user ID
     * @param password
     *            the password for the new user
     */
    public CreateAccount(String userID, String password) {
        userID_ = userID;
        password_ = password;
    }


    /**
     * Create the user account
     * 
     * @return the user account
     */
    public UserAccountRepresentation call() throws IOException, ServerException {
        UserAccountRepresentation rep = new UserAccountRepresentation();
        rep.setEmail(userID_);
        rep.setPassword(password_);

        URL url = new URL(ClientUtil.BASE_HTTPS_URL, "user");
        byte[] data = ClientUtil.writeValue(rep);
        Response<UserAccountRepresentation> resp = ClientUtil.doSimple("POST",
                url, data, null);
        resp.throwError();
        return resp.getValue();
    }
}
