package com.belk.rest.actions;

import com.belk.rest.client.BMOAuthConsumer;
import com.belk.rest.client.ClientUtil;
import com.belk.rest.client.ClientUtil.Response;
import com.belk.rest.client.ServerException;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * Logout from the server by deleting the registered OAuthConsumer entity.
 */
public class Logout implements Callable<BMOAuthConsumer> {
    /** The user's OAuth identifier */
    private final BMOAuthConsumer oauth_;


    /**
     * New logout attempt
     * 
     * @param oauth
     *            the user's oauth identifier
     */
    public Logout(BMOAuthConsumer oauth) {
        oauth_ = oauth;
    }


    @Override
    public BMOAuthConsumer call() throws IOException, ServerException {
        URL url = new URL(ClientUtil.BASE_HTTPS_URL, "oAuthConsumer/" + oauth_.getKey());
        Response<Void> resp = ClientUtil.doOAuth(oauth_, "DELETE", url, null, null);
        resp.throwError();
        return null;
    }

}
