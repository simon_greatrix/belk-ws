package com.belk.rest.client;

import com.bluemartini.rest.client.AttributeList;
import com.bluemartini.rest.client.BaseRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Client representation of a REGISTRANT object.
 */
@XmlRootElement(name="Registrant")
public class RegistrantRepresentation extends BaseRepresentation {

    public RegistrantRepresentation() {
        // do nothing
    }

    private AttributeList attributes_ = new AttributeList();
    
    @XmlElementWrapper(name="attributes")
    @XmlElement(name="attribute")
    public AttributeList getAttributes() {
        return attributes_;
    }
    
    public void setAttributeList(AttributeList attributes) {
        attributes_ = attributes;
    }


    private String coregEmail_;

    @XmlElement
    public String getCoregEmail() {
        return coregEmail_;
    }
    
    public void setCoregEmail(String coregEmail) {
        coregEmail_ = coregEmail;
    }
    

    private String coregFname_;

    @XmlElement
    public String getCoregFname() {
        return coregFname_;
    }
    
    public void setCoregFname(String coregFname) {
        coregFname_ = coregFname;
    }
    

    private String coregLname_;

    @XmlElement
    public String getCoregLname() {
        return coregLname_;
    }
    
    public void setCoregLname(String coregLname) {
        coregLname_ = coregLname;
    }
    

    private String coregPhonenum_;

    @XmlElement
    public String getCoregPhonenum() {
        return coregPhonenum_;
    }
    
    public void setCoregPhonenum(String coregPhonenum) {
        coregPhonenum_ = coregPhonenum;
    }
    

    private String coregRole_;

    @XmlElement
    public String getCoregRole() {
        return coregRole_;
    }
    
    public void setCoregRole(String coregRole) {
        coregRole_ = coregRole;
    }
    

    private Long coregUadId_;

    @XmlElement
    public Long getCoregUadId() {
        return coregUadId_;
    }
    
    public void setCoregUadId(Long coregUadId) {
        coregUadId_ = coregUadId;
    }
    

    private Boolean giftCardAvailable_;

    @XmlElement
    public Boolean getGiftCardAvailable() {
        return giftCardAvailable_;
    }
    
    public void setGiftCardAvailable(Boolean giftCardAvailable) {
        giftCardAvailable_ = giftCardAvailable;
    }
    

    private String otherEmail_;

    @XmlElement
    public String getOtherEmail() {
        return otherEmail_;
    }
    
    public void setOtherEmail(String otherEmail) {
        otherEmail_ = otherEmail;
    }
    

    private String otherFname_;

    @XmlElement
    public String getOtherFname() {
        return otherFname_;
    }
    
    public void setOtherFname(String otherFname) {
        otherFname_ = otherFname;
    }
    

    private String otherLname_;

    @XmlElement
    public String getOtherLname() {
        return otherLname_;
    }
    
    public void setOtherLname(String otherLname) {
        otherLname_ = otherLname;
    }
    

    private Long otherUadId_;

    @XmlElement
    public Long getOtherUadId() {
        return otherUadId_;
    }
    
    public void setOtherUadId(Long otherUadId) {
        otherUadId_ = otherUadId;
    }
    

    private String regPhonenum_;

    @XmlElement
    public String getRegPhonenum() {
        return regPhonenum_;
    }
    
    public void setRegPhonenum(String regPhonenum) {
        regPhonenum_ = regPhonenum;
    }
    

    private Calendar registrantAltAddrDate_;

    @XmlElement
    public Calendar getRegistrantAltAddrDate() {
        return registrantAltAddrDate_;
    }
    
    public void setRegistrantAltAddrDate(Calendar registrantAltAddrDate) {
        registrantAltAddrDate_ = registrantAltAddrDate;
    }
    

    private Long registrantAltUadId_;

    @XmlElement
    public Long getRegistrantAltUadId() {
        return registrantAltUadId_;
    }
    
    public void setRegistrantAltUadId(Long registrantAltUadId) {
        registrantAltUadId_ = registrantAltUadId;
    }
    

    private String registrantCommMethod_;

    @XmlElement
    public String getRegistrantCommMethod() {
        return registrantCommMethod_;
    }
    
    public void setRegistrantCommMethod(String registrantCommMethod) {
        registrantCommMethod_ = registrantCommMethod;
    }
    

    private String registrantConsulId_;

    @XmlElement
    public String getRegistrantConsulId() {
        return registrantConsulId_;
    }
    
    public void setRegistrantConsulId(String registrantConsulId) {
        registrantConsulId_ = registrantConsulId;
    }
    

    private String registrantConsulStrNo_;

    @XmlElement
    public String getRegistrantConsulStrNo() {
        return registrantConsulStrNo_;
    }
    
    public void setRegistrantConsulStrNo(String registrantConsulStrNo) {
        registrantConsulStrNo_ = registrantConsulStrNo;
    }
    

    private String registrantEmail_;

    @XmlElement
    public String getRegistrantEmail() {
        return registrantEmail_;
    }
    
    public void setRegistrantEmail(String registrantEmail) {
        registrantEmail_ = registrantEmail;
    }
    

    private String registrantFamStrNo_;

    @XmlElement
    public String getRegistrantFamStrNo() {
        return registrantFamStrNo_;
    }
    
    public void setRegistrantFamStrNo(String registrantFamStrNo) {
        registrantFamStrNo_ = registrantFamStrNo;
    }
    

    private String registrantFname_;

    @XmlElement
    public String getRegistrantFname() {
        return registrantFname_;
    }
    
    public void setRegistrantFname(String registrantFname) {
        registrantFname_ = registrantFname;
    }
    

    private String registrantGrhCode_;

    @XmlElement
    public String getRegistrantGrhCode() {
        return registrantGrhCode_;
    }
    
    public void setRegistrantGrhCode(String registrantGrhCode) {
        registrantGrhCode_ = registrantGrhCode;
    }
    

    private Long registrantGrhId_;

    @XmlElement
    public Long getRegistrantGrhId() {
        return registrantGrhId_;
    }
    
    public void setRegistrantGrhId(Long registrantGrhId) {
        registrantGrhId_ = registrantGrhId;
    }
    

    private String registrantLname_;

    @XmlElement
    public String getRegistrantLname() {
        return registrantLname_;
    }
    
    public void setRegistrantLname(String registrantLname) {
        registrantLname_ = registrantLname;
    }
    

    private Long registrantPerImg_;

    @XmlElement
    public Long getRegistrantPerImg() {
        return registrantPerImg_;
    }
    
    public void setRegistrantPerImg(Long registrantPerImg) {
        registrantPerImg_ = registrantPerImg;
    }
    

    private Long registrantPrimaryUadId_;

    @XmlElement
    public Long getRegistrantPrimaryUadId() {
        return registrantPrimaryUadId_;
    }
    
    public void setRegistrantPrimaryUadId(Long registrantPrimaryUadId) {
        registrantPrimaryUadId_ = registrantPrimaryUadId;
    }
    

    private String registrantRole_;

    @XmlElement
    public String getRegistrantRole() {
        return registrantRole_;
    }
    
    public void setRegistrantRole(String registrantRole) {
        registrantRole_ = registrantRole;
    }
    

    private String registrantShipPref_;

    @XmlElement
    public String getRegistrantShipPref() {
        return registrantShipPref_;
    }
    
    public void setRegistrantShipPref(String registrantShipPref) {
        registrantShipPref_ = registrantShipPref;
    }
    

    private String registrantStoreNo_;

    @XmlElement
    public String getRegistrantStoreNo() {
        return registrantStoreNo_;
    }
    
    public void setRegistrantStoreNo(String registrantStoreNo) {
        registrantStoreNo_ = registrantStoreNo;
    }
    

    private Long registrantUadId_;

    @XmlElement
    public Long getRegistrantUadId() {
        return registrantUadId_;
    }
    
    public void setRegistrantUadId(Long registrantUadId) {
        registrantUadId_ = registrantUadId;
    }
    

}
