package com.belk.rest.client;

import com.bluemartini.rest.client.HMAC_SHA256;

import net.oauth.OAuthAccessor;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.signature.OAuthSignatureMethod;
import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

import javax.net.ssl.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Utilities for communicating with the server
 */
public class ClientUtil {
    /**
     * Encapsulation of a response from the server, which may error
     * 
     * @param <T>
     *            expected return type
     */
    public static class Response<T> {
        /** Value returned */
        private final T value_;

        /** HTTP response code */
        private final int code_;

        /** Error if any */
        private final ExceptionEntity error_;


        /**
         * Create new response
         * 
         * @param code
         *            response code
         * @param value
         *            value, or null
         * @param error
         *            error, or null
         */
        Response(int code, T value, ExceptionEntity error) {
            code_ = code;
            value_ = value;
            error_ = error;
        }


        /**
         * Get the status code
         * 
         * @return the status code
         */
        public int getStatusCode() {
            return code_;
        }


        /**
         * If the server request failed, throw an error now
         * 
         * @throws ServerException
         */
        public void throwError() throws ServerException {
            if( error_ != null ) throw new ServerException(error_);
        }


        /**
         * Get the error the server failed with
         * 
         * @return the error
         */
        public ExceptionEntity getError() {
            return error_;
        }


        /**
         * Get the value the server returned
         * 
         * @return the value
         */
        public T getValue() {
            return value_;
        }


        @Override
        public String toString() {
            return "Response [code=" + code_ + ", value=" + value_ + ", error="
                    + error_ + "]";
        }
    }

    /** Logger for the REST client */
    public final static Logger LOGGER;

    /** URL for non-secure services */
    public final static URL BASE_HTTP_URL;

    /** URL for secure services */
    public final static URL BASE_HTTPS_URL;

    /** This application's ID, used in identifying to the server */
    public final static String APP_ID;

    /** This application's name */
    public final static String APP_NAME;

    /** This application's description */
    public final static String APP_DESCRIPTION;

    /** Converter from JSON to POJO */
    private final static ObjectMapper MAPPER;

    /** UTF-8 character set */
    public final static Charset CS_UTF8 = Charset.forName("UTF-8");

    /** ASCII character set */
    public final static Charset CS_ASCII = Charset.forName("US-ASCII");

    /** Are SSL checks disabled for testing? */
    private final static boolean IS_TESTING_SSL;

    static {
        OAuthSignatureMethod.registerMethodClass("HMAC-SHA256",
                HMAC_SHA256.class);

        Properties properties;
        try {
            // initialise logging
            InputStream in = ClientUtil.class.getClassLoader().getResourceAsStream(
                    "com/belk/rest/client/logging.properties");
            LogManager manager = LogManager.getLogManager();
            manager.readConfiguration(in);
            in.close();
            LOGGER = Logger.getLogger("com.belk.RESTClient");

            // load client properties
            in = ClientUtil.class.getClassLoader().getResourceAsStream(
                    "com/belk/rest/client/client.properties");
            Reader reader = new InputStreamReader(in, "UTF-8");
            properties = new Properties();
            properties.load(reader);
            reader.close();

            // initialise client properties
            BASE_HTTP_URL = new URL(properties.getProperty("http.url"));
            LOGGER.config("Base HTTP URL is: " + BASE_HTTP_URL);
            BASE_HTTPS_URL = new URL(properties.getProperty("https.url"));
            LOGGER.config("Base HTTPS URL is: " + BASE_HTTPS_URL);

            APP_ID = properties.getProperty("app.id");
            LOGGER.config("Application id is: " + APP_ID);
            APP_NAME = properties.getProperty("app.name");
            LOGGER.config("Application Name is: " + APP_NAME);
            APP_DESCRIPTION = properties.getProperty("app.description");
            LOGGER.config("Application Description is: " + APP_NAME);

            IS_TESTING_SSL = properties.getProperty("app.test.ssl").equalsIgnoreCase(
                    "true");
            LOGGER.config("Is testing SSL? : " + IS_TESTING_SSL);

        } catch (Exception e) {
            throw new ExceptionInInitializerError(e);
        }

        ObjectMapper jsonifier = new ObjectMapper();

        // Use ISO-8601 date format
        jsonifier.configure(
                SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);

        // Do not error when deserializing properties that don't have a setter
        jsonifier.configure(
                DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        // Use JAXB annotations in addition to Jackson annotations
        AnnotationIntrospector primary = new JacksonAnnotationIntrospector();
        AnnotationIntrospector secondary = new JaxbAnnotationIntrospector();
        AnnotationIntrospector introspector = new AnnotationIntrospector.Pair(
                primary, secondary);
        SerializationConfig serializeConf = jsonifier.getSerializationConfig().withAnnotationIntrospector(
                introspector);
        jsonifier.setSerializationConfig(serializeConf);
        DeserializationConfig deserialConf = jsonifier.getDeserializationConfig().withAnnotationIntrospector(
                introspector);
        jsonifier.setDeserializationConfig(deserialConf);

        MAPPER = jsonifier;
    }


    /**
     * Convert bytes to RFC-4648 Base 64 representation. Note this differs from
     * the normal RFC-2045 representation by excluding the '=' padding, using
     * '-' instead of '+', and using '_' instead of '/'.
     * 
     * @param data
     *            bytes to encode
     * @return RFC-4648 compliant Base 64
     */
    public static String toBase64URL(byte[] data) {
        byte[] ascii = Base64.encodeBase64(data);

        // The key may not contain '=', '+', nor '/'. We transform the Base64 to
        // RFC 4648 compliant representation which uses '-' and '_' instead.
        for(int i = 0;i < ascii.length;i++) {
            if( ascii[i] == '+' ) {
                ascii[i] = '-';
            } else if( ascii[i] == '/' ) {
                ascii[i] = '_';
            }
        }

        // remove final '='
        int len = ascii.length;
        while( ascii[len - 1] == '=' ) {
            len--;
        }

        // convert to string
        return new String(ascii, 0, len, ClientUtil.CS_ASCII);
    }


    /**
     * Write a value as JSON
     * 
     * @param value
     *            the value to write
     * @return the value as UTF-8 encoded JSON
     * @throws IOException
     */
    public static byte[] writeValue(Object value) throws IOException {
        String json = MAPPER.writeValueAsString(value);
        if( LOGGER.isLoggable(Level.INFO) ) {
            LOGGER.info("Sending value:\n" + json + "\n");
        }
        return json.getBytes(CS_UTF8);
    }


    /**
     * Convert a JSON value back to a POJO
     * 
     * @param data
     *            the JSON value
     * @param type
     *            the expected POJO type
     * @param <T>
     *            the expected return type
     * @return the value
     * @throws IOException
     */
    public static <T> T readValue(byte[] data, Class<T> type) throws IOException {
        return MAPPER.readValue(data, type);
    }


    /**
     * Encode characters as UTF-8. This API allows shared arrays to be used so
     * that secure information can be wiped later.
     * 
     * @param buf
     *            the output buffer
     * @param pos
     *            current location in output
     * @param data
     *            data to encode
     * @return the new position in the buffer
     */
    private static int encode(byte[] buf, int pos, char[] data) {
        for(int i = 0;i < data.length;i++) {
            char ch = data[i];
            if( ch < 0x80 ) {
                buf[pos] = (byte) ch;
                pos++;
            } else if( ch < 0x800 ) {
                buf[pos] = (byte) (0xc0 | (ch >> 6));
                buf[pos + 1] = (byte) (0x80 | (ch & 0x3f));
                pos += 2;
            } else {
                buf[pos] = (byte) (0xe0 | (ch >> 12));
                buf[pos + 1] = (byte) (0x80 | ((ch >> 6) & 0x3f));
                buf[pos + 2] = (byte) (0x80 | (ch & 0x3f));
                pos += 3;
            }
        }
        return pos;
    }


    /**
     * Prepare a connection. If testing, this will turn off security checks.
     * 
     * @param connection
     *            the connection
     * @throws IOException
     */
    private static void prepareSSLConnection(HttpsURLConnection connection) throws IOException {
        if( !IS_TESTING_SSL ) return;

        SSLContext sc;
        try {
            sc = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e) {
            throw new IOException("SSL not supported");
        }
        try {
            sc.init(new KeyManager[0],
                    new TrustManager[] { new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] arg0,
                                String arg1) {
                            // do nothing
                        }


                        @Override
                        public void checkServerTrusted(X509Certificate[] arg0,
                                String arg1) {
                            // do nothing
                        }


                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    } }, new SecureRandom());
        } catch (KeyManagementException e) {
            IOException ioe = new IOException("Test SSL initialisation failed");
            ioe.initCause(e);
            throw ioe;
        }
        SSLSocketFactory sslFactory = sc.getSocketFactory();

        connection.setSSLSocketFactory(sslFactory);
        connection.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
    }


    /**
     * Connect to the server using HTTP Basic authorization
     * 
     * @param userId
     *            the user ID
     * @param password
     *            the user's password
     * @param verb
     *            the HTTP verb for the request
     * @param url
     *            the URL to connect to
     * @param content
     *            the message content to send, or null
     * @param returnType
     *            the return type, or null if none expected
     * @param <T>
     *            the expected return type
     * @return the server's response
     * @throws IOException
     */
    public static <T> Response<T> doBasicAuth(String userId, char[] password,
            String verb, URL url, byte[] content, Class<T> returnType) throws IOException {
        if( !url.getProtocol().equals("https") ) {
            throw new IOException("Basic Authorization must use HTTPS");
        }

        // Create authorization message. We do this in a local array so we can
        // explicitly clear the password once we have done it.
        LOGGER.info("Connecting to " + verb + " " + url);
        byte[] auth = new byte[(userId.length() + password.length + 1) * 3];
        int pos = encode(auth, 0, userId.toCharArray());
        auth[pos] = 0x1f;
        pos++;
        pos = encode(auth, pos, password);

        // convert auth to base 64
        byte[] tmpAuth = new byte[pos];
        System.arraycopy(auth, 0, tmpAuth, 0, pos);
        byte[] base64 = Base64.encodeBase64(tmpAuth);

        // can delete the password copies
        Arrays.fill(auth, (byte) 0);
        Arrays.fill(password, '\0');
        Arrays.fill(tmpAuth, (byte) 0);

        // Build the header, clearing what we can of the password data
        StringBuilder authHeader = new StringBuilder();
        authHeader.append("Basic ");
        for(int i = 0;i < base64.length;i++) {
            authHeader.append((char) base64[i]);
            base64[i] = 0;
        }
        String authText = authHeader.toString();
        for(int i = 0;i < authHeader.length();i++) {
            authHeader.setCharAt(i, '\0');
        }
        // Strings are immutable and cannot be cleared, unfortunately it is what
        // the API requires.

        // open connection
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        prepareSSLConnection(connection);

        // always need these
        connection.setRequestMethod(verb);
        connection.setDoInput(true);
        connection.setInstanceFollowRedirects(true);
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("X-Rest-Authorization", authText);

        // if sending data, set appropriate content headers
        if( content != null ) {
            LOGGER.info("Sending " + content.length + " bytes to server");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type",
                    "application/json; charset=utf-8");
            connection.setRequestProperty("Content-Length",
                    Integer.toString(content.length));
        }

        // ready to connect
        connection.connect();

        // send content, if any
        if( content != null ) {
            if( LOGGER.isLoggable(Level.FINE) ) {
                LOGGER.fine("Sending message:\n" + new String(content, CS_UTF8)
                        + "\n");
            }
            OutputStream out = connection.getOutputStream();
            out.write(content);
            out.flush();
        }

        // get the reply, which may be an error
        int code = connection.getResponseCode();

        if( LOGGER.isLoggable(Level.FINE) ) {
            logHeaders(connection);
        }

        Response<T> response;
        InputStream in;
        if( code == 204 ) {
            // success with no content
            LOGGER.info("Request succeeded: 204");
            response = new Response<T>(code, null, null);
        } else if( code < 400 ) {
            // success with content
            LOGGER.info("Request succeeded: " + code);
            T val = null;
            if( returnType != null ) {
                in = connection.getInputStream();
                in = logStream(in);
                val = MAPPER.readValue(in, returnType);
            }
            response = new Response<T>(code, val, null);
        } else {
            // failure
            LOGGER.info("Request failed: " + code);
            in = connection.getErrorStream();
            in = logStream(in);
            ExceptionEntity ee = MAPPER.readValue(in, ExceptionEntity.class);
            response = new Response<T>(code, null, ee);
        }

        connection.disconnect();

        return response;
    }


    /**
     * Connect to the server using HTTP Basic authorization
     * 
     * @param consumer
     *            the OAuth consumer
     * @param verb
     *            the HTTP verb for the request
     * @param url
     *            the URL to connect to
     * @param content
     *            the message content to send, or null
     * @param returnType
     *            the return type, or null if none expected
     * @param <T>
     *            the expected return type
     * @return the server's response
     * @throws IOException
     */
    public static <T> Response<T> doOAuth(BMOAuthConsumer consumer,
            String verb, URL url, byte[] content, Class<T> returnType) throws IOException {
        LOGGER.info("Connecting to " + verb + " " + url);

        OAuthAccessor accessor = consumer.asAccessor();
        OAuthMessage message = new OAuthMessage(verb, url.toString(), null);

        // create digest of message content
        String digest = null;
        if( content != null ) {
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) {
                throw new IOException("SHA-256 not supported");
            }
            digest = toBase64URL(md.digest(content));
            message.addParameter("oauth_digest", digest);
        }

        // set up the OAuth message
        try {
            message.addRequiredParameters(accessor);
        } catch (OAuthException e) {
            throw new IOException("OAuth Failed", e);
        } catch (URISyntaxException e) {
            throw new IOException("Invalid URI", e);
        }

        // open connection
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        if( connection instanceof HttpsURLConnection ) {
            prepareSSLConnection((HttpsURLConnection) connection);
        }

        // always need these
        connection.setRequestMethod(verb);
        connection.setDoInput(true);
        connection.setInstanceFollowRedirects(true);
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Authorization",
                message.getAuthorizationHeader(null));

        // if sending data, set appropriate content headers
        if( content != null ) {
            LOGGER.info("Sending " + content.length + " bytes to server");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type",
                    "application/json; charset=utf-8");
            connection.setRequestProperty("Content-Length",
                    Integer.toString(content.length));
        }

        // ready to connect
        connection.connect();

        // send content, if any
        if( content != null ) {
            if( LOGGER.isLoggable(Level.FINE) ) {
                LOGGER.fine("Sending message:\n" + new String(content, CS_UTF8)
                        + "\n");
                LOGGER.fine("Message digest: " + digest);
            }
            OutputStream out = connection.getOutputStream();
            out.write(content);
            out.flush();
        }

        // get the reply, which may be an error
        int code = connection.getResponseCode();

        if( LOGGER.isLoggable(Level.FINE) ) {
            logHeaders(connection);
        }

        Response<T> response;
        InputStream in;
        if( code == 204 ) {
            // success with no content
            LOGGER.info("Request succeeded: 204");
            response = new Response<T>(code, null, null);
        } else if( code < 400 ) {
            // success with content
            LOGGER.info("Request succeeded: " + code);
            T val = null;
            if( returnType != null ) {
                in = connection.getInputStream();
                in = logStream(in);
                val = MAPPER.readValue(in, returnType);
            }
            response = new Response<T>(code, val, null);
        } else {
            // failure
            LOGGER.info("Request failed: " + code);
            in = connection.getErrorStream();
            in = logStream(in);
            ExceptionEntity ee = MAPPER.readValue(in, ExceptionEntity.class);
            response = new Response<T>(code, null, ee);
        }

        connection.disconnect();

        return response;
    }


    /**
     * Connect to the server with no authorisation
     * 
     * @param verb
     *            the HTTP verb for the request
     * @param url
     *            the URL to connect to
     * @param content
     *            the message content to send, or null
     * @param returnType
     *            the return type, or null if none expected
     * @param <T>
     *            the expected return type
     * @return the server's response
     * @throws IOException
     */
    public static <T> Response<T> doSimple(String verb, URL url,
            byte[] content, Class<T> returnType) throws IOException {
        LOGGER.info("Connecting to " + verb + " " + url);

        // open connection
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        if( connection instanceof HttpsURLConnection ) {
            prepareSSLConnection((HttpsURLConnection) connection);
        }

        // always need these
        connection.setRequestMethod(verb);
        connection.setDoInput(true);
        connection.setInstanceFollowRedirects(true);
        connection.setRequestProperty("Accept", "application/json");

        // if sending data, set appropriate content headers
        if( content != null ) {
            LOGGER.info("Sending " + content.length + " bytes to server");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type",
                    "application/json; charset=utf-8");
            connection.setRequestProperty("Content-Length",
                    Integer.toString(content.length));
        }

        // ready to connect
        connection.connect();

        // send content, if any
        if( content != null ) {
            if( LOGGER.isLoggable(Level.FINE) ) {
                LOGGER.fine("Sending message:\n" + new String(content, CS_UTF8)
                        + "\n");
            }
            OutputStream out = connection.getOutputStream();
            out.write(content);
            out.flush();
        }

        // get the reply, which may be an error
        int code = connection.getResponseCode();

        if( LOGGER.isLoggable(Level.FINE) ) {
            logHeaders(connection);
        }

        Response<T> response;
        InputStream in;
        if( code == 204 ) {
            // success with no content
            LOGGER.info("Request succeeded: 204");
            response = new Response<T>(code, null, null);
        } else if( code < 400 ) {
            // success with content
            LOGGER.info("Request succeeded: " + code);
            T val = null;
            if( returnType != null ) {
                in = connection.getInputStream();
                in = logStream(in);
                val = MAPPER.readValue(in, returnType);
            }
            response = new Response<T>(code, val, null);
        } else {
            // failure
            LOGGER.info("Request failed: " + code);
            in = connection.getErrorStream();
            in = logStream(in);
            ExceptionEntity ee = MAPPER.readValue(in, ExceptionEntity.class);
            response = new Response<T>(code, null, ee);
        }

        connection.disconnect();

        return response;
    }


    /**
     * Log the server's response
     * 
     * @param in
     *            the server's response
     * @return the response stream after logging
     * @throws IOException
     */
    private static InputStream logStream(InputStream in) throws IOException {
        if( !LOGGER.isLoggable(Level.FINE) ) return in;

        StringBuilder msg = new StringBuilder("Received message:\n");
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        int r;
        while( (r = in.read()) != -1 ) {
            buf.write(r);
            msg.append((char) r);
        }
        msg.append('\n');
        LOGGER.fine(msg.toString());
        return new ByteArrayInputStream(buf.toByteArray());
    }


    /**
     * Log the server response's HTTP headers
     * 
     * @param connection
     *            the connection to the server
     */
    private static void logHeaders(HttpURLConnection connection) {
        Map<String, List<String>> headers = connection.getHeaderFields();
        StringBuilder msg = new StringBuilder();
        for(Map.Entry<String, List<String>> e:headers.entrySet()) {
            for(String v:e.getValue()) {
                msg.append(e.getKey()).append(" : ").append(v).append('\n');
            }
        }
        LOGGER.fine(msg.toString());
    }
}
