package com.belk.rest.client;

import com.belk.rest.client.ClientUtil.Response;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * Ping the server. Receives a message and responds with the same text
 * timestamped.
 * 
 * @author Simon Greatrix
 *
 */
public class Ping implements Callable<ExceptionEntity> {
    /** The OAuth Consumer to verify this with */
    private BMOAuthConsumer consumer_;

    /** The URL to connect to */
    private URL url_;

    /** New Ping attempt
     * 
     * @param consumer for verification
     */
    public Ping(BMOAuthConsumer consumer) {
        consumer_ = consumer;
        try {
            url_ = new URL(ClientUtil.BASE_HTTP_URL, "ping");
        } catch (MalformedURLException e) {
            // should not happen
            throw new ExceptionInInitializerError(e);
        }
    }


    @Override
    public ExceptionEntity call() throws ServerException, IOException {
        ExceptionEntity test = new ExceptionEntity();
        test.setErrorCode("PING");
        test.setDevMessage("ping");
        byte[] data = ClientUtil.writeValue(test);
        Response<ExceptionEntity> resp = ClientUtil.doOAuth(consumer_, "POST",
                url_, data, ExceptionEntity.class);
        resp.throwError();
        return resp.getValue();
    }
}
