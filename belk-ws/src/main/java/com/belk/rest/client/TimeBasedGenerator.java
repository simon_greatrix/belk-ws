package com.belk.rest.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.SecureRandom;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of UUID generator that uses time/location based generation
 * method (variant 1).
 * <p>
 */
public class TimeBasedGenerator {
    /** Secure random number generator */
    private static final Random RANDOM = new SecureRandom();

    /** Default instance */
    private static TimeBasedGenerator INSTANCE = null;
    
    /** Logger for UUID related stuff */
    private static Logger LOGGER = Logger.getLogger("com.belk.RESTClient.UUID");


    /**
     * Create a Type-1 UUID using the default MAC address
     * 
     * @return a newly generated UUID
     */
    public static UUID create() {
        if( INSTANCE == null ) {
            INSTANCE = new TimeBasedGenerator(null);
        }
        return INSTANCE.generate();
    }
    
    
    /**
     * Get the milliseconds since the epoch time.
     * @param uuid the UUID
     * @return the milliseconds since the epoch
     */
    public static long getTime(UUID uuid) {
        long timestamp = uuid.timestamp();
        timestamp -= UUIDTime.CLOCK_OFFSET;
        timestamp /= 10000;
        return timestamp;
    }


    /**
     * Get the MAC address to use with this computer
     * 
     * @return the MAC address
     */
    private static byte[] getAddress() {
        try {
            // first try local host
            InetAddress localHost = InetAddress.getLocalHost();
            if( !localHost.isLoopbackAddress() ) {
                NetworkInterface nint = NetworkInterface.getByInetAddress(localHost);
                if( nint != null ) {
                    byte[] data = nint.getHardwareAddress();
                    if( data != null && data.length == 6 ) {
                        return data;
                    }
                }
            }
        } catch (IOException e) {
            // possibly the look-up of local host failed
            LOGGER.log(Level.INFO,"Failed to get localhost hardware address.",e);
        }

        // now try all interfaces
        LinkedList<NetworkInterface> list = new LinkedList<NetworkInterface>();
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while( en.hasMoreElements() ) {
                list.addLast(en.nextElement());
            }
        } catch (SocketException se) {
            // cannot process interfaces
            LOGGER.log(Level.INFO,"Failed to get network interfaces.",se);
        }

        while( !list.isEmpty() ) {
            NetworkInterface nint = list.removeFirst();
            try {
                if( !nint.isLoopback() ) {
                    byte[] data = nint.getHardwareAddress();
                    if( data != null && data.length == 6 ) {
                        return data;
                    }
                }

                // queue up sub-interfaces in order
                LinkedList<NetworkInterface> tmp = new LinkedList<NetworkInterface>();
                Enumeration<NetworkInterface> en = nint.getSubInterfaces();
                while( en.hasMoreElements() ) {
                    tmp.addLast(en.nextElement());
                }
                while( !tmp.isEmpty() ) {
                    list.addFirst(tmp.removeLast());
                }
            } catch (SocketException se) {
                // ignore this interface
                LOGGER.log(Level.INFO,"Failed to get localhost hardware address or sub-interfaces for "+nint.getDisplayName(),se);
            }
        }

        // Must create random multi-cast address. We will create one from an
        // unused block in the CF range. The CF range is currently closed but
        // was intended for when there is no appropriate regular organizational
        // unit identifier (OUI) which would normally constitute the first three
        // bytes of the MAC address.
        byte[] data = new byte[6];
        RANDOM.nextBytes(data);
        data[0] = (byte) 0xcf;
        data[1] = (byte) (data[1] | 0x80);
        return data;
    }

    /** The ethernet address this generator is associated with */
    protected final long ethernetAddress_;


    /**
     * Create a Type 1 UUID generator for the specified MAC address
     * 
     * @param address
     *            the MAC address (6 bytes)
     */
    public TimeBasedGenerator(byte[] address) {
        if( address != null && address.length != 6 ) {
            throw new IllegalArgumentException(
                    "MAC Address must contain 48 bits");
        }
        UUIDTime.init(RANDOM);
        if( address == null ) {
            address = getAddress();
        }

        long v = 0;
        for(int i = 0;i < 6;i++) {
            v = (v << 8) | (address[i] & 0xff);
        }
        ethernetAddress_ = v;
    }


    /**
     * Generate a Type 1 UUID for this address
     * 
     * @return a new UUID
     */
    public UUID generate() {
        UUIDTime time = UUIDTime.newTimeStamp();
        final long rawTimestamp = time.getTime();
        final int sequence = time.getSequence();

        // first 32 bits are the lowest 32 bits of the time
        long l1 = (rawTimestamp & 0xffffffffl) << 32;

        // next 16 bits are the middle of the time
        l1 |= (rawTimestamp & 0xffff00000000l) >> 16;

        // next 4 bits are the version code
        l1 |= 0x1000;

        // last 12 bits are the next 12 bits of the time
        l1 |= (rawTimestamp & 0xfff000000000000l) >> 48;

        // the top 4 bits of the time are lost

        // now combine the 14 bit sequence with the address.
        long l2 = ((long) ((sequence & 16383) | 32768)) << 48;
        l2 |= ethernetAddress_;

        return new UUID(l1, l2);
    }
}




/**
 * Representation of the time and sequence used to generate Type-1 UUIDs.
 */
final class UUIDTime {
    /**
     * UUIDs need time from the beginning of Gregorian calendar (15-OCT-1582),
     * need to apply this offset from the System current time.
     */
    final static long CLOCK_OFFSET = 0x01b21dd213814000L;

    /**
     * Sequence number for a given clock value.
     */
    private static int CLOCK_SEQUENCE = 0;
    
    /**
     * Clock sequence mask which is XORed to comply with the RFCs requirement
     * for unpredictable sequence numbers.
     */
    private static int CLOCK_MASK = 0;

    /**
     * Initial time which may be negative. Used to ensure we get a positive time
     * difference.
     */
    private final static long INIT_TIME = CLOCK_OFFSET + (10000 * System.currentTimeMillis()) - (System.nanoTime()/100);

    /** Has the timer been initialised? */
    private static boolean IS_INIT_DONE = false;

    /**
     * Timestamp value last used for generating a UUID.
     */
    private static long LAST_MASK_TIMESTAMP = 0L;
    
    /**
     * Source for clock masks
     */
    private static Random MASK_SOURCE;


    /**
     * Initialize this timer. Ideally the random number generator will provide a
     * secure random value to initialise the sequence with.
     * 
     * @param random
     *            RNG
     */
    static synchronized void init(Random random) {
        if( IS_INIT_DONE ) return;

        if( random == null ) {
            random = new SecureRandom();
        }
        MASK_SOURCE = random;
        CLOCK_MASK = random.nextInt(16384);
        LAST_MASK_TIMESTAMP = (System.nanoTime()/100) + INIT_TIME;
        IS_INIT_DONE = true;        
    }


    /**
     * Method that constructs a unique timestamp.
     * 
     * @return 64-bit timestamp to use for constructing UUID
     */
    public static final synchronized UUIDTime newTimeStamp() {
        long sysTime = (System.nanoTime()/100) + INIT_TIME;
        if( sysTime < LAST_MASK_TIMESTAMP ) {
            sysTime = LAST_MASK_TIMESTAMP;
        }

        CLOCK_SEQUENCE++;
        if( CLOCK_SEQUENCE == 16384 ) {
            sysTime++;
            CLOCK_SEQUENCE = 0;
            CLOCK_MASK = MASK_SOURCE.nextInt(16384);
            LAST_MASK_TIMESTAMP = sysTime;
        }

        return new UUIDTime(sysTime, CLOCK_SEQUENCE ^ CLOCK_MASK);
    }

    /**
     * A 14-bit sequence number unique with the current 100 nanosecond interval.
     */
    private final int sequence_;

    /**
     * The number of 100 nanosecond intervals that have passed since the start
     * of the Gregorian calendar.
     */
    private final long timeStamp_;


    /**
     * New time and sequence
     * 
     * @param timeStamp
     *            the time stamp
     * @param sequence
     *            the sequence
     */
    private UUIDTime(long timeStamp, int sequence) {
        timeStamp_ = timeStamp;
        sequence_ = sequence & 0x3fff;
    }


    /**
     * Get the 16-bit sequence value
     * 
     * @return the sequence value
     */
    public int getSequence() {
        return sequence_;
    }


    /**
     * Get the 64-bit 100 nanosecond time value
     * 
     * @return the time value
     */
    public long getTime() {
        return timeStamp_;
    }
}
