package com.belk.rest.client;

import com.bluemartini.rest.client.SkuRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;

/**
 * Representation of a BelkSku which extends a Blue Martini Sku with additional
 * properties.
 * 
 * @author Simon Greatrix
 *
 */
@XmlRootElement(name = "sku")
public class BelkSkuRepresentation extends SkuRepresentation {
    /** This Sku's brand */
    private String brand_;

    /** This Sku's color */
    private String colorDesc_;

    /** This Sku's department */
    private String department_;

    /** This Sku's representative image URL */
    private String imageURL_;

    /** This Sku's eligibility for bridal sales */
    private boolean isForBridal_;

    /** This Sku's list price */
    private BigDecimal listPrice_;

    /** This Sku's descriptive name */
    private String name_;

    /** This Sku's color shade */
    private String shadeDesc_;

    /** This Sku's description */
    private String shortDescription_;

    /** This Sku's size */
    private String sizeDesc_;

    /** This Sku's universal product code */
    private String upc_;


    /** Create a new BelkSku representation */
    public BelkSkuRepresentation() {
        // do nothing
    }


    @XmlElement
    public String getBrand() {
        return brand_;
    }


    @XmlElement
    public String getColorDesc() {
        return colorDesc_;
    }


    @XmlElement
    public String getDepartment() {
        return department_;
    }


    @XmlElement
    public String getImageURL() {
        return imageURL_;
    }


    @XmlElement
    public BigDecimal getListPrice() {
        return listPrice_;
    }


    @XmlElement
    public String getName() {
        return name_;
    }


    @XmlElement
    public String getShadeDesc() {
        return shadeDesc_;
    }


    @XmlElement
    public String getShortDescription() {
        return shortDescription_;
    }


    @XmlElement
    public String getSizeDesc() {
        return sizeDesc_;
    }


    @XmlElement
    public String getUPC() {
        return upc_;
    }


    public boolean isForBridal() {
        return isForBridal_;
    }


    public void setBrand(String brand) {
        brand_ = brand;
    }


    public void setColorDesc(String colorDesc) {
        colorDesc_ = colorDesc;
    }


    public void setDepartment(String department) {
        department_ = department;
    }


    public void setForBridal(boolean isForBridal) {
        isForBridal_ = isForBridal;
    }


    public void setImageURL(String imageURL) {
        imageURL_ = imageURL;
    }


    public void setListPrice(BigDecimal listPrice) {
        listPrice_ = listPrice;
    }


    public void setName(String name) {
        name_ = name;
    }


    public void setShadeDesc(String shadeDesc) {
        shadeDesc_ = shadeDesc;
    }


    public void setShortDescription(String shortDescription) {
        shortDescription_ = shortDescription;
    }


    public void setSizeDesc(String sizeDesc) {
        sizeDesc_ = sizeDesc;
    }


    public void setUPC(String upc) {
        upc_ = upc;
    }
}
