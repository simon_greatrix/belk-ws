package com.belk.rest.client;

import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * Representation of an OAuth Consumer
 */
@XmlRootElement(name = "oAuthConsumer")
public class BMOAuthConsumer {
    /** OAuthAccessor for working with net.oauth API */
    private transient OAuthAccessor accessor_ = null;

    /** Callback URL */
    private String callbackURL_;

    /** OAuthConsumer for working with net.oauth API */
    private transient OAuthConsumer consumer_ = null;

    /** Description of application */
    private String description_ = ClientUtil.APP_DESCRIPTION;

    /** Consumer's key */
    private String key_;

    /** Application name */
    private String name_ = ClientUtil.APP_NAME;

    /** Consumer's secret */
    private String secret_;

    /** Consumer's status */
    private String status_ = "A";

    /** User ID associated with this consumer */
    private String userId_;


    /**
     * Get this as an accessor
     * @return this as an accessor
     */
    public OAuthAccessor asAccessor() {
        OAuthAccessor acc = accessor_;
        if( acc != null ) return acc;
        acc = new OAuthAccessor(asConsumer());
        accessor_ = acc;
        return acc;
    }

    /**
     * Get this as a consumer
     * @return this as a consumer
     */
    public OAuthConsumer asConsumer() {
        OAuthConsumer cons = consumer_;
        if( cons != null ) return cons;

        cons = new OAuthConsumer(callbackURL_, key_, secret_, null);
        cons.setProperty("oauth_signature_method","HMAC-SHA256");
        cons.setProperty("description", description_);
        cons.setProperty("name", name_);
        consumer_ = cons;

        return cons;
    }


    /**
     * Get the callback URL
     * 
     * @return the URL
     */
    @XmlElement
    public String getCallbackURL() {
        return callbackURL_;
    }


    /**
     * Get the application description. Defaults to the value specified in the
     * <code>client.properties</code> file.
     * 
     * @return the description
     */
    @XmlElement(name = "consumerDescription")
    public String getDescription() {
        return description_;
    }


    /**
     * Get this consumer's identifying key
     * 
     * @return the key
     */
    @XmlElement(name = "consumerKey")
    public String getKey() {
        return key_;
    }


    /**
     * Get this application's name. Defaults to the value specified in the
     * <code>client.properties</code> file.
     * 
     * @return the name
     */
    @XmlElement(name = "consumerName")
    public String getName() {
        return name_;
    }


    /**
     * Get the secret key used by this consumer
     * 
     * @return the secret key
     */
    @XmlElement(name = "consumerSecret")
    public String getSecret() {
        return secret_;
    }


    /**
     * Get this consumer's status. Either 'A' for Active or 'I' for Inactive.
     * Defaults to 'A'.
     * 
     * @return the status
     */
    @XmlElement(name = "statusCd")
    public String getStatus() {
        return status_;
    }


    /**
     * Get the user ID associated with this consumer.
     * 
     * @return the user ID
     */
    @XmlElement
    public String getUserId() {
        return userId_;
    }


    /**
     * Initialise this consumer's key. This method creates a unique key that can
     * be used to identify this user on this application. It should be
     * impossible to guess the key a user will be using.
     * <p>
     * 
     * The key is required to be globally unique. This is achieved by building
     * the key from three components: a Type-1 UUID, the user's ID, and the
     * application's ID. These components are combined and then processed with
     * SHA-256 to create a unique opaque identifier.
     * 
     */
    public void initKey() {
        // First we create a unique identifier string for this application. The
        // identifier consists of:
        //
        // 1) A type 1 UUID, identifying this machine and the time it was
        // created
        // 2) The user ID
        // 3) The application ID
        //
        // This information is then passed through a SHA-256 digest to create an
        // opaque unique identifier that reveals nothing about the client.

        // Create UUID
        UUID uuid = TimeBasedGenerator.create();

        byte[] bytes;
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            DataOutputStream data = new DataOutputStream(buffer);
            data.writeLong(uuid.getMostSignificantBits());
            data.writeLong(uuid.getLeastSignificantBits());
            data.writeUTF(userId_);
            data.writeUTF(ClientUtil.APP_ID);
            data.flush();
            bytes = buffer.toByteArray();
        } catch (IOException ioe) {
            // should never happen writing to a ByteArrayOutputStream
            throw new Error("Impossible IO Exception", ioe);
        }

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            bytes = md.digest(bytes);
        } catch (NoSuchAlgorithmException nsae) {
            System.err.println("WARNING: SHA-256 not supported");
            // if no SHA-256, just use first 32 bytes
            if( bytes.length > 32 ) {
                byte[] tmp = new byte[32];
                System.arraycopy(bytes, 0, tmp, 0, 32);
                bytes = tmp;
            }
        }

        key_ = ClientUtil.toBase64URL(bytes);
    }


    /**
     * Set the callback URL
     * 
     * @param callbackURL
     *            the URL
     */
    public void setCallbackURL(String callbackURL) {
        this.callbackURL_ = callbackURL;
    }


    /**
     * Set this application's description. Defaults to the value specified in
     * the <code>client.properties</code> file.
     * 
     * @param description
     *            the description.
     */
    public void setDescription(String description) {
        description_ = description;
    }


    /**
     * Set this consumer's identifying key
     * 
     * @param key
     *            the key
     */
    public void setKey(String key) {
        key_ = key;
    }


    /**
     * Set this application's name. Defaults to the value specified in the
     * <code>client.properties</code> file.
     * 
     * @param name
     *            the name
     */
    public void setName(String name) {
        name_ = name;
    }


    /**
     * Set this consumers's secret.
     * 
     * @param secret
     *            the secret
     */
    public void setSecret(String secret) {
        secret_ = secret;
    }


    /**
     * Set this consumer's status. Valid values are 'A' for Active and 'I' for
     * Inactive. Defaults to 'A'.
     * 
     * @param status
     *            the new status code
     */
    public void setStatus(String status) {
        status_ = status;
    }


    /**
     * Set the user ID associated with this consumer.
     * 
     * @param userId
     *            the user ID
     */
    public void setUserId(String userId) {
        this.userId_ = userId;
    }


    @Override
    public String toString() {
        return "OAuthConsumer [callbackURL=" + callbackURL_ + ", description="
                + description_ + ", key=" + key_ + ", name=" + name_
                + ", secret=" + secret_ + ", status=" + status_ + ", userId="
                + userId_ + "]";
    }

}
