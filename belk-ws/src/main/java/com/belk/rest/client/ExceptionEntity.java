package com.belk.rest.client;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A REST API entity representation that carries the detail information to be
 * returned to the client to accompany an exception or error response. These
 * would typically be HTTP 400 or 500 series responses.
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * 
 * @author Simon Greatrix
 */
@XmlRootElement(name = "exceptionDetail")
public class ExceptionEntity {

    /** A developer orientated message */
    protected String devMessage_;

    /** An error code for the message */
    protected String errorCode_;

    /** URL for more information, if any */
    protected String moreInfoLink_ = null;

    /** Timestamp of the exception */
    protected String timestamp_;

    /** A user orientated message */
    protected String userMessage_;


    /**
     * New entity.
     * 
     * @internal Default constructor required by JAXB.
     */
    public ExceptionEntity() {}


    /**
     * Gets the verbose message to assist the client developer.
     * 
     * @return the developer message as a String
     */
    @XmlElement
    public String getDeveloperMessage() {
        return devMessage_;
    }


    /**
     * Gets the error code for this response.
     * 
     * @return the error code as a String
     */
    @XmlElement
    public String getErrorCode() {
        return errorCode_;
    }


    /**
     * Gets the link to obtain more info for this API response.
     * 
     * @return the moreInfo link as a String
     */
    @XmlElement
    public String getMoreInfo() {
        return moreInfoLink_;
    }


    /**
     * Gets the date and time this response entity was created. Value will be in
     * ISO-8601 format.
     * 
     * @return the datetime time stamp as a String
     */
    @XmlElement
    public String getTimestamp() {
        return timestamp_;
    }


    /**
     * Gets the user message to be presented to the user.
     * 
     * @return the user message as a String
     */
    @XmlElement
    public String getUserMessage() {
        return userMessage_;
    }


    /**
     * Set the developer orientated message
     * 
     * @param devMessage
     *            the message
     */
    public void setDevMessage(String devMessage) {
        devMessage_ = devMessage;
    }


    /**
     * Set the error code
     * 
     * @param errorCode
     *            the error code
     */
    public void setErrorCode(String errorCode) {
        errorCode_ = errorCode;
    }


    /**
     * Sets the link to obtain more info for this API response.
     * 
     * @param moreInfoLink
     *            the moreInfo link as a String
     */
    public void setMoreInfo(String moreInfoLink) {
        moreInfoLink_ = moreInfoLink;
    }


    /**
     * Set the time stamp
     * 
     * @param timestamp
     *            the time stamp
     */
    public void setTimestamp(String timestamp) {
        timestamp_ = timestamp;
    }


    /**
     * Set the user orientated message
     * 
     * @param userMessage
     *            the message
     */
    public void setUserMessage(String userMessage) {
        userMessage_ = userMessage;
    }


    /**
     * Returns a String enumerating the properties of this bean as name/value
     * pairs. Each property is separated by a line-feed.
     * 
     * @return a String representation of this object
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nTimestamp   : ").append(timestamp_).append("\n");
        sb.append("Error Code  : ").append(errorCode_).append("\n");
        sb.append("User Message: ").append(userMessage_).append("\n");
        sb.append("Dev Message : ").append(devMessage_).append("\n");
        if( moreInfoLink_ != null ) {
            sb.append("More Info   : ").append(moreInfoLink_).append("\n");
        }
        return sb.toString();
    }
}
