package com.belk.rest.client;

/**
 * An exception thrown when the server has encountered a problem
 */
public class ServerException extends Exception {
    /** serial version UID */
    private static final long serialVersionUID = 1l;


    /**
     * Create a new exception. The exception's message is taken from the entity
     * 
     * @param entity
     *            the entity returned from the server
     */
    public ServerException(ExceptionEntity entity) {
        super(entity.toString());
    }


    /**
     * Create a new exception.
     * 
     * @param message
     *            descriptive message
     * @param cause
     *            cause of exception
     */
    public ServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
