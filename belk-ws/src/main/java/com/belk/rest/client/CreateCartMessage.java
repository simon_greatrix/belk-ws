package com.belk.rest.client;

import com.bluemartini.rest.client.CartItemRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreateCart")
public class CreateCartMessage {
    @XmlElement
    public CartItemRepresentation[] cartItems;    
}
