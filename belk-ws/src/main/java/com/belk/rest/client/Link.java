package com.belk.rest.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A link object that can be returned within a resource representation. Links
 * point to other resources and actions that the client can navigate to within
 * the API.
 * <p>
 * Copyright (c) 2013 JDA Software
 * <p>
 * 
 * @author Stephanie Kroll
 */
@XmlType(propOrder = { "href", "ref", "method", "type" })
@XmlAccessorType(XmlAccessType.NONE)
public class Link {

    /**
     * An enumeration of the valid HTTP methods for a Link.
     */
    public enum HTTPMethod {
        GET, POST, PUT, DELETE, PATCH, OPTIONS
    }

    // this link's relationship value
    private String rel_;

    // the URI to the link's target
    private String href_;

    // the HTTP method used to access the link's target
    private HTTPMethod method_;

    // the media type to use for requests to the link's target
    private String mediaTypeName_;


    /**
     * Constructs a new empty {@link Link} object. Default constructor required
     * by JAXB.
     */
    public Link() {}


    /**
     * Gets the URI to use when accessing the target of this link.
     *
     * @return the target URI as a String
     */
    @XmlElement(name = "href")
    public String getHref() {
        return href_;
    }


    /**
     * Gets the HTTP method (verb) to use when following this link.
     *
     * @return the HTTP method as one of the {@link HTTPMethod} enumeration
     *         values
     */
    @XmlElement(name = "method")
    public HTTPMethod getMethod() {
        return method_;
    }


    /**
     * Gets the relationship between the current resource and the target of this
     * link.
     *
     * @return a String containing the relationship value
     */
    @XmlElement(name = "rel")
    public String getRel() {
        return rel_;
    }


    /**
     * Gets the name of the media type to use for requests to the link.
     *
     * @return a String containing the media type name (MIME type)
     */
    @XmlElement(name = "type")
    public String getType() {
        return mediaTypeName_;
    }


    /**
     * Sets the URI to use when accessing the target of this link.
     *
     * @param href
     *            the target URI as a String
     */
    public void setHref(String href) {
        href_ = href;
    }


    /**
     * Sets the HTTP method (verb) to use when following this link.
     *
     * @param method
     *            the HTTP method as one of the {@link HTTPMethod} enumeration
     *            values
     */
    public void setMethod(HTTPMethod method) {
        method_ = method;
    }


    /**
     * Sets the relationship between the current resource and the target of this
     * link.
     *
     * @param rel
     *            a String containing the relationship value
     */
    public void setRel(String rel) {
        rel_ = rel;
    }


    /**
     * Sets the name of the media type to use for requests to the link.
     *
     * @param mediaTypeName
     *            a String containing the media type name (MIME type)
     */
    public void setType(String mediaTypeName) {
        mediaTypeName_ = mediaTypeName;
    }
}
