package com.belk.rest.client;

import com.bluemartini.rest.client.AttributeList;
import com.bluemartini.rest.client.BaseRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Client representation of a EVENT object.
 */
@XmlRootElement(name="Event")
public class EventRepresentation extends BaseRepresentation {

    public EventRepresentation() {
        // do nothing
    }

    private AttributeList attributes_ = new AttributeList();
    
    @XmlElementWrapper(name="attributes")
    @XmlElement(name="attribute")
    public AttributeList getAttributes() {
        return attributes_;
    }
    
    public void setAttributeList(AttributeList attributes) {
        attributes_ = attributes;
    }


    private String eventCity_;

    @XmlElement
    public String getEventCity() {
        return eventCity_;
    }
    
    public void setEventCity(String eventCity) {
        eventCity_ = eventCity;
    }
    

    private Calendar eventDt_;

    @XmlElement
    public Calendar getEventDt() {
        return eventDt_;
    }
    
    public void setEventDt(Calendar eventDt) {
        eventDt_ = eventDt;
    }
    

    private Long eventGrhId_;

    @XmlElement
    public Long getEventGrhId() {
        return eventGrhId_;
    }
    
    public void setEventGrhId(Long eventGrhId) {
        eventGrhId_ = eventGrhId;
    }
    

    private String eventOtherState_;

    @XmlElement
    public String getEventOtherState() {
        return eventOtherState_;
    }
    
    public void setEventOtherState(String eventOtherState) {
        eventOtherState_ = eventOtherState;
    }
    

    private String eventState_;

    @XmlElement
    public String getEventState() {
        return eventState_;
    }
    
    public void setEventState(String eventState) {
        eventState_ = eventState;
    }
    

    private Integer numGuests_;

    @XmlElement
    public Integer getNumGuests() {
        return numGuests_;
    }
    
    public void setNumGuests(Integer numGuests) {
        numGuests_ = numGuests;
    }
    

}
