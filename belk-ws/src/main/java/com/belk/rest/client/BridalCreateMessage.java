package com.belk.rest.client;

import com.bluemartini.rest.client.AddressRepresentation;
import com.bluemartini.rest.client.UserAccountRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Message to create a bridal registry
 * 
 * @author Simon Greatrix
 *
 */
@XmlRootElement(name = "BridalCreate")
public class BridalCreateMessage {
    /** The user account which will own the bridal registry */
    @XmlElement
    public UserAccountRepresentation userAccount;

    /** Address of the primary registrant for the registry */
    @XmlElement
    public AddressRepresentation registrantAddress;

    /** Alternate Address for the registry */
    @XmlElement
    public AddressRepresentation alternateAddress;

    /** The registrant for the registry */
    @XmlElement
    public RegistrantRepresentation registrant;

    /** The wedding event details */
    @XmlElement
    public EventRepresentation event;
}
