package com.belk.rest.client;

import com.bluemartini.rest.client.AddressRepresentation;
import com.bluemartini.rest.client.CartItemRepresentation;
import com.bluemartini.rest.client.CreditCardRepresentation;
import com.bluemartini.rest.client.UserAccountRepresentation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Message to create an order.
 * 
 * @author Simon Greatrix
 *
 */
@XmlRootElement(name = "OrderCreate")
public class OrderCreateMessage {

    /**
     * The user account which will own the order. Must specify either a USA_ID
     * or an e-mail address.
     */
    @XmlElement
    public UserAccountRepresentation userAccount;

    /** Address to ship the order to */
    @XmlElement
    public AddressRepresentation shipTo;

    /** Address for billing */
    @XmlElement
    public AddressRepresentation billTo;

    /**
     * The items in the shopping cart
     */
    @XmlElement
    public CartItemRepresentation.Items cartItems;

    @XmlElement
    public CreditCardRepresentation.Items creditCards;
}
